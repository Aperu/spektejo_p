<?php

/**
 * Portempa trajto dum Populareco estas implementata
 *
 * @package Spektejo
 */


/**
 * Legas la eventodatumbazon kaj listigas la laste plej aktivajn erojn laŭ kiterioj
 */
function a_spektejo_ptmp2311_retrovi_identigilojn_de_plejaktivaj_afisxoj()
{
    $datumbazo = a_spektejo_eventodatumbazo();
    if (!$datumbazo) {
        return []; // FIXME: Montri eraron al la administranto
    }

    $sql = "SELECT afisxo_id, MAX(tempo) as maks_tempo
    FROM eventoj
    WHERE evento IN ('spekto', 'komento')
    GROUP BY afisxo_id
    ORDER BY maks_tempo DESC
    LIMIT 36";

    $inst = $datumbazo->prepare($sql);
    $inst->execute();
    $rezulto = $inst->fetchAll(PDO::FETCH_ASSOC);

    $datumbazo = null; // Fermi la konekton al la datumbazo

    if (empty($rezulto)) {
        return [];
    }

    $identigiloj = array_column($rezulto, 'afisxo_id');

    return $identigiloj;
}

/**
 * Konservas la laste plej aktivajn erojn  kiel JSON por esti legata poste, de aperu.net
 */
function a_spektejo_ptmp2311_retrovi_datumojn_por_plejaktivaj_afisxoj()
{
    $identigiloj = a_spektejo_ptmp2311_retrovi_identigilojn_de_plejaktivaj_afisxoj();
    if (empty($identigiloj)) {
        return [];
    }

    // Elfiltri identigilojn por kaŝitaj afiŝoj
    /* $identigiloj = array_filter(
        $identigiloj,
        function ($identigilo) {
            $rilateco = wp_get_post_terms($identigilo, 'v_rilateco')[0]->slug;
            return in_array($rilateco, ['esperanta', 'priesperanta', 'nedifina'], true);
        }
    ); */

    $afisxodatumoj = array_map(
        function ($identigilo) {
            $afisxo = get_post($identigilo);
            $fonto = wp_get_post_terms($identigilo, 'v_fonto')[0]->slug;
            $identigilo_fonto = get_post_meta($identigilo, 'v_identigilo_fonto', true);
            return [
                'id'      => $identigilo,
                'speco'   => 'video',
                'titolo'  => $afisxo->post_title,
                'auxtoro' => wp_get_post_terms($identigilo, 'v_kanalo')[0]->name,
                'ligilo'  => get_permalink($identigilo),
                'bildeto' => match ($fonto) {
                    'youtube' => 'https://i.ytimg.com/vi/' . $identigilo_fonto . '/default.jpg',
                    default   => ''
                }
            ];
        },
        $identigiloj
    );

    return $afisxodatumoj;
}

/**
 * Konservas la laste plej aktivajn erojn  kiel JSON por esti legata poste, de aperu.net
 */
function a_spektejo_ptmp2311_elporti_plejaktivajn_afisxojn()
{
    $dosierujo = ABSPATH . '/datumoj/elporte/';
    $dosiernomo = 'laste_aktivaj.json';
    $dosierloko = $dosierujo . $dosiernomo;
    if (!is_dir($dosierujo) && !mkdir($dosierujo)) {
        return;
    }

    $eroj = a_spektejo_ptmp2311_retrovi_datumojn_por_plejaktivaj_afisxoj();
    if (empty($eroj)) {
        return;
    }

    $eroj_json = json_encode($eroj, JSON_PRETTY_PRINT);
    file_put_contents($dosierloko, $eroj_json);
}
add_action('ago_elportado_de_laste_aktivaj_afisxoj', 'a_spektejo_ptmp2311_elporti_plejaktivajn_afisxojn');
