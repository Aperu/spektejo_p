<?php

/**
 * Programaro por Spektejo
 *
 * @package Spektejo
 *
 * @wordpress-plugin
 * Plugin Name:       Programaro por Spektejo
 * Plugin URI:        https://aperu.net
 * Description:       Vortpresa kromprogramo por funkciigi la Vortpresan etoson Spektejo
 * Version:           23.12.31
 * Author:            Teamo de Aperu!
 * Author URI:        https://aperu.net
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

if (! defined('ABSPATH')) {
    exit;
}

define('LOKO_DE_KROMPROGRAMO', __FILE__);


/**
 * === LA PROJEKTONOMO ===
 * Difinu la nomon de via projekto (uzu minusklojn).
 * La defaŭlto estas 'tubaro', kiun indas ŝanĝi.
 * LEGU LA ATENTIGON SUBE!
 */
define('PROJEKTNOMO', 'tubaro');
/**
 * === **ATENTU!** ===
 *
 * La projektnomo devas esti **NE PLI LONGA OL 14 SIGNOJ**, alikaze la sistemo
 *   ne funkcios!
 *
 * Tio estas, ĉar tiu ŝnuro estas uzata, interalie, kiel parto de nomo por la
 *   afiŝospeco (Post Type) por videoafiŝoj, al kiu estas aldonita la ŝnuro `_video`
 *   (6 signojn longa) por konstrui tiun nomon, kaj Vortpreso ne permesas uzi ŝnuron
 *   pli longan ol 20 signojn kiel nomon de afiŝospecoj.
 */


if (! defined('VERSIO')) {
    define('VERSIO', '231212');
}

if (! defined('KUKETO_KONSENTOTEMPO')) {
    define('KUKETO_KONSENTOTEMPO', 1660157694);
}

/**
 * Ĉu protokoli eventojn aŭ ne.
 * La protokolitaj eventoj estos uzataj por kalkuli popularecon.
 * Por malaktivigi, metu `false` kiel la valoron
 */
define('PROTOKOLI_EVENTOJN', true);


/**
 * Ĝisnunigi kaj refreŝigi la datumojn, tempumadon, ktp kiam la versio ŝanĝiĝas
 */
function a_spektejo_gxisnunigi()
{
    if (!current_user_can('administrator') || wp_doing_ajax()) {
        return;
    }

    if (get_option(PROJEKTNOMO . '_versio', '0') != VERSIO) {
        $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
        protokolu(null, 'La versio estis ŝanĝita. Ĝisnunigas la datumojn...', $protokoloID);

        do_action('a_spektejo_h_gxisnunigi');
        update_option(PROJEKTNOMO . '_versio', VERSIO);
    }
}
add_action('plugins_loaded', 'a_spektejo_gxisnunigi');

/**
 * Malpermesi aliron al la JSON REST API provizita de Vortpreso, por normalaj uzantoj kaj vizitantoj, ĉar ni ne bezonas ĝin
 */
function a_spektejo_malpermesi_neadministran_aliron_al_JSON_API($rezulto)
{
    if (true === $rezulto || is_wp_error($rezulto)) {
        return $rezulto;
    }

    if (!current_user_can('administrator')) {
        return new WP_Error(
            'rest_malpermesite',
            __('Vi ne havas aliron al ĉi tiu API'),
            ['status' => 401]
        );
    }

    return $rezulto;
}
add_filter('rest_authentication_errors', 'a_spektejo_malpermesi_neadministran_aliron_al_JSON_API');

/**
 * Ebligi protokoladon por sencimigado
 */
require plugin_dir_path(LOKO_DE_KROMPROGRAMO) . '/ene/protokolado.php';

/**
 * Difini la bazan datumoskemon
 */
require plugin_dir_path(LOKO_DE_KROMPROGRAMO) . '/datumostarigo.php';

/**
 * Aldoni funkciojn, kiuj provizas kromajn eblojn
 */
require plugin_dir_path(LOKO_DE_KROMPROGRAMO) . '/ene/helpofunkcioj.php';
// FIXME: Eble aldonu nur kiam administranto estas ensalutinta?
require plugin_dir_path(LOKO_DE_KROMPROGRAMO) . '/ene/admin/helpofunkcioj-admin.php';

/**
 * Aldoni funkciojn, kiuj uzas eksterajn programojn aŭ servojn
 */
require plugin_dir_path(LOKO_DE_KROMPROGRAMO) . '/aldonajxoj.php';

/**
 * Aldoni funkciojn pri tempumado kaj envicigi tempume-aktivigatajn funkciojn
 */
require plugin_dir_path(LOKO_DE_KROMPROGRAMO) . '/tempumado.php';

/**
 * Aldoni funkciojn, kiuj pritraktas agojn pri enhaveroj, ekz. afiŝoj, komentoj ktp.
 */
require plugin_dir_path(LOKO_DE_KROMPROGRAMO) . '/ene/enhaveroj.php';

/**
 * Aldoni funkciojn, kiuj pritraktas signalojn de uzantoj (ekz. raportoj pri problemoj aŭ kategorio-proponoj)
 */
require plugin_dir_path(LOKO_DE_KROMPROGRAMO) . '/ene/signaloj.php';

/**
 * Aldoni funkciojn, kiuj pritraktas envicigadon de eroj por ripetaj operacioj
 */
require plugin_dir_path(LOKO_DE_KROMPROGRAMO) . '/ene/envicigo.php';


/**
 * Protokoli la eventojn por kalkulado de Populareco
 */
require plugin_dir_path(LOKO_DE_KROMPROGRAMO) . 'ene/eventoprotokolo.php';


// FIXME: PORTEMPE
require plugin_dir_path(LOKO_DE_KROMPROGRAMO) . 'portempe/antaux-populareco.php';
