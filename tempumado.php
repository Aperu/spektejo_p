<?php

/**
 * Difini planojn por tempume alvoki funkciojn
 *
 * @package Spektejo
 */


/**
 * Difini tempumadojn ĉe WP-Cron
 */
function a_spektejo_difini_cron_intervalojn($planaro)
{
    $spektejo_planaro = [
        'kvaronhoro' => [
            'interval' => 900,
            'display'  => 'Ĉiu 15 minutoj',
        ],
        'duonhoro' => [
            'interval' => 1800,
            'display'  => 'Ĉiu 30 minutoj',
        ],
        'unu_horo' => [
            'interval' => 3600,
            'display'  => 'Ĉiu 1 horo',
        ],
        'du_horoj' => [
            'interval' => 7200,
            'display'  => 'Ĉiuj 2 horoj',
        ],
        'tri_horoj' => [
            'interval' => 10800,
            'display'  => 'Ĉiuj 3 horoj',
        ],
        'kvar_horoj' => [
            'interval' => 14400,
            'display'  => 'Ĉiuj 4 horoj',
        ],
        'kvin_horoj' => [
            'interval' => 18000,
            'display'  => 'Ĉiuj 5 horoj',
        ],
        'ses_horoj' => [
            'interval' => 21600,
            'display'  => 'Ĉiuj 6 horoj',
        ],
        'ok_horoj' => [
            'interval' => 28800,
            'display'  => 'Ĉiuj 8 horoj',
        ],
        'dekdu_horoj' => [
            'interval' => 43200,
            'display'  => 'Ĉiuj 12 horoj',
        ],
        'dudek_kvar_horoj' => [
            'interval' => 86400,
            'display'  => 'Ĉiuj 24 horoj',
        ],
    ];

    return [...$planaro, ...$spektejo_planaro];
}
add_filter('cron_schedules', 'a_spektejo_difini_cron_intervalojn');



/**
 * Aldoni tempume-planitajn funkciojn
 */
require plugin_dir_path(__FILE__) . '/tempume/pordumo.php';
// require plugin_dir_path(__FILE__) . '/tempume/poentigo.php';
// require plugin_dir_path(__FILE__) . '/tempume/enkategoriigo.php'; // FIXME: Temas pri prizorgi raportojn/signalojn. elektu pli taŭgan nomon.
require plugin_dir_path(__FILE__) . '/tempume/rekalkulo.php';
require plugin_dir_path(__FILE__) . '/tempume/renovigo.php';
// require plugin_dir_path(__FILE__) . '/tempume/purigo.php';



/**
 * Aldoni taskojn al la planaro de WP-Cron
 */

// Taskoj pri aldoni afiŝojn aŭtomate
if (! wp_next_scheduled('ago_pordumado')) {
    wp_schedule_event(time(), 'kvar_horoj', 'ago_pordumado');
}

// Taskoj pri poentigi/enkategoriigi afiŝojn aŭtomate
/* if (! wp_next_scheduled('ago_poentigado_por_videoj')) {
    wp_schedule_event(time(), 'unu_horo', 'ago_poentigado_por_videoj');
} */
if (! wp_next_scheduled('ago_rekalkulado_de_eble_rilataj_afisxoj')) {
    wp_schedule_event(time() + 930, 'unu_horo', 'ago_rekalkulado_de_eble_rilataj_afisxoj');
}
if (! wp_next_scheduled('ago_rekalkulado_de_rilataj_afisxoj')) {
    wp_schedule_event(time() + 1860, 'duonhoro', 'ago_rekalkulado_de_rilataj_afisxoj');
}
/* if (! wp_next_scheduled('ago_prizorgado_de_kategoriosignaloj')) {
    wp_schedule_event(time() + 2790, 'dekdu_horoj', 'ago_prizorgado_de_kategoriosignaloj');
} */
if (! wp_next_scheduled('ago_rekalkulado_de_plejuzataj_lingvoj')) {
    wp_schedule_event(time() + 3270, 'dudek_kvar_horoj', 'ago_rekalkulado_de_plejuzataj_lingvoj');
}
if (! wp_next_scheduled('ago_rekalkulado_de_statistikoj')) {
    wp_schedule_event(time() + 4200, 'dudek_kvar_horoj', 'ago_rekalkulado_de_statistikoj');
}

// Taskoj pri purigi kaj renovigi datumojn // FIXME: REAKTIVIGU
/* if (! wp_next_scheduled('ago_purigado')) {
    wp_schedule_event(time(), 'dudek_kvar_horoj', 'ago_purigado');
} */
if (!wp_next_scheduled('ago_renovigado')) {
    wp_schedule_event((time() + 1500), 'unu_horo', 'ago_renovigado');
}
if (!wp_next_scheduled('ago_datumoricevado_por_renovigado')) {
    wp_schedule_event(time() + 3200, 'kvar_horoj', 'ago_datumoricevado_por_renovigado');
}

// FIXME: PORTEMPE
if (!wp_next_scheduled('ago_elportado_de_laste_aktivaj_afisxoj')) {
    wp_schedule_event(time() + 3340, 'kvaronhoro', 'ago_elportado_de_laste_aktivaj_afisxoj');
}


// Purigi la tempumejon je aktivigo de la programaro
function a_spektejo_purigi_tempumajn_hokojn()
{
    // FIXME: Rekontrolu ĉion
    wp_clear_scheduled_hook('ago_pordumado');
    // wp_clear_scheduled_hook('ago_poentigado_por_videoj');
    // wp_clear_scheduled_hook('ago_prizorgado_de_kategoriosignaloj');
    wp_clear_scheduled_hook('ago_rekalkulado_de_eble_rilataj_afisxoj');
    wp_clear_scheduled_hook('ago_rekalkulado_de_rilataj_afisxoj');
    wp_clear_scheduled_hook('ago_rekalkulado_de_plejuzataj_lingvoj');
    wp_clear_scheduled_hook('ago_rekalkulado_de_statistikoj');
    // wp_clear_scheduled_hook('ago_purigado');
    wp_clear_scheduled_hook('ago_renovigado');
    wp_clear_scheduled_hook('ago_datumoricevado_por_renovigado');
}
register_activation_hook(LOKO_DE_KROMPROGRAMO, 'a_spektejo_purigi_tempumajn_hokojn');
add_action('a_spektejo_h_gxisnunigi', 'a_spektejo_purigi_tempumajn_hokojn');
