<?php

/**
 * Helpaj funkcioj
 *
 * @package Spektejo
 */

if (! defined('ABSPATH')) {
    exit;
}



/**
 * == TEKSTUMADO ==
 *
 * Funkcioj por pritrakti tekston
 */




/**
 * Forigi ligilojn kaj tro longajn "vortojn" el teksto
 * a_spektejo_prepari_tekstospecimenon
 *
 * @param string $teksto
 *
 * @return string
 */
function a_spektejo_prepari_tekstospecimenon($teksto)
{
    // forigi ret-adresojn
    $teksto = preg_replace('/[a-zA-Z]*[:\/\/]*[A-Za-z0-9\-_]+\.+[A-Za-z0-9\.\/%&=\?\-_]+/i', '', $teksto);

    // forigi "vortojn" pli grandajn ol 15 signoj
    $teksto = preg_replace('/\w{15,}/i', '', $teksto);

    return $teksto;
}




/**
 * Konverti iksoliterojn al ĉapelliteroj
 * a_spektejo_seniksigi_vorton
 *
 * @param string $vorto
 *
 * @return string
 */
function a_spektejo_seniksigi_vorton($vorto)
{

    $reguloj = [
        'cx' => 'ĉ',
        'gx' => 'ĝ',
        'hx' => 'ĥ',
        'jx' => 'ĵ',
        'sx' => 'ŝ',
        'ux' => 'ŭ',
        /*'Cx' => 'Ĉ',
        'Gx' => 'Ĝ',
        'Hx' => 'Ĥ',
        'Jx' => 'Ĵ',
        'Sx' => 'Ŝ',
        'Ux' => 'Ŭ',
        'CX' => 'Ĉ',
        'GX' => 'Ĝ',
        'HX' => 'Ĥ',
        'JX' => 'Ĵ',
        'SX' => 'Ŝ',
        'UX' => 'Ŭ',*/
    ];
    $vorto = strtr($vorto, $reguloj);

    return $vorto;
}





/**
 * Konverti iksoliterojn al ĉapelliteroj
 * a_spektejo_iksigi_vorton
 *
 * @param string $vorto
 *
 * @return string
 */
function a_spektejo_iksigi_vorton($vorto)
{

    $reguloj = [
        'ĉ' => 'cx',
        'ĝ' => 'gx',
        'ĥ' => 'hx',
        'ĵ' => 'jx',
        'ŝ' => 'sx',
        'ŭ' => 'ux',
        // 'Ĉ' => 'Cx',
        // 'Ĝ' => 'Gx',
        // 'Ĥ' => 'Hx',
        // 'Ĵ' => 'Jx',
        // 'Ŝ' => 'Sx',
        // 'Ŭ' => 'Ux',
        'Ĉ' => 'CX',
        'Ĝ' => 'GX',
        'Ĥ' => 'HX',
        'Ĵ' => 'JX',
        'Ŝ' => 'SX',
        'Ŭ' => 'UX',
    ];
    $vorto = strtr($vorto, $reguloj);

    return $vorto;
}





/**
 * Forigi Esperantajn finaĵojn el vortoj
 * Algoritmo danke al Vanege
 * a_spektejo_senfinajxigi_vorton
 *
 * @param string $vorto
 *
 * @return string
 */
function a_spektejo_senfinajxigi_vorton($vorto)
{
    if (preg_match('/.+ojn$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)ojn$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } elseif (preg_match('/.+ajn$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)ajn$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } elseif (preg_match('/.+on$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)on$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } elseif (preg_match('/.+an$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)an$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } elseif (preg_match('/.+en$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)en$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } elseif (preg_match('/.+oj$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)oj$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } elseif (preg_match('/.+aj$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)aj$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } elseif (preg_match('/.+is$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)is$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } elseif (preg_match('/.+as$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)as$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } elseif (preg_match('/.+os$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)os$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } elseif (preg_match('/.+us$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)us$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } elseif (preg_match('/.+u$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)u$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } elseif (preg_match('/.+i$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)i$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } elseif (preg_match('/.+ado$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)ado$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } elseif (preg_match('/.+o$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)o$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } elseif (preg_match('/.+a$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)a$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } elseif (preg_match('/.+e$/i', $vorto)) {
        $vorto_senfinajxa = preg_replace('/(.+)e$/i', '$1', $vorto);
        return ($vorto_senfinajxa ? $vorto_senfinajxa : $vorto);
    } else {
        return $vorto;
    }
}





/**
 * Kalkuli aĝon laŭ komenca tempo
 * a_spektejo_agxo
 *
 * @param int  $tempo
 * @param bool $aldoni_antaux
 *
 * @return string
 */
function a_spektejo_agxo($tempo, $aldoni_antaux = false)
{

    $agxo_s  = time() - $tempo;
    $agxo_m  = intval(floor($agxo_s / 60));      // minutoj
    $agxo_h  = intval(round($agxo_s / 3600));    // horoj
    $agxo_t  = intval(round($agxo_s / 86400));   // tagoj
    $agxo_mo = intval(round($agxo_s / 2592000)); // monatoj

    // kalkuli belan formaton
    if ($agxo_mo < 12) {                                     // malpli ol 12 monatoj
        if ($agxo_t < 30) {                                  // malpli ol 30 tagoj
            if ($agxo_h < 72) {                              // malpli ol 72 horoj
                if ($agxo_h >= 48) {                         // inter 48 kaj 72 horoj
                    $agxo_montro = 'antaŭhieraŭ';
                } elseif ($agxo_h >= 24) {                   // inter 24 kaj 48 horoj
                    $agxo_montro = 'hieraŭ';
                } else {                                     // malpli ol 24 horoj
                    if ($agxo_m >= 90) {                     // inter 90 minutoj kaj 24 horoj
                        $agxo_montro = $agxo_h . ' horoj';
                    } elseif ($agxo_m >= 60) {               // inter 60 kaj 90 minutoj
                        $agxo_montro = '1 horo';
                    } elseif ($agxo_m > 1) {                 // inter 1 kaj 60 minutoj
                        $agxo_montro = $agxo_m . ' minutoj';
                    } elseif ($agxo_s >= 45) {               // 1 minuto
                        $agxo_montro = ' minuto';
                    } else {                                 // malpli ol 1 minuto
                        $agxo_montro = 'ĵus';
                    }
                }
            } else {                                         // inter 72 horoj kaj 30 tagoj
                //$agxo_t = round($agxo_h / 24);
                $agxo_montro = $agxo_t . ' tagoj';
            }
        } elseif ($agxo_t < 45) {                            // inter 30 kaj 45 tagoj
            $agxo_montro = '1 monato';
        } else {                                             // pli ol 45 tagoj
            $agxo_montro = $agxo_mo . ' monatoj';
        }
    } elseif ($agxo_mo < 18) {                               // inter 12 kaj 18 monatoj
        $agxo_montro = '1 jaro';
    } else {                                                 // pli ol 18 monatoj
        $agxo_montro = round($agxo_mo / 12) . ' jaroj';
    }

    if ($aldoni_antaux) {
        if (strpos($agxo_montro, 'jaro')   !== false
            || strpos($agxo_montro, 'monato') !== false
            || strpos($agxo_montro, 'tago')   !== false
            || strpos($agxo_montro, 'horo')   !== false
            || strpos($agxo_montro, 'minuto') !== false
        ) {
            $agxo_montro = 'antaŭ ' . $agxo_montro;
        }
    }

    return $agxo_montro;
}








/**
 * == RETUMADO ==
 *
 * Funkcioj por sendi datumojn tra la interreto
 */



/**
 * Elŝuti JSON-datumaron el reta adreso kaj redoni ĝin kiel normalan aranĝaĵon
 * (uzas la funkcion `json_decode`)
 * a_spektejo_elsxuti_json
 *
 * @param string $adreso
 *
 * @return mixed  aranĝaĵo, se ĉio estas en ordo (rekte el `json_decode`)
 */
function a_spektejo_elsxuti_json($adreso)
{
    // $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO

    if (! isset($adreso) || empty($adreso)) {
        return false;
    }

    // protokolu($adreso, 'la adreso estas jene:', $protokoloID);

    $kunteksto = stream_context_create(
        ['http' => ['timeout' => 5]]
    );
    $rezulto = file_get_contents($adreso, 0, $kunteksto);

    // protokolu($rezulto, 'jen la rezulto:', $protokoloID);

    if (! $rezulto) {
        return false;
    }

    $datumoj = json_decode($rezulto, true);

    // protokolu($datumoj, 'jen la elkodigita rezulto:', $protokoloID);

    return $datumoj;
}





/**
 * Sendi retpoŝtmesaĝon al administranto
 * (uzas la Vortpresan `wp_mail`)
 * a_spektejo_sendi_retmesagxon_al_administranto
 *
 * @param string  $titolo
 * @param string  $mesagxo
 *
 * @return bool   rezulto rekte el `wp_mail`
 */
function a_spektejo_sendi_retmesagxon_al_administranto($titolo, $mesagxo)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton, mondo!', $protokoloID);
    protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);

    $domajno              = str_replace('www.', '', parse_url(home_url(), PHP_URL_HOST));
    $retejonomo           = get_bloginfo('name');
    $administra_retadreso = get_bloginfo('admin_email');
    if (empty($administra_retadreso)) {
        $administra_retadreso = 'admin@' . $domajno;
    }

    $mesagxo_kapo   = [];
    $mesagxo_kapo[] = 'From: ' . $retejonomo . ' <ne-respondu@' . $domajno . '>';

    protokolu($administra_retadreso, 'jen la retadreso de la administranto:', $protokoloID);
    protokolu($mesagxo_kapo, 'jen la mesaĝokapo:', $protokoloID);

    $rezulto = wp_mail(
        $administra_retadreso,
        wp_specialchars_decode($titolo),
        $mesagxo,
        $mesagxo_kapo
    );

    protokolu($rezulto, 'jen la rezulto:', $protokoloID);

    return $rezulto;
}








/**
 * == VIZITOJ ==
 *
 * Funkcioj por pritrakti datumojn pri vizitoj
 */



/**
 * Analizi la paĝoreferencilon kaj redoni informojn pri la vizitodeveno
 * a_spektejo_identigi_vizitodevenon
 *
 * @return array
 */
function a_spektejo_identigi_vizitodevenon()
{

    $fonto = [];

    $referencilo        = wp_get_referer();
    $referencilo_partoj = parse_url($referencilo);

    if (isset($referencilo_partoj['host'])) {
        $referencilo_retejo = $referencilo_partoj['host'];
        $cxitiu_retejo      = parse_url(site_url(), PHP_URL_HOST);
        $cxu_interna        = strpos($referencilo_retejo, $cxitiu_retejo);

        if ($cxu_interna === false) { //ekstera referencilo
            $fonto['deveno'] = 'ekstera';
            $fonto['retejo'] = $referencilo_retejo;
        } else { // interna referencilo
            $referencilo_adreso           = $referencilo_partoj['path'];
            //$referencilo_adreso         = parse_url($referencilo, PHP_URL_PATH);
            $referencilo_adreso_ero_fina  = basename(dirname($referencilo_adreso));
            $referencilo_adreso_ero_lasta = basename($referencilo_adreso);

            if ($referencilo_adreso_ero_lasta == 'sercxo') {
                $fonto['deveno'] = 'sercxo';
            } else {
                switch ($referencilo_adreso_ero_fina) {
                    case '':
                        $fonto['deveno'] = 'cxefpagxo';
                        break;
                    case 'v':
                        $fonto['deveno'] = 'video';
                        $fonto['video']  = $referencilo_adreso_ero_lasta;
                        break;
                    case 'k':
                        $fonto['deveno'] = 'kanalo';
                        $fonto['kanalo'] = $referencilo_adreso_ero_lasta;
                        break;
                    default:
                        $fonto['deveno'] = 'nekonata';
                }
            }
        }
    } else {
        $fonto['deveno'] = 'nekonata';
    }

    return $fonto;
}





/**
 * Krei anoniman identigilon por la vizitanto
 * a_spektejo_krei_anon_id
 *
 * @return string
 */
function a_spektejo_krei_anon_id()
{

    $retumilo = $_SERVER['HTTP_USER_AGENT'] ?? '';

    $ip_adreso = '';
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip_adreso =   $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip_adreso =   $_SERVER['HTTP_X_FORWARDED_FOR'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED'])) {
        $ip_adreso =   $_SERVER['HTTP_X_FORWARDED'];
    } elseif (!empty($_SERVER['HTTP_FORWARDED_FOR'])) {
        $ip_adreso =   $_SERVER['HTTP_FORWARDED_FOR'];
    } elseif (!empty($_SERVER['HTTP_FORWARDED'])) {
        $ip_adreso =   $_SERVER['HTTP_FORWARDED'];
    } elseif (!empty($_SERVER['REMOTE_ADDR'])) {
        $ip_adreso =   $_SERVER['REMOTE_ADDR'];
    } else {
        $ip_adreso = '';
    }

    // krei haketaĵon
    $identigilo_anon = hash('adler32', $ip_adreso . $retumilo);

    return $identigilo_anon;
}





/**
 * Determini la identigilon kaj la ensalutintecon de la vizitanto
 * a_spektejo_kiu_estas_tiu_cxi
 *
 * @param bool  $redoni_staton // FIXME: Forigu
 *
 * @return array
 */
function a_spektejo_kiu_estas_tiu_cxi($redoni_staton = false)
{

    if (is_user_logged_in()) {
        $anon   = false;
        $uzanto = wp_get_current_user()->user_login;
    } else {
        $anon   = true;
        $uzanto = a_spektejo_krei_anon_id();
    }

    $kiu = ($redoni_staton === true) ? ['uzanto' => $uzanto, 'anon' => $anon] : $uzanto;

    return $kiu;
}










/**
 * Determini, ĉu la uzanto havas specifan rajton, aŭ ne.
 * a_spektejo_cxu_uzanto_rajtas
 *
 * @param string $rajto    La rajto por kontroli
 *
 * @return bool            Ĉu la uzanto havas tiun rajton
 */
function a_spektejo_cxu_uzanto_rajtas($rajto)
{
    // $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton, mondo!', $protokoloID);
    // protokolu(null, 'Kontrolante la rajton ' . $rajto . ' por la uzanto #' . get_current_user_id() . '...', $protokoloID);

    if (!is_user_logged_in()) {
        return false;
    }

    if (current_user_can('administrator')) {
        return true;
    }

    $rajtoj = get_user_meta(get_current_user_id(), 'u_rajtoj', true);

    // protokolu($rajtoj, 'Jen la rajtoj por la uzanto: ', $protokoloID);

    if (is_array($rajtoj) && in_array($rajto, $rajtoj, true)) {
        return true;
    }

    return false;
}



/**
 * Determini, ĉu la uzanto havas specifan rolon (aŭ administran rolon), aŭ ne.
 * a_spektejo_cxu_uzanto_havas_rolon
 *
 * @param string $rolo    La rolo por kontroli
 *
 * @return bool           Ĉu la uzanto havas tiun rolon
 */
function a_spektejo_cxu_uzanto_havas_rolon($rolo)
{
    if (!is_user_logged_in()) {
        return false;
    }

    if (current_user_can('administrator')) {
        return true;
    }

    $uzanto = wp_get_current_user();
    $roloj = $uzanto->exists() ? $uzanto->roles : [];

    if (is_array($roloj) && in_array($rolo, $roloj, true)) {
        return true;
    }

    return false;
}










/**
 * Doni liston de eblaj kategorioj laŭ etikedoj
 * a_spektejo_proponi_kategoriojn_laux_etikedoj
 *
 * @param array $etikedaro
 *
 * @return array propona listo de kategorioj
 */
function a_spektejo_proponi_kategoriojn_laux_etikedoj($etikedaro)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton!', $protokoloID);
    // protokolu($etikedaro, 'jen la etikedaro:', $protokoloID);

    $cxefvortaro_loko = ABSPATH . 'datumoj/etikedoj-kategorioj.json';
    if (! is_file($cxefvortaro_loko)) {
        // protokolu(null, 'la dosiero ne ekzistas!', $protokoloID);

        return false;
    }

    $cxefvortaro_json = file_get_contents($cxefvortaro_loko);
    if (empty($cxefvortaro_json)) {
        // protokolu(null, 'la dosiero malplenas!', $protokoloID);

        return false;
    }
    $cxefvortaro  = json_decode($cxefvortaro_json, true);
    if (empty($cxefvortaro)) {
        // protokolu(null, 'ne eblas elkodigi la dosieron!', $protokoloID);

        return false;
    }

    $kategorioj = [];

    foreach ($cxefvortaro as $kategorio => $cxefvortoj) {
        // protokolu(null, 'kontrolas la kategorion ' . $kategorio . '...', $protokoloID);
        // protokolu($cxefvortoj, 'la ĉefvortoj estas jene:', $protokoloID);

        foreach ($cxefvortoj as $cxefvorto) {
            if (in_array($kategorio, $kategorioj, true)) {
                // protokolu(null, 'la kategorio estas jam aldonita al la proponoj. kontrolas la sekvan kategorion...', $protokoloID);

                continue 2;
            }
            foreach ($etikedaro as $etikedo) {
                if ($etikedo == $cxefvorto) {
                    // protokolu(null, 'la etikedo ' . $etikedo . ' kongruas kun la ĉefvorto ' . $cxefvorto . '. aldonas la kategorioj al la listo de proponoj...', $protokoloID);

                    $kategorioj[] = $kategorio;
                }
            }
        }
    }

    // protokolu($kategorioj, 'finis la kontroladon. jen la proponaj kategorioj:', $protokoloID);

    return $kategorioj;
}





/**
 * Pretigi krudajn etikedojn por aldoni ilin en la datumbazon
 * (minuskligas, iksigas, senfinaĵigas, forigas signojn ktp)
 * a_spektejo_prepari_etikedaron
 *
 * @param array $kruda_etikedaro
 *
 * @return array
 */
function a_spektejo_prepari_etikedaron($kruda_etikedaro = [])
{

    if (! is_array($kruda_etikedaro) || empty($kruda_etikedaro)) {
        return [];
    }

    $etikedaro = [];

    $esceptaj_vortoj_loko = ABSPATH . 'datumoj/senfinajxigo_esceptoj'; // listo de esceptoj danke al Vanege // FIXME: Ŝanĝu la lokon
    $esceptaj_vortoj      = file($esceptaj_vortoj_loko, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

    if ($esceptaj_vortoj === false) {
        $esceptaj_vortoj = [];
    } else {
        $esceptaj_vortoj = array_map('mb_strtolower', $esceptaj_vortoj);
    }

    foreach ($kruda_etikedaro as $kruda_etikedo) {
        // forigi neliterajn signoj, ekz. # ! ktp
        $kruda_etikedo = preg_replace('/[\W_]+/iu', ' ', $kruda_etikedo);

        // minuskligi
        $kruda_etikedo = mb_strtolower($kruda_etikedo);

        // iksigi
        $kruda_etikedo = a_spektejo_iksigi_vorton($kruda_etikedo);

        // apartigi vortojn laŭ spaceto
        $etikedo_vortoj = explode(' ', $kruda_etikedo);

        foreach ($etikedo_vortoj as $vorto) {
            if (in_array($vorto, $esceptaj_vortoj, true) || strlen($vorto) < 3) {
                continue;
            } // ignoru la esceptojn kaj mallongajn vortojn, ne aldonu ilin kiel etikedon

            $etikedo = a_spektejo_senfinajxigi_vorton($vorto);

            $etikedoj_ignorataj = [
                'esperant',
                'th',
                'us',
                'yo',
                'qu',
                'bo',
                'me',
                'mi',
                'cx',
                'ho',
                'and',
                'est',
                'what'
            ];
            if (strlen($etikedo) > 1 && !in_array($etikedo, $etikedoj_ignorataj, true)) {
                $etikedaro[] = $etikedo;
            }
        }
    }

    // forigi duoblaĵojn
    $etikedaro = array_values(array_unique($etikedaro));

    return $etikedaro;
}









/**
 * == KUKETOJ ==
 * funkcioj pri aldoni kuketojn
 */




/**
* Krei permesan kuketon je peto
* a_spektejo_aldoni_konsentan_kuketon
*
* @param int   $identigilo
*
* @return array // FIXME
*/
function a_spektejo_aldoni_konsentan_kuketon($konsentaro)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu($konsentaro, 'jen la kuketo-konsentoj ricevitaj:', $protokoloID);

    $kuketoparametroj  = [
        'expires' => time() + 31536000, // unu jaro ekde nun
        'path'    => COOKIEPATH,
        'domain'  => COOKIE_DOMAIN,
        // 'secure' => true,
        'httponly' => false,
        'samesite' => 'strict',
    ];

    if (! isset($_COOKIE['konsento_gxenerala'])) {
        setcookie('konsento_gxenerala', 'jes', $kuketoparametroj);
    }

    $kuketo_specoj = [
        'eksteraj' => [],
        'preferoj' => ['prefero', 'legitaj_mesagxoj'],
        'kolektoj' => ['kolektoj']
    ];

    foreach ($kuketo_specoj as $speco => $prefiksoj) {
        if (in_array($speco, $konsentaro)) {
            setcookie('konsento_' . $speco, 'jes', $kuketoparametroj); // por unu jaro
        } else {
            setcookie('konsento_' . $speco, 'ne', $kuketoparametroj); // por unu jaro
            // forigi ĉiujn kuketojn de tiu speco
            foreach (array_keys($_COOKIE) as $kuketo_nomo) {
                foreach ($prefiksoj as $prefikso) {
                    if (strpos($kuketo_nomo, $prefikso) === 0) {
                        unset($_COOKIE[$kuketo_nomo]);
                        setcookie($kuketo_nomo, '', array_merge($kuketoparametroj, ['expires' => time() - 86400 /* pasintece */]));
                    }
                }
            }
        }
    }

    // konservi konsento-tempon
    setcookie('konsento_tempo', strval(time()), $kuketoparametroj);

    return [
        'mesagxo_sukcesa' => 'Kuketo kreite!',
        'kodo_sukcesa'    => 'sukcese_kreis_kuketon'
    ];
}







/**
 * == KTP ==
 * funkcioj pri aldoni kuketojn
 */









/**
 * Kalkuli popularecon por videoafiŝo
 * a_spektejo_kalkuli_popularecon
 *
 * @param int $identigilo
 *
 * @return int la kalkulita populareco
 */
function a_spektejo_kalkuli_popularecon($identigilo)
{

    // legi spektojn, komentonombron kaj voĉoj
    $spektajxoj  = intval(get_post_meta($identigilo, 'v_spektajxoj', true));
    $vocxoj      = intval(get_post_meta($identigilo, 'v_vocxoj', true));
    $komentoj_nr = intval(get_comments_number($identigilo));

    // kalkuli la popularecon
    $populareco  = $spektajxoj + (3 * $komentoj_nr) + (2 * $vocxoj);

    return $populareco;
}
