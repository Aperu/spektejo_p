<?php

/**
 *    EVENTO        ALVOKOMANIERO   LOKO           PRISKRIBO
 * ----------------------------------------------------------------------------------------------------------------------------
 * - `vizito`            hoke       ĈI TIE         vizito de la paĝo, deveninte de ie ajn
 * - `vizito_deekstere`  hoke       ĈI TIE         vizito de la paĝo, deveninte de ekster Tubaro
 * - `referenco`         hoke       ĈI TIE         esti la referencinto por alia paĝo ĉe Tubaro
 * - `komento`           hoke       ĈI TIE         aldono de komento al la videoafiŝo
 * - `apero`             ----                      apero kiel ligilo en listoj (ĉefa listo, rilataj afiŝoj, serĉorezultoj ktp)
 * - `diskonigo`         js         video.js       klako sur la diskonigobutono de la videoafiŝo
 * - `ekludo`            js         video.js       vizito dum kiu okazis almenaŭ unu klako sur la videokadro
 * - `spekto`            enfunkcie  enhaveroj.php  klako sur la videokadro kaj restado sur la paĝo dum difinita minimuma tempo
 * - `sxato`             enfunkcie  enhaveroj.php  aldono de ŝato al la videoafiŝo
 * - `malsxato`          enfunkcie  enhaveroj.php  aldono de malŝato al la videoafiŝo
 * - `kategoriosignalo`  enfunkcie  signaloj.php   aldono de signalo por proponi kategorion por la videoafiŝo
 */




// Prepari konekton al la eventodatumbazo
// ATENTU: la funkcioj, kiuj alvokas ĉi tiun funkcion, devos mem fermi la konekton
// (krei ĝin se necese, krei konekton al ĝi, kaj krei la tabelon se necese)
function a_spektejo_eventodatumbazo()
{
    $loko = ABSPATH . 'datumoj/eventoj.db';
    if (!is_writable(dirname($loko))) {
        return null;
    }
    // Krei konekton
    $datumbazo = new PDO('sqlite:' . $loko); // Ĉi tio kreos la dosieron se ĝi ne ekzistas
    $datumbazo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // Krei la tabelon, se ĝi ne ekzistas
    $sql = "CREATE TABLE IF NOT EXISTS eventoj (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    evento TEXT,
    aginto TEXT,
    afisxo_id INTEGER,
    tempo INTEGER
    )";
    $datumbazo->exec($sql);
    return $datumbazo;
}






// pritrakti la eventojn `vizito`, `vizito_deekstere` kaj `referenco`
function a_spektejo_procedi_viziton()
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO

    $retumilo = $_SERVER['HTTP_USER_AGENT'] ?? '';
    if (empty($retumilo)) {
        return;
    }

    // ignori la retumilo-identigilojn de konataj robotoj
    $konataj_robotoj = [
        'adscanner',
        'ahrefsbot',
        'ahrefssiteaudit',
        'antbot',
        'applebot',
        'archive.org_bot',
        'awariobot',
        'baiduspider',
        'barkrowler',
        'bingbot',
        'blexbot',
        'bubing',
        'bytespider',
        'checkmarknetwork',
        'cincraw',
        'claudebot',
        'coccocbot',
        'dataforseobot',
        'discordbot',
        'dnbcrawler',
        'domainstatsbot',
        'dotbot',
        'exabot',
        'facebookexternalhit',
        'googlebot',
        'googleimageproxy',
        'ia_archiver',
        'ioncrawl',
        'jobboersebot',
        'linkbot',
        'linkdexbot',
        'linkfluence',
        'livelapbot',
        'mail.ru_bot',
        'mediapartners',
        'megaindex',
        'mj12bot',
        'msnbot',
        'petalbot',
        'seekportbot',
        'semrushbot',
        'seokicks',
        'seoscanners',
        'serpstatbot',
        'seznambot',
        'slackbot',
        'slurp',
        'sogou',
        'synapse',
        'telegrambot',
        'trendictionbot',
        'ttd-content',
        'twitterbot',
        'yandexbot',
        'yandeximages',
        'y!j-asr',
        'y!j-wsc',
        'zoombot',
    ];
    foreach ($konataj_robotoj as $roboto_id) {
        if (stripos($retumilo, $roboto_id) !== false) {
            // protokolu(null, 'ignoras la viziton, ĉar verŝajne ĝi venas de la roboto ' . $roboto_id . '.', $protokoloID);
            // protokolu(null, 'la retumilo-identigilo estas: ' . $retumilo, $protokoloID);
            return;
        }
    }

    $identigilo   = get_queried_object_id();
    $referencilo  = a_spektejo_identigi_vizitodevenon();
    $vizitodeveno = $referencilo['deveno'] ?? '';

    if (is_singular(PROJEKTNOMO . '_video')) {
        // vizito al videopaĝo
        a_spektejo_protokoli_eventon('vizito', $identigilo);

        if ($vizitodeveno == 'ekstera') {
            a_spektejo_protokoli_eventon('vizito_deekstere', $identigilo);
        }
    }

    if ($vizitodeveno == 'video' && !empty($referencilo['video'])) {
        // poentigi la vizitoreferencintan videoafiŝon
        $referencinto_nometo = $referencilo['video'] ?? '';

        $afisxo = get_posts(
            [
                'post_type'   => PROJEKTNOMO . '_video',
                'name'        => $referencinto_nometo,
                'post_status' => ['publish', 'pending'],
                'fields'  => 'ids',
                'numberposts' => 1
            ]
        );
        $referencinto = $afisxo[0] ?? 0;
        if ($referencinto != $identigilo) {
            a_spektejo_protokoli_eventon('referenco', $referencinto);
        }
    }
}
add_action('wp_head', 'a_spektejo_procedi_viziton');







// pritrakti la eventon `komento`
function a_spektejo_protokoli_komenton($komentoID, $komento)
{
    $afisxo_id = $komento->comment_post_ID;
    a_spektejo_protokoli_eventon('komento', $afisxo_id);
}
add_action('wp_insert_comment', 'a_spektejo_protokoli_komenton', 99, 2);





// pritrakti la eventojn `ekludo` kaj `diskonigo` tra AJAX-petoj
function a_spektejo_ajax_pritrakti_eventoprotokolon()
{
    // 1. kontroli sekurecon

    if (! check_ajax_referer('spektejo_ajax', 'fojvorto')) {
        wp_send_json_error(
            [
                'mesagxo' => 'Ne eblas konfirmi la sekurecon!'
            ]
        );
    }

    // $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton!', $protokoloID);


    // 2. inici kaj kontroli la datumojn

    $datumoj = (isset($_POST['datumoj']) ? json_decode(html_entity_decode(stripslashes($_POST['datumoj'])), true) : []);

    // protokolu($datumoj, 'jen la datumoj:', $protokoloID);

    if (empty($datumoj)) {
        wp_send_json_error(
            [
                'mesagxo' => 'Eraro: malplena datumaro!',
                'kodo'    => 'datumaro_malplena'
            ]
        );
    }

    $identigilo  = (isset($datumoj['identigilo']))  ? intval($datumoj['identigilo'])      : 0;
    $evento      = (isset($datumoj['evento']))      ? sanitize_key($datumoj['evento'])    : '';
    /* $cxu_respondu = (isset($datumoj['cxu_respondu'])) ? $datumoj['cxu_respondu']       : ''; */

    if (empty($evento)) {
        wp_send_json_error(
            [
                'mesagxo' => 'Eraro: nedifinita evento!',
                'kodo'    => 'evento_malplena'
            ]
        );
    }
    if (! empty($erospeco) && $identigilo <= 0) {
        wp_send_json_error(
            [
                'mesagxo' => 'Eraro: nevalida identigilo!',
                'kodo'    => 'identigilo_nevalida'
            ]
        );
    }


    // 3. alvoki la funkcion
    switch ($evento) {
        case 'ekludo':
            a_spektejo_protokoli_eventon('ekludo', $identigilo);
            break;

        case 'diskonigo':
            a_spektejo_protokoli_eventon('diskonigo', $identigilo);
            break;

        default:
            wp_send_json_error(
                [
                    'mesagxo' => 'Eraro: Nekonata evento!',
                    'kodo'    => 'evento_malvalida'
                ]
            );
    }
}
add_action('wp_ajax_pritrakti_eventoprotokolon', 'a_spektejo_ajax_pritrakti_eventoprotokolon');
add_action('wp_ajax_nopriv_pritrakti_eventoprotokolon', 'a_spektejo_ajax_pritrakti_eventoprotokolon');



// Protokoli la eventon en la datumbazon
function a_spektejo_protokoli_eventon($evento, $afisxo_id)
{
    if (! PROTOKOLI_EVENTOJN) {
        return;
    }

    // Ne protokoli la eventon, se la afiŝo estas neaprobita aŭ kaŝita
    $rilateco = wp_get_post_terms($afisxo_id, 'v_rilateco')[0]->slug;
    if (get_post_status($afisxo_id) !== 'publish' || !in_array($rilateco, ['esperanta', 'priesperanta', 'nedifina'], true)) {
        return;
    };

    $datumbazo = a_spektejo_eventodatumbazo();
    if (!$datumbazo) {
        return; // FIXME: Montri eraron al la administranto
    }

    $aginto = a_spektejo_kiu_estas_tiu_cxi();
    $tempo = time();

    $sql = "INSERT INTO eventoj (evento, aginto, afisxo_id, tempo) VALUES (?, ?, ?, ?)";
    $inst = $datumbazo->prepare($sql);
    $inst->execute([$evento, $aginto, $afisxo_id, $tempo]);

    // Fermi la konekton al la datumbazo
    $datumbazo = null;
}


// Tempumi la elportadon de popularecaj listoj // TODO!
/* if (! wp_next_scheduled('ago_elporti_hierauxan_eventorpotokolajxojn')) {
    wp_schedule_event(time() + 960, 'dekdu_horoj', 'ago_elporti_hierauxan_eventorpotokolajxojn');
} */
// add_action('ago_elporti_hierauxan_eventorpotokolajxojn', 'a_spektejo_elporti_hierauxan_eventoprotokolajxon');
/* function a_spektejo_elporti_hierauxan_eventoprotokolajxon()
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO

    $dato          = getdate(time());
    $hieraux       = ($dato['yday'] - 1);
    $kampo_hieraux = PROJEKTNOMO . '_ptmp__eventoj22_' . $hieraux;

    $eventoj_hieraux = get_option($kampo_hieraux, []);

    if (empty($eventoj_hieraux)) {
        protokolu(null, 'hieraŭaj eventoj ne troviĝas. nenio por fari.', $protokoloID);
        return;
    }

    $eventoj_hieraux_csv = '# evento,aginto,afisxo,tempo' . "\r\n";
    foreach ($eventoj_hieraux as $evento) {
        $eventoj_hieraux_csv .= '"' . $evento['speco'] .     '",';
        $eventoj_hieraux_csv .= '"' . $evento['aginto'] .    '",';
        $eventoj_hieraux_csv .= $evento['afisxo_id'] . ',';
        $eventoj_hieraux_csv .= $evento['tempo'] .  "\r\n";
    }

    protokolu($eventoj_hieraux_csv, 'protokolos la jenan CSV-enhavon:', $protokoloID);

    $dosiero_loko = ABSPATH . 'eventoj2201/22_' . $hieraux . '.csv';
    file_put_contents($dosiero_loko, $eventoj_hieraux_csv);
} */
