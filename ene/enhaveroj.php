<?php

/**
 * Funkcioj rilataj al operacioj pri enhaveroj (ekz. videoafiŝoj, kanaloj, komentoj) kaj kromdatumoj
 *
 * @package Spektejo
 */



/* == KREADO, FORIGADO == */


/**
 * Krei videoafiŝon
 * (uzas la Vortpresan `wp_insert_post`)
 * a_spektejo_krei_videoafisxon
 *
 * @param array  $datumoj
 * @param bool   $cxu_publike
 *
 * @return array  raporto pri la rezulto
 */
function a_spektejo_krei_videoafisxon($datumoj, $cxu_publike = false)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton, mondo!', $protokoloID);
    protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);

    // 1. Kontroli la enigaĵon
    if (empty($datumoj) || ! is_array($datumoj)) {
        return false;
    }


    // 2. Prepariĝi

    // 2.1. Malaktivigi la limojn por la titoloj por ne ŝanĝi majusklojn en nometoj
    remove_filter('sanitize_title', 'sanitize_title_with_dashes');
    remove_filter('sanitize_title', 'a_spektejo_ebligi_majusklon_punktojn_streketojn_en_titoloj');

    // 2.2. Prepari la datumojn
    $v_identigilo    = $datumoj['identigilo'];
    $v_titolo        = $datumoj['titolo'];
    $v_etikedoj      = $datumoj['etikedoj'];
    $v_priskribo     = $datumoj['priskribo'];
    $v_kanalo        = $datumoj['kanalo'];
    $v_auxtoro       = $datumoj['auxtoro'];
    $v_fonto         = $datumoj['fonto'];
    $v_fonto_kodo    = $datumoj['fonto-kodo'];
    $v_dauxro        = $datumoj['dauxro'];
    $v_dato          = $datumoj['dato'];
    $v_lingvo        = $datumoj['lingvo'];
    $v_rilateco      = $datumoj['rilateco'];

    $v_nometo        = $v_fonto_kodo . '_' . $v_identigilo;
    $v_kanalo_nometo = $v_fonto_kodo . '_' . $v_kanalo;

    // $v_agxo          = time() - strtotime($v_dato); // ne uzata
    // $v_agxo_t        = round($v_agxo / 86400); // ne uzata

    // kontroli, ĉu temas pri vivelsendaĵo
    if (empty($v_dauxro) || $v_dauxro == 'PT0S') {
        return [
            'mesagxo_malsukcesa' => 'Eraro: La daŭro de la filmo estas nulo. Eble ĝi estas vivelsendata. Reprovu poste.',
            'kodo_malsukcesa'    => 'dauxro_nulas'
        ];
    }

    // 2.3. Pritrakti kanalon
    // krei kiel novan terminon, se ĝi ankoraŭ ne ekzistas
    $v_kanalo_termino = get_term_by('slug', $v_kanalo_nometo, 'v_kanalo');
    if (!$v_kanalo_termino) {
        // krei la kanalon
        $v_kanalo_termino    = wp_insert_term($v_auxtoro, 'v_kanalo', ['slug' => $v_kanalo_nometo]);
        $v_kanalo_termino_id = intval($v_kanalo_termino['term_id']);
        // aldoni kromdatumojn
        add_term_meta($v_kanalo_termino_id, 'v_kanalo_inkluziveco', 'normala', true);
        add_term_meta($v_kanalo_termino_id, 'v_kanalo_fonto', $v_fonto, true);
        add_term_meta($v_kanalo_termino_id, 'v_kanalo_identigilo_fonto', $v_kanalo, true);
    } else {
        $v_kanalo_termino_id = $v_kanalo_termino->term_id;
    }


    // 2.4. Pritrakti lingvon kaj rilatecon
    // krei novan lingvoterminon, se ĝi ankoraŭ ne ekzistas
    $lingvoj = a_spektejo_retrovi_terminoliston('v_lingvo');
    if (!empty($v_lingvo) && isset($lingvoj[$v_lingvo])) {
        // krei la lingvo-terminon
        $v_lingvo_nomo = $lingvoj[$v_lingvo]['nomo'];
        wp_insert_term($v_lingvo_nomo, 'v_lingvo', ['slug' => $v_lingvo]);
    } else {
        $v_lingvo == 'xx'; // nekonata lingvo
    }


    // 2.5. Pritrakti la etikedojn kaj kategoriojn
    // eltiri kradvortojn el la priskribo
    $kradvortoj = [];
    preg_match_all("/#\w+/iu", $v_priskribo, $kradvortoj);

    protokolu($kradvortoj, 'jen la kradvortoj:', $protokoloID);


    // prepari etikedojn
    $v_etikedoj_preparitaj = a_spektejo_prepari_etikedaron(array_merge($v_etikedoj, [$v_titolo], ($kradvortoj[0])));

    // proponi kategoriojn
    $v_kategorioj_laux_etikedoj = a_spektejo_proponi_kategoriojn_laux_etikedoj($v_etikedoj_preparitaj);
    $v_kategorioj_plejoftaj_de_kanalo_kun_kvantoj = get_term_meta($v_kanalo_termino_id, 'v_kanalo_plejoftaj_kategorioj', true);
    $v_kategorioj_plejoftaj_de_kanalo = (!empty($v_kategorioj_plejoftaj_de_kanalo_kun_kvantoj) && is_array($v_kategorioj_plejoftaj_de_kanalo_kun_kvantoj)) ? array_keys($v_kategorioj_plejoftaj_de_kanalo_kun_kvantoj) : [];
    $v_kategorioj_proponeblaj = array_unique(array_merge($v_kategorioj_laux_etikedoj, $v_kategorioj_plejoftaj_de_kanalo));


    // 2.6. Pritrakti la afiŝostaton kaj proponitecon
    if ($cxu_publike) {
        $v_afisxostato = 'publish';
        $proponiteco   = 'ne'; // FIXME: Ĉu necese?
    } else {
        $v_afisxostato = 'pending';
        $proponiteco   = 'jes';
    }



    // 3. Konstrui la afiŝodatumaron

    $v_datumoj_nova = [
        'post_title'   => wp_strip_all_tags($v_titolo),
        'post_type'    => PROJEKTNOMO . '_video',
        'post_content' => wp_strip_all_tags($v_priskribo),
        'post_name'    => $v_nometo,
        'post_status'  => $v_afisxostato,
        'meta_input'   => [
            'v_alsxutdato'       => $v_dato,
            'v_dauxro'           => $v_dauxro,
            'v_identigilo_fonto' => $v_identigilo,
            'v_proponita'        => $proponiteco,
            'v_spektajxoj'       => 0,
            'v_vocxoj'           => 0,
            'v_populareco'       => 0,
            'v_komentoamplekso'  => 0,
            // 'v_komentintoj_n'    => 0,
            'v_komento_lasta'    => 0,
            // 'v_enkategoriigoj'   => 0, // FIXME: Ĉu ne plu necesas?
            'v_afisxoj_rilataj'  => [],
            'v_etikedoj_krude'   => implode(',', $v_etikedoj),
            'v_laste_renovigita' => time()
        ]
    ];

    // Pritrakti afiŝoaŭtoron
    $kiu      = a_spektejo_kiu_estas_tiu_cxi(true);
    $uzanto   = $kiu['uzanto'];
    $anon     = $kiu['anon'];
    if ($anon) {
        // la proponinto ne ensalutis. atribui la defaŭltan uzanton al la afiŝo (se ĝi ekzistas)
        $v_uzanto = get_user_by('login', PROJEKTNOMO);
        if ($v_uzanto != false) {
            $v_datumoj_nova['post_author'] = $v_uzanto->ID; // NOVA
        }
        // $v_datumoj_nova['meta_input']['v_proponinto'] = $uzanto;
    } else {
        $v_datumoj_nova['post_author'] = $uzanto;
    }

    protokolu($v_datumoj_nova, 'jen la preparitaj videodatumoj:', $protokoloID);



    // 5. Aldoni la videoafiŝon en la datumbazon
    $v_afisxo_n_id = wp_insert_post($v_datumoj_nova, true);

    protokolu($v_afisxo_n_id, 'jen la identigilo:', $protokoloID);

    // 6. Proponi kategoriojn
    if (! empty($v_kategorioj_proponeblaj)) {
        a_spektejo_aldoni_signalon('video', $v_afisxo_n_id, 'kategorio', $v_kategorioj_proponeblaj, ['uzanto' => PROJEKTNOMO]);
    }

    // 7. Atribui la taksonomiojn
    $taksonomioj = [
        'v_lingvo'     => $v_lingvo, // ŜANĜITA
        'v_fonto'      => $v_fonto,
        'v_rilateco'   => $v_rilateco,
        'v_kanalo'     => $v_kanalo_nometo,
        // 'v_kategorioj' => $v_kategorioj_proponeblaj,
        'v_etikedoj'   => $v_etikedoj_preparitaj,
    ];
    foreach ($taksonomioj as $taksonomio => $terminaro) {
        wp_set_object_terms($v_afisxo_n_id, $terminaro, $taksonomio);
    }

    // ĝisnunigi la kanalon
    a_spektejo_gxisnunigi_modifodaton_por_kanalo($v_kanalo_nometo);
    a_spektejo_gxisnunigi_kanalograndecon($v_kanalo_nometo);
    a_spektejo_gxisnunigi_plejoftajn_kategoriojn_por_kanalo($v_kanalo_nometo);


    // 8. Raporti pri la rezulto
    if (is_wp_error($v_afisxo_n_id)) {
        return [
            'mesagxo_malsukcesa' => 'Eraro: Ne eblas aldoni afiŝon en la datumbazon!',
            'kodo_malsukcesa'    => 'malsukcesis_aldoni_videon'
        ];
    } else {
        if (current_user_can('administrator') || $cxu_publike === true) {
            return [
                'mesagxo_sukcesa' => 'Enorde. La filmo estas publike videbla en la listo.',
                'kodo_sukcesa'    => 'sukcese_aldonis_videon',
                'operacio'        => 'krei_videoafisxon',
                'identigilo'      => $v_afisxo_n_id
            ];
        } else {
            return [
                'mesagxo_sukcesa' => 'Enorde. Via proponita filmo estos publike videbla en la listo post aprobo de administranto.',
                'kodo_sukcesa'    => 'sukcese_proponis_videon',
                'operacio'        => 'proponi_videoafisxon',
                'identigilo'      => $v_afisxo_n_id
            ];
        }
    }
}





/**
 * Aldoni videon kiel proponon
 * a_spektejo_proponi_videon
 *
 * @param array $parametroj
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_proponi_videon($parametroj)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton, mondo!', $protokoloID);
    protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);

    // 1. Prepari la datumojn
    if (! isset($parametroj)
        || ! is_array($parametroj)
        || empty($parametroj)
        || ! isset($parametroj['fonto'])
        || empty($parametroj['fonto'])
        || ! isset($parametroj['adreso'])
        || empty($parametroj['adreso'])
        || ! isset($parametroj['lingvo'])
        || empty($parametroj['lingvo'])
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }


    $pv_fonto    = $parametroj['fonto'];
    $adreso      = esc_url($parametroj['adreso']);
    $pv_rilateco = sanitize_text_field($parametroj['lingvo']);

    if ($pv_rilateco !== 'esperanta' && $pv_rilateco !== 'priesperanta') {
        return [
            'mesagxo_malsukcesa' => 'Nekonata lingvo!',
            'kodo_malsukcesa'    => 'nekonata_rilateco'
        ];
    }

    /*
    $kiu      = a_spektejo_kiu_estas_tiu_cxi(true);
    $uzanto   = $kiu['uzanto'];
    $anon     = $kiu['anon'];
    */

    // 2. Kontroli pri jama ekzisto
    // eltiri la ĉeplatforman identigilon
    switch ($pv_fonto) {
        case 'youtube':
            $pv_fonto_kodo           = 'ytb';
            $pv_identigilo_platforma = a_spektejo_eltiri_videoidentigilon_el_adreso_ytb($adreso);
            break;
        default:
            return [
                'mesagxo_malsukcesa' => 'Nekonata platformo!',
                'kodo_malsukcesa'    => 'nekonata_fonto'
            ];
    }

    // kontroli, ĉu la afiŝo jam ne ekzistas en la datumbazo
    $arg = [
        'post_type'   => PROJEKTNOMO . '_video',
        // 'name'        => $pv_fonto_kodo . '_' . $pv_identigilo_platforma,
        'meta_key'    => 'v_identigilo_fonto',
        'meta_value'  => $pv_identigilo_platforma,
        'post_status' => ['publish', 'pending'],
        // 'fields'  => 'ids',
        'numberposts' => 1
    ];
    $samnomaj_afisxoj = get_posts($arg);

    protokolu($arg, 'la argumentoj por serĉi samnomajn afiŝojn estas jene:', $protokoloID);
    protokolu($samnoma_afisxo, 'la trovitaj samnomaj afiŝoj estas jene:', $protokoloID);

    if (! empty($samnomaj_afisxoj)) {
        $samnoma_afisxo = $samnomaj_afisxoj[0];
        $afisxo_ID      = $samnoma_afisxo->ID;
        $afisxo_stato   = $samnoma_afisxo->post_status;

        if ($afisxo_stato == 'pending') {
            return [
                'mesagxo_malsukcesa' => 'Tiu filmo estas proponita antaŭe kaj atendas aprobon de administranto.',
                'kodo_malsukcesa'    => 'propono_atendas_aprobon'
            ];
        }

        $afisxo_proponiteco = get_post_meta($afisxo_ID, 'v_proponita', true);
        $afisxo_rilateco    = wp_get_post_terms($afisxo_ID, 'v_rilateco')[0]->slug;

        if ($afisxo_rilateco == 'spama') {
            return [
                'mesagxo_malsukcesa' => 'Via propono estas malakceptita, ĉar la filmo "' . $pv_identigilo_platforma . '" estas markita kiel spamaĵo.',
                'kodo_malsukcesa'    => 'propono_spama'
            ];
        }
        if ($afisxo_rilateco == 'nerilata') {
            return [
                'mesagxo_malsukcesa' => 'Via propono estas malakceptita, ĉar la filmo "' . $pv_identigilo_platforma . '" estas markita kiel nerilata/maltaŭga.',
                'kodo_malsukcesa'    => 'propono_nerilata'
            ];
        }
        if ($afisxo_rilateco == 'nedisponebla') {
            return [
                'mesagxo_malsukcesa' => 'Via propono estas malakceptita, ĉar la filmo "' . $pv_identigilo_platforma . '" estas markita kiel nedisponebla.',
                'kodo_malsukcesa'    => 'propono_nedisponebla'
            ];
        }

        if ($afisxo_proponiteco = 'jes') {
            return [
                'mesagxo_malsukcesa' => 'La filmo estas jam proponita.',
                'kodo_malsukcesa'    => 'propono_duobla'
            ];
        }

        return [
            'mesagxo_malsukcesa' => 'Via propono estas malakceptita, ĉar la filmo estas jam afiŝita.',
            'kodo_malsukcesa'    => 'propono_afisxita'
        ];
    }



    // 2. Konstrui la peto-adreson laŭ la fonta platformo
    switch ($pv_fonto) {
        case 'youtube':
            $peto_adreso = a_spektejo_konstrui_api_adreson_por_videodatumoj_ytb(['identigilo' => $pv_identigilo_platforma]);
            break;
        default:
            return [
                'mesagxo_malsukcesa' => 'Nekonata platformo!',
                'kodo_malsukcesa'    => 'nekonata_fonto'
            ];
    }
    if (! $peto_adreso) {
        return [
            'mesagxo_malsukcesa' => 'Malĝusta adreso! Nuntempe ni akceptas filmojn nur de Youtube. Ni planas subteni pli da platformoj baldaŭ.',
            'kodo_malsukcesa'    => 'malgxusta_adreso'
        ];
    }



    // 3. Elŝuti datumojn el la peto-adreso
    $peto_datumoj = a_spektejo_elsxuti_json($peto_adreso);
    if (! $peto_datumoj) {
        return [
            'mesagxo_malsukcesa' => 'Eraro: Ne eblas kolekti la datumojn!',
            'kodo_malsukcesa'    => 'datumokolekto_malsukcesis'
        ];
    }


    // 4. Kolekti kaj kontroli datumojn el la alŝutaĵo
    switch ($pv_fonto) {
        case 'youtube':
            $videodatumoj = a_spektejo_konstrui_videodatumojn_el_ricevajxo_ytb($peto_datumoj);
            break;
        default:
            return [
                'mesagxo_malsukcesa' => 'Nekonata platformo!',
                'kodo_malsukcesa'    => 'nekonata_fonto'
            ];
    }
    if (! $videodatumoj) {
        return [
            'mesagxo_malsukcesa' => 'Eraro: La filmo ne troviĝas ĉe la platformo!',
            'kodo_malsukcesa'    => 'propono_malvalida'
        ];
    }


    // Kontroli ka kanalon
    $pv_kanalo             = $videodatumoj['kanalo'];
    $pv_kanalo_nometo      = $pv_fonto_kodo . '_' . $pv_kanalo;
    $pv_kanalo_termino     = term_exists($pv_kanalo_nometo, 'v_kanalo');

    if ($pv_kanalo_termino !== 0 && $pv_kanalo_termino !== null && $pv_kanalo_termino !== false && !empty($pv_kanalo_termino)) {
        $pv_kanalo_termino_id  = $pv_kanalo_termino['term_id'];
        $pv_kanalo_inkluziveco = get_term_meta($pv_kanalo_termino_id, 'v_kanalo_inkluziveco', true);
    } else {
        $pv_kanalo_inkluziveco = '';
    }

    /* if ($pv_kanalo_inkluziveco == 'ekskludata') {
        return [
            'mesagxo_malsukcesa' => 'Ne eblas aldoni la filmon: la kanalo "' . $videodatumoj['auxtoro'] . '" estas en la ekskluda listo.',
            'kodo_malsukcesa'    => 'propono_ekskludata'
        ];
    } */

    // Plenigi la afiŝodatumojn
    if (! empty($videodatumoj['priskribo']) || str_word_count($videodatumoj['titolo']) > 5) {
        $lingvo_specimeno    = $videodatumoj['titolo'] . ' ' . $videodatumoj['priskribo'];
        $lingvo_specimeno    = a_spektejo_prepari_tekstospecimenon(a_spektejo_seniksigi_vorton($lingvo_specimeno));
        $pv_lingvo_detektita = a_spektejo_e_detekti_lingvon($lingvo_specimeno);
    } else {
        $pv_lingvo_detektita = 'xx';
    }

    if ($pv_rilateco == 'esperanta') {
        $pv_lingvo = 'eo';
    } else {
        $pv_lingvo = $pv_lingvo_detektita;
    }

    $videodatumoj['fonto']         = $pv_fonto;
    $videodatumoj['fonto-kodo']    = $pv_fonto_kodo;
    $videodatumoj['nometo']        = $pv_fonto_kodo . '_' . $pv_identigilo_platforma;
    $videodatumoj['kanalo-nometo'] = $pv_fonto_kodo . '_' . $pv_kanalo;
    $videodatumoj['rilateco']      = $pv_rilateco;
    $videodatumoj['lingvo']        = $pv_lingvo;



    // 5. Aldoni la videoafiŝon
    if (a_spektejo_cxu_uzanto_rajtas('afisxo-aldoni')
        // || $pv_lingvo_detektita == 'eo'
        || $pv_kanalo_inkluziveco == 'inkludata'
    ) {
        $publikeco = true;
    } else {
        $publikeco = false;
    }
    $kreorezulto = a_spektejo_krei_videoafisxon($videodatumoj, $publikeco);

    protokolu($kreorezulto, 'jen la kreorezulto:', $protokoloID);



    // 6. Sendi retpoŝtmesaĝon al administranto, se ĉio estas enorda
    if ($publikeco === false && isset($kreorezulto['identigilo'])) {
        $retejonomo = get_bloginfo('name');
        $proponoadreso = get_permalink($kreorezulto['identigilo']);

        a_spektejo_sendi_retmesagxon_al_administranto(
            'Nova proponita filmo ĉe ' . $retejonomo,
            'Saluton! Iu proponis filmon ĉe ' . $retejonomo . ' . Bv. ensaluti kaj kontroli ĉe ' . $proponoadreso . "\r\n\r\n"
        );
    }

    protokolu(null, 'ĉio sonas en ordo! :D', $protokoloID);



    // 7. raporti pri la rezulto
    return $kreorezulto;
}





/**
 * Aldoni kanalon kiel proponon
 * a_spektejo_proponi_kanalon
 *
 * @param array $parametroj
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_proponi_kanalon($parametroj)
{

    // 1. prepari la datumojn
    if (! isset($parametroj)
        || ! is_array($parametroj)
        || empty($parametroj)
        || ! isset($parametroj['fonto'])
        || empty($parametroj['fonto'])
        || ! isset($parametroj['adreso'])
        || empty($parametroj['adreso'])
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    $pk_fonto = $parametroj['fonto'];
    $adreso   = esc_url($parametroj['adreso']);

    /*
    $kiu      = a_spektejo_kiu_estas_tiu_cxi(true);
    $uzanto   = $kiu['uzanto'];
    $anon     = $kiu['anon'];
    */

    // 2. konstrui la pet-adreson laŭ la fonta platformo
    switch ($pk_fonto) {
        case 'youtube':
            $peto_adreso = a_spektejo_konstrui_api_adreson_por_kanalodatumoj_ytb(['adreso' => $adreso]);
            break;
        default:
            return [
                'mesagxo_malsukcesa' => 'Nekonata platformo!',
                'kodo_malsukcesa'    => 'nekonata_fonto'
            ];
    }
    if (! $peto_adreso) {
        return [
            'mesagxo_malsukcesa' => 'Malĝusta adreso! Nuntempe ni akceptas kanalojn nur de Youtube. Ni planas subteni pli da platformoj baldaŭ.',
            'kodo_malsukcesa'    => 'malgxusta_adreso'
        ];
    }

    // 3. elŝuti datumojn el la peto-adreso
    $peto_datumoj = a_spektejo_elsxuti_json($peto_adreso);
    if (! $peto_datumoj) {
        return [
            'mesagxo_malsukcesa' => 'Eraro: Ne eblas kolekti la datumojn!',
            'kodo_malsukcesa'    => 'malsukesis_kolekti_datumojn'
        ];
    }

    // 4. kolekti kaj kontroli datumojn el la alŝutaĵo
    switch ($pk_fonto) {
        case 'youtube':
            $pk_fonto_kodo = 'ytb';
            $kanalodatumoj = a_spektejo_konstrui_kanalodatumojn_el_ricevajxo_ytb($peto_datumoj);
            break;
        default:
            return [
                'mesagxo_malsukcesa' => 'Nekonata platformo!',
                'kodo_malsukcesa'    => 'nekonata_fonto'
            ];
    }
    if (! $kanalodatumoj) {
        return [
            'mesagxo_malsukcesa' => 'Eraro: La kanalo ne troviĝas ĉe la platformo!',
            'kodo_malsukcesa'    => 'ne_trovis_kanalon_cxe_platformo'
        ];
    }
    $pk_identigilo = $kanalodatumoj['identigilo'];
    $pk_titolo     = $kanalodatumoj['titolo'];
    $pk_nometo     = $pk_fonto_kodo . '_' . $pk_identigilo;

    // 5. aldoni la kanalon
    // 5.1. kontroli la duoblecon
    $samnomaj_kanaloj = term_exists($pk_nometo, 'v_kanalo');
    if ($samnomaj_kanaloj) {
        $samnoma_kanalo  = $samnomaj_kanaloj[0];
        $kanalo_ID       = $samnoma_kanalo->term_id;

        $kanalo_inkluziveco = get_term_meta($kanalo_ID, 'v_kanalo_inkluziveco', true);

        if ($kanalo_inkluziveco == 'neaprobita') {
            return [
                'mesagxo_malsukcesa' => 'La kanalo estas jam proponita kaj atendas aprobon de administranto.',
                'kodo_malsukcesa'    => 'propono_atendas_aprobon'
            ];
        }

        if ($kanalo_inkluziveco == 'ekskluda') {
            return [
                'mesagxo_malsukcesa' => 'Via propono estas malakceptita, ĉar la kanalo "' . $pk_titolo . '" estas ignorata.',
                'kodo_malsukcesa'    => 'propono_ekskludata'
            ];
        }

        return [
            'mesagxo_malsukcesa' => 'La kanalo "' . $pk_titolo . '" jam ekzistas en ' . get_bloginfo('name') . '.',
            'kodo_malsukcesa'    => 'propono_duobla'
        ];
    }

    // 5.2 kontroli jaman forigitecon
    // TODO: Kontroli ĉe tombejo

    // 5.3. prepariĝi
    // por ne ŝanĝi majusklojn en nometoj
    remove_filter('sanitize_title', 'sanitize_title_with_dashes');
    remove_filter('sanitize_title', 'a_spektejo_ebligi_majusklon_punktojn_streketojn_en_titoloj');
    $sekvata = a_spektejo_cxu_uzanto_rajtas('kanalo-marki-inkluziveco') ? true : false;

    // 5.3. krei la kanaloterminon
    $pk_term_n      = wp_insert_term($pk_titolo, 'v_kanalo', ['slug' => $pk_nometo]);
    $pk_term_n_id   = intval($pk_term_n['term_id']);
    $pk_inkluziveco = $sekvata ? 'inkludata' : 'neaprobita';

    // 5.4. aldoni kromdatumojn
    add_term_meta($pk_term_n_id, 'v_kanalo_inkluziveco', $pk_inkluziveco, true);
    add_term_meta($pk_term_n_id, 'v_kanalo_fonto', $pk_fonto, true);
    add_term_meta($pk_term_n_id, 'v_kanalo_identigilo_fonto', $pk_identigilo, true);

    // 6. sendi retpoŝtmesaĝon al administranto
    $retejonomo = get_bloginfo('name');
    $proponoadreso = get_term_link($pk_term_n_id);

    if (!$sekvata) {
        a_spektejo_sendi_retmesagxon_al_administranto(
            'Nova proponita kanalo ĉe ' . $retejonomo,
            'Saluton! Iu proponis kanalon ĉe ' . $retejonomo . '. Bv. ensaluti kaj kontroli ĉe ' . $proponoadreso . "\r\n\r\n"
        );
    }

    // 7. raporti pri la rezulto
    if ($sekvata) {
        return [
            'mesagxo_sukcesa' => 'Enorde. La kanalo estos sekvata ekde nun.',
            'kodo_sukcesa'    => 'sukcese_aldonis_kanalon',
            'operacio'        => 'sekvi_kanalon'
        ];
    } else {
        return [
            'mesagxo_sukcesa' => 'Enorde. Via proponita kanalo estos sekvata post aprobo de administranto.',
            'kodo_sukcesa'    => 'sukcese_proponis_kanalon',
            'operacio'        => 'proponi_kanalon'
        ];
    }
}







/**
 * Forigi la enhaveron de la ĉefa datumbazo, kaj aldoni referencon ĉe la tombejo
 * a_spektejo_forigi_enhaveron
 *
 * @param string  $speco
 * @param int     $identigilo
 *
 * @return array  raporto pri la rezulto
 */
function a_spektejo_forigi_enhaveron($speco, $identigilo, $kialo = '')
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton!', $protokoloID);
    protokolu([$speco, $identigilo, $kialo], 'Jen la speco, identigilo kaj la kialo:', $protokoloID);

    if (! isset($identigilo)
        || $identigilo <= 0
    ) {
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }
    if (empty($kialo)) {
        $kialo = 'nespecifa';
    }

    $identigilo = intval($identigilo);

    switch ($speco) {
        case 'video':
            $fonto    = wp_get_post_terms($identigilo, 'v_fonto')[0]->slug;
            $id_fonto = get_post_meta($identigilo, 'v_identigilo_fonto', true);
            $auxtoro  = ''; // FIXME
            break;
        case 'kanalo':
            $grandeco = get_term($identigilo, 'v_kanalo')->count;
            if ($grandeco > 0) {
                return [
                    'mesagxo_malsukcesa' => 'La kanalo ne estas malplena!',
                    'kodo_malsukcesa'    => 'kanalo_nemalplena'
                ];
            }
            $fonto    = get_term_meta($identigilo, 'v_kanalo_fonto', true);
            $id_fonto = get_term_meta($identigilo, 'v_kanalo_identigilo_fonto', true);
            $auxtoro  = ''; // FIXME
            break;
        case 'komento':
            $fonto    = PROJEKTNOMO;
            $id_fonto = '0';
            $auxtoro  = a_spektejo_retrovi_auxtoron_de_komento($identigilo);
            break;
        default:
            return [
                'mesagxo_malsukcesa' => 'Nedifina ero-speco.',
                'kodo_malsukcesa'    => 'speco_nekonata'
            ];
    }

    // TODO: GRAVE forigi ĉiujn signalojn por la ero

    // Aldoni referencon ĉe la Tombejo
    $rezulto_sendi_tombejen = a_spektejo_aldoni_eron_cxe_tombejo($speco, $identigilo, $fonto, $id_fonto, $kialo, $auxtoro);

    protokolu($rezulto_sendi_tombejen, 'jen la rezulto de la operacio:', $protokoloID);

    if (! $rezulto_sendi_tombejen) {
        return [
            'mesagxo_malsukcesa' => 'Ne eblas forigi eron.',
            'kodo_malsukcesa'    => 'malsukcesis_sendi_tombejen'
        ];
    }

    // Forigi la eron
    switch ($speco) {
        case 'video':
            $forigorezulto = wp_delete_post($identigilo);
            break;
        case 'kanalo':
            $forigorezulto = wp_delete_term($identigilo, 'v_kanalo');
            break;
        case 'komento':
            $forigorezulto = wp_delete_comment($identigilo);
            break;
        default:
            return [
                'mesagxo_malsukcesa' => 'Ne eblas forigi eron.',
                'kodo_malsukcesa'    => 'malsukcesis_forigi'
            ];
    }

    protokolu($forigorezulto, 'jen la forigorezulto:', $protokoloID);

    if (! $forigorezulto || is_wp_error($forigorezulto)) {
        return [
            'mesagxo_malsukcesa' => 'Ne eblas forigi eron.',
            'kodo_malsukcesa'    => 'malsukcesis_forigi'
        ];
    }


    return [
        'mesagxo_sukcesa' => 'Enorde. La ero estis forigita.',
        'kodo_sukcesa'    => 'forigis_eron',
        'operacio'        => 'forigi_eron'
    ];
}








/**
 * Aldoni referencilon por la forigota ero al la "tombejo"
 * a_spektejo_aldoni_eron_cxe_tombejo
 *
 * @param string  $speco
 * @param int     $identigilo
 * @param string  $fonto
 * @param string  $id_fonto
 * @param string  $kialo
 * @param string  $auxtoro
 *
 * @return array  terminoj aranĝitaj kiel 'nometo' => 'nomo'
 */
function a_spektejo_aldoni_eron_cxe_tombejo($speco, $identigilo, $fonto, $id_fonto, $kialo = 'nespecifa', $auxtoro = '')
{

    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton!', $protokoloID);
    protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);


    if (empty($speco)
        || empty($identigilo)
        || empty($fonto)
        || empty($id_fonto)
    ) {
        return false;
    }

    global $wpdb;
    $tabelo_nomo = $wpdb->prefix . PROJEKTNOMO . '_tombejo';

    $aldoni_tombejen = $wpdb->insert(
        $tabelo_nomo,
        [
            'speco'     => $speco,
            'fonto'     => $fonto,
            'id_fonto'  => $id_fonto,
            'wpid'      => $identigilo,
            'auxtoro'   => $auxtoro,
            'kialo'     => $kialo,
            'tempo'     => current_time('mysql'),
        ]
    );

    protokolu($aldoni_tombejen, 'jen la rezulto pri aldoni tombejen:', $protokoloID);


    if ($aldoni_tombejen === false) {
        return false;
    }

    return true;
}




/* == SPEKTADO, AKTIVECO == */





/**
 * Kalkuli la minimuman tempodaŭron necesa por enkalkuli spekton por filmo
 * a_spektejo_kalkuli_minimuman_spektodauxron_por_video
 *
 * @param int $identigilo
 *
 * @return int la daŭro, en milisekundoj
 */
function a_spektejo_kalkuli_minimuman_spektodauxron_por_video($identigilo)
{
    // $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO

    $dauxro     = new DateInterval(get_post_meta($identigilo, 'v_dauxro', true));
    $dauxro_ms  = ($dauxro->h * 3600000) + ($dauxro->i * 60000) + ($dauxro->s * 1000); // milisekundoj
    $dauxro_min = round(60000 * ($dauxro_ms / 180000) / (($dauxro_ms / 180000) + 1)) + 3000; // milisekundoj

    // protokolu(null, 'la minimuma spektodaŭro estas ' . $dauxro_min / 1000 . ' skundoj.', $protokoloID);
    return $dauxro_min;
}







/**
 * Aldoni spekton por videoafiŝo
 * a_spektejo_aldoni_spekton_por_video
 *
 * @param int $identigilo
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_aldoni_spekton_por_video($identigilo)
{
    // $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'aldonas spekton por la afiŝo #' . $identigilo . '...', $protokoloID);

    // aldoni la spekton
    $aldoni_spekton_rezulto = a_spektejo_aldoni_poenton_por_video($identigilo, 'v_spektajxoj');

    // Protokoli la eventon
    a_spektejo_protokoli_eventon('spekto', $identigilo);

    // aldoni la spektinton
    a_spektejo_aldoni_aganton_por_video($identigilo, 'v_spektintoj');

    if ($aldoni_spekton_rezulto === false) {
        return [
            'mesagxo_malsukcesa' => 'Ne sukcesis aldoni spekton por la filmo!',
            'kodo_malsukcesa'    => 'malsukcesis_spekti',
            'operacio'           => 'spekti',
            'identigilo'         => $identigilo
        ];
    } else {
        return [
            'mesagxo_sukcesa' => 'Sukcese aldonis spekton por la filmo!',
            'kodo_sukcesa'    => 'sukcese_spektis',
            'operacio'        => 'spekti',
            'identigilo'      => $identigilo
        ];
    }
}





/**
 * Aldoni referencon por videoafiŝo
 * a_spektejo_aldoni_referencopoenton_por_video
 *
 * @param int $identigilo
 *
 * @return mixed // FIXME
 */
// FIXME: Ĉu uzi a_spektejo_aldoni_poenton_por_video anstataŭe?
function a_spektejo_aldoni_referencopoenton_por_video($identigilo)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton, mondo!', $protokoloID);

    if (empty($identigilo)) {
        return;
    }

    protokolu($identigilo, 'jen la identitilo:', $protokoloID);

    $referenconombro = intval(get_post_meta($identigilo, 'v_referencoj', true));
    $referenconombro++;
    update_post_meta($identigilo, 'v_referencoj', $referenconombro);
}





/**
 * Aldoni arbitran poenton por videoafiŝo
 * a_spektejo_aldoni_poenton_por_video
 *
 * @param int    $identigilo
 * @param string $kampo      kampo de poentoj, en kiu aldoni la poenton
 * @param int    $poentoj    kiom da poentoj aldoni
 *
 * @return mixed  // FIXME
 */
function a_spektejo_aldoni_poenton_por_video($identigilo, $kampo, $poentoj = 1)
{
    // $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton, mondo!', $protokoloID);
    // protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);

    if (empty($identigilo) || empty($kampo)) {
        return;
    }

    $poentoj_jamaj = intval(get_post_meta($identigilo, $kampo, true));
    $poentoj = $poentoj_jamaj + $poentoj;
    $aldoni_poenton_rezulto = update_post_meta($identigilo, $kampo, $poentoj);

    // protokolu($aldoni_poenton_rezulto, 'jen la rezulto pri aldoni poentojn:', $protokoloID);

    return $aldoni_poenton_rezulto;
}





/**
 * Aldoni arbitran aganton por videoafiŝo
 * a_spektejo_aldoni_aganton_por_video
 *
 * @param int    $identigilo
 * @param string $kampo      kampo de aganto, en kiu aldoni la poenton
 *
 * @return mixed // FIXME
 */
function a_spektejo_aldoni_aganton_por_video($identigilo, $kampo)
{
    // $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton, mondo!', $protokoloID);
    // protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);

    if (empty($identigilo) || empty($kampo)) {
        return;
    }

    $aganto  = a_spektejo_kiu_estas_tiu_cxi();
    $agintoj = get_post_meta($identigilo, $kampo, true);
    if (empty($agintoj)) {
        $agintoj = [];
    }
    $agintoj[] = $aganto;

    $aldoni_aganton_rezulto = update_post_meta($identigilo, $kampo, array_unique($agintoj));

    // protokolu($aldoni_aganton_rezulto, 'jen la rezulto por aldoni la aganton:', $protokoloID);

    return $aldoni_aganton_rezulto;
}










/* == VOĈDONOJ == */




/**
 * Aldoni ŝaton aŭ malŝaton por videoafiŝo
 * a_spektejo_aldoni_vocxon_por_video
 *
 * @param int    $identigilo
 * @param string $vocxo
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_aldoni_vocxon_por_video($identigilo, $vocxo)
{

    // 1. prepari la datumojn
    if (! isset($identigilo)
        || $identigilo <= 0
        || ! isset($vocxo)
        || empty($vocxo)
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    $kiu    = a_spektejo_kiu_estas_tiu_cxi(true);
    $uzanto = $kiu['uzanto'];
    $anon   = $kiu['anon'];

    $vocxo_jama = a_spektejo_kiel_uzanto_vocxdonis_por_video($identigilo, $uzanto, $anon);

    $metakampo_sxatantoj    = ($anon) ? 'v_sxatantoj_anon'    : 'v_sxatantoj';
    $metakampo_malsxatantoj = ($anon) ? 'v_malsxatantoj_anon' : 'v_malsxatantoj';

    /*
    $vocxoj_cxio    = a_spektejo_retrovi_vocxojn_por_video($identigilo, 'cxio');
    $sxatoj_sumo    = intval($vocxoj_cxio['sxatoj_sumo']);
    $sxatoj_anon    = intval($vocxoj_cxio['sxatoj_anon']);
    $malsxatoj_sumo = intval($vocxoj_cxio['malsxatoj_sumo']);
    $malsxatoj_anon = intval($vocxoj_cxio['malsxatoj_anon']);
    $vocxoj         = intval($vocxoj_cxio['sumo']);

    $cxio_sumo      = $sxatoj_sumo + $malsxatoj_sumo;
    $vocxoj_anon    = $sxatoj_anon + $malsxatoj_anon;

    if (($anon) && empty($vocxo_jama) && ($cxio_sumo > 2) && ($cxio_sumo / 3 < $vocxoj_anon + 1) ) {
        return [
            'mesagxo_malsukcesa' => 'Anonimaj voĉdonoj momente ne eblas por ĉi tiu video, pro malsufiĉo de la voĉoj de aliĝintoj. Aliĝu por enkalkuli vian voĉon :)',
            'kodo_malsukcesa'    => 'tro_da_anonimaj_vocxoj'
        ];
    }
    */


    // 2. aldoni/forigi ŝaton/malŝaton laŭ la voĉdonopeto kaj la antaŭaj voĉdonoj
    switch ($vocxo) {
        case 'sxati':
            switch ($vocxo_jama) {
                case 'ne-vocxdonis':
                    $aldoni_sxaton_rezulto = add_post_meta($identigilo, $metakampo_sxatantoj, $uzanto);
                    if ($aldoni_sxaton_rezulto === false) {
                        return [
                            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste',
                            'kodo_malsukcesa'    => 'ne_sukcesis_sxati',
                        ];
                    }
                    $vocxdono_farite = 'sxati';
                    break;

                case 'sxatis':
                    // uzanto jam ŝatis la videon. forigu la ŝaton
                    $forigi_sxaton_rezulto = delete_post_meta($identigilo, $metakampo_sxatantoj, $uzanto);
                    if ($forigi_sxaton_rezulto === false) {
                        return [
                            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste',
                            'kodo_malsukcesa'    => 'ne_sukcesis_nesxati'
                        ];
                    }
                    $vocxdono_farite = 'nesxati';
                    break;

                case 'malsxatis':
                    // uzanto antaŭe malŝatis. forigu la malŝaton kaj aldonu ŝaton
                    $forigi_malsxaton_rezulto = delete_post_meta($identigilo, $metakampo_malsxatantoj, $uzanto);
                    $aldoni_sxaton_rezulto    = add_post_meta($identigilo, $metakampo_sxatantoj, $uzanto);
                    if ($forigi_malsxaton_rezulto === false || $aldoni_sxaton_rezulto === false) {
                        return [
                            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste',
                            'kodo_malsukcesa'    => 'ne_sukcesis_nemalsxati_sxati'
                        ];
                    }
                    $vocxdono_farite = 'sxati';
                    break;

                default:
                    return [
                        'mesagxo_malsukcesa' => 'Nedifina ago.',
                        'kodo_malsukcesa'    => 'nekonata_ago'
                    ];
            }
            break;

        case 'malsxati':
            switch ($vocxo_jama) {
                case 'ne-vocxdonis':
                    $aldoni_malsxaton_rezulto = add_post_meta($identigilo, $metakampo_malsxatantoj, $uzanto);
                    if ($aldoni_malsxaton_rezulto === false) {
                        return [
                            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste',
                            'kodo_malsukcesa'    => 'ne_sukcesis_malsxati'
                        ];
                    }
                    $vocxdono_farite = 'malsxati';
                    break;

                case 'malsxatis':
                    // uzanto jam malŝatis la videon. forigu la malŝaton
                    $forigi_malsxaton_rezulto = delete_post_meta($identigilo, $metakampo_malsxatantoj, $uzanto);
                    if ($forigi_malsxaton_rezulto === false) {
                        return [
                            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste',
                            'kodo_malsukcesa'    => 'ne_sukcesis_nemalsxati'
                        ];
                    }
                    $vocxdono_farite = 'nemalsxati';
                    break;

                case 'sxatis':
                    // uzanto antaŭe ŝatis. forigu la ŝaton kaj aldonu malŝaton
                    $forigi_sxaton_rezulto    = delete_post_meta($identigilo, $metakampo_sxatantoj, $uzanto);
                    $aldoni_malsxaton_rezulto = add_post_meta($identigilo, $metakampo_malsxatantoj, $uzanto);
                    if ($forigi_sxaton_rezulto === false || $aldoni_malsxaton_rezulto === false) {
                        return [
                            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste',
                            'kodo_malsukcesa'    => 'ne_sukcesis_nesxati_malsxati'
                        ];
                    }
                    $vocxdono_farite = 'malsxati';
                    break;

                default:
                    return [
                        'mesagxo_malsukcesa' => 'Nedifina ago.',
                        'kodo_malsukcesa'    => 'nekonata_ago'
                    ];
            }
            break;

        default:
            return [
                'mesagxo_malsukcesa' => 'Nedifina ago.',
                'kodo_malsukcesa'    => 'nekonata_ago'
            ];
    }

    $vocxoj_nun                 = a_spektejo_retrovi_vocxojn_por_video($identigilo);
    $gxisnunigi_vocxojn_rezulto = update_post_meta($identigilo, 'v_vocxoj', $vocxoj_nun);

    // 3. Raporti pri la rezulto
    if ($gxisnunigi_vocxojn_rezulto === false) {
        return [
            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste',
            'kodo_malsukcesa'    => 'ne_sukcesis_gxisnunigi_vocxojn'
        ];
    }

    switch ($vocxdono_farite) {
        case 'sxati':
            a_spektejo_protokoli_eventon('sxato', $identigilo); // Protokoli la eventon
            $mesagxo_sukcesa = 'Via ŝato estis aldonita.';
            break;
        case 'nesxati':
            $mesagxo_sukcesa = 'Via ŝato estis forigita.';
            break;
        case 'malsxati':
            a_spektejo_protokoli_eventon('malsxato', $identigilo); // Protokoli la eventon
            $mesagxo_sukcesa = 'Via malŝato estis aldonita.';
            break;
        case 'nemalsxati':
            $mesagxo_sukcesa = 'Via malŝato estis forigita.';
            break;

        default:
            $mesagxo_sukcesa = 'Via voĉo estis aldonita.';
            break;
    }

    return [
        'mesagxo_sukcesa' => $mesagxo_sukcesa,
        'kodo_sukcesa'    => 'sukcese_vocxdonis',
        'operacio'        => 'vocxdoni',
        'identigilo'      => $identigilo,
        'detaloj'         => [
            'vocxo_donita' => $vocxdono_farite,
            'vocxoj_nun'   => $vocxoj_nun
        ]
    ];
}




/**
 * Determini, ĉu/kiel la vizitanto/uzanto antaŭe voĉdonis por videoafiŝo
 * a_spektejo_kiel_uzanto_vocxdonis_por_video
 *
 * @param int   $identigilo
 * @param array $uzanto // FIXME
 * @param bool  $anon
 *
 * @return string
 */
function a_spektejo_kiel_uzanto_vocxdonis_por_video($identigilo, $uzanto = '', $anon = false)
{

    if (empty($uzanto)) {
        $kiu    = a_spektejo_kiu_estas_tiu_cxi(true);
        $uzanto = $kiu['uzanto'];
        $anon   = $kiu['anon'];
    } // FIXME

    $metakampo_sxatantoj    = ($anon) ? 'v_sxatantoj_anon'    : 'v_sxatantoj';
    $metakampo_malsxatantoj = ($anon) ? 'v_malsxatantoj_anon' : 'v_malsxatantoj';

    $sxatantoj_jam    = get_post_meta($identigilo, $metakampo_sxatantoj, false);
    $malsxatantoj_jam = get_post_meta($identigilo, $metakampo_malsxatantoj, false);

    if (in_array($uzanto, $sxatantoj_jam)) {
        $uzanto_antauxe = 'sxatis';
    } elseif (in_array($uzanto, $malsxatantoj_jam)) {
        $uzanto_antauxe = 'malsxatis';
    } else {
        $uzanto_antauxe = 'ne-vocxdonis';
    }

    return $uzanto_antauxe;
}





/**
 * Retrovi la jamajn voĉojn donitajn (ŝatoj kaj malŝatoj) al videoafiŝo
 * a_spektejo_retrovi_vocxojn_por_video
 *
 * @param int   $identigilo
 * @param array $vocxospecoj
 *
 * @return array
 */
function a_spektejo_retrovi_vocxojn_por_video($identigilo, $vocxospecoj = [])
{

    $sxatantoj_reg     = get_post_meta($identigilo, 'v_sxatantoj', false);
    $sxatantoj_anon    = get_post_meta($identigilo, 'v_sxatantoj_anon', false);
    $malsxatantoj_reg  = get_post_meta($identigilo, 'v_malsxatantoj', false);
    $malsxatantoj_anon = get_post_meta($identigilo, 'v_malsxatantoj_anon', false);

    $sxatoj_reg        = (is_array($sxatantoj_reg))     ? count($sxatantoj_reg)     : 0;
    $sxatoj_anon       = (is_array($sxatantoj_anon))    ? count($sxatantoj_anon)    : 0;
    $malsxatoj_reg     = (is_array($malsxatantoj_reg))  ? count($malsxatantoj_reg)  : 0;
    $malsxatoj_anon    = (is_array($malsxatantoj_anon)) ? count($malsxatantoj_anon) : 0;

    $sxatoj_sumo       = $sxatoj_reg    + $sxatoj_anon;
    $malsxatoj_sumo    = $malsxatoj_reg + $malsxatoj_anon;
    $sumo              = $sxatoj_sumo   - $malsxatoj_sumo;

    if (empty($vocxospecoj)) {
        $vocxoj = $sumo;
    } else {
        $vocxoj = [];
        switch ($vocxospecoj) {
            case 'sxatoj-anon':
                $vocxoj['sxatoj-anon']    = $sxatoj_anon;
                $vocxoj['sxatoj-sumo']    = $sxatoj_sumo;
                break;
            case 'malsxatoj-anon':
                $vocxoj['malsxatoj-anon'] = $malsxatoj_anon;
                $vocxoj['malsxatoj-sumo'] = $malsxatoj_sumo;
                break;
            case 'sxatoj-cxio':
                $vocxoj['sxatoj-reg']     = $sxatoj_reg;
                $vocxoj['sxatoj-anon']    = $sxatoj_anon;
                $vocxoj['sxatoj-sumo']    = $sxatoj_sumo;
                break;
            case 'malsxatoj-cxio':
                $vocxoj['malsxatoj-reg']  = $malsxatoj_reg;
                $vocxoj['malsxatoj-anon'] = $malsxatoj_anon;
                $vocxoj['malsxatoj-sumo'] = $malsxatoj_sumo;
                break;
            case 'cxio':
                $vocxoj['sxatoj-reg']     = $sxatoj_reg;
                $vocxoj['sxatoj-anon']    = $sxatoj_anon;
                $vocxoj['malsxatoj-reg']  = $malsxatoj_reg;
                $vocxoj['malsxatoj-anon'] = $malsxatoj_anon;
                $vocxoj['sxatoj-sumo']    = $sxatoj_sumo;
                $vocxoj['malsxatoj-sumo'] = $malsxatoj_sumo;
                break;
        }
        $vocxoj['sumo'] = $sumo;
    }

    return $vocxoj;
}





/* == ETIKEDOJ, KATEGORIOJ == */







/* == KROMDATUMOJ == */





/**
 * Retrovi liston de terminoj por specifa taksonomio, ekzemple rilatecoj, lingvoj ktp.
 * a_spektejo_retrovi_terminoliston
 *
 * @param string $taksonomio
 *
 * @return array  terminoj aranĝitaj kiel 'nometo' => 'nomo'
 */
function a_spektejo_retrovi_terminoliston($taksonomio, $ordoparametro = 'term_id', $ekskludi_malplenajn = false)
{
    switch ($ordoparametro) {
        case 'term_id':
            $ordo = 'ASC';
            break;
        case 'count':
            $ordo = 'DESC';
            break;
        default:
            $ordo = 'ASC';
    }

    $terminoj = get_terms(
        [
            'taxonomy'   => $taksonomio,
            'number'     => 0,
            'orderby'    => $ordoparametro,
            'order'      => $ordo,
            'hide_empty' => $ekskludi_malplenajn,
        ]
    );

    /* return array_column($terminoj, 'name', 'slug'); */

    $terminoj_resume = array_map(
        function ($termino) {
            return [
                'nomo'     => $termino->name,
                'grandeco' => $termino->count
            ];
        },
        $terminoj
    );

    return array_combine(array_column($terminoj, 'slug'), $terminoj_resume);
}






/**
 * Retrovi liston de terminoj por specifa taksonomio, ekzemple rilatecoj, lingvoj ktp.
 * a_spektejo_retrovi_terminoliston_por_afisxo
 *
 * @param int    $identigilo
 * @param string $taksonomio
 *
 * @return array  terminoj aranĝitaj kiel 'nometo' => 'nomo'
 */
function a_spektejo_retrovi_terminoliston_por_afisxo($identigilo, $taksonomio)
{
    $terminoj = get_the_terms($identigilo, $taksonomio);

    if ($terminoj === false || is_wp_error($terminoj)) {
        return [];
    }

    return array_column($terminoj, 'name', 'slug');
}







/* == RILATAJ AFIŜOJ == */




/**
 * TODO:
 * a_spektejo_trovi_eble_rilatajn_videojn
 *
 * @param int    $identigilo
 *
 * @return array TODO:
 */
function a_spektejo_trovi_eble_rilatajn_videojn($identigilo)
{
    // $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton!', $protokoloID);
    // protokolu(null, 'pritrkatas la afiŝon #' . $identigilo . '...', $protokoloID);

    $identigilo = intval($identigilo);
    $rilateco   = wp_get_post_terms($identigilo, 'v_rilateco', ['fields' => 'slugs'])[0];

    if ($rilateco != 'esperanta'
        && $rilateco != 'priesperanta'
        && $rilateco != 'nedifina'
    ) {
        // protokolu(null, 'la rilateco estas ' . $rilateco . '. ignoras la afiŝon...', $protokoloID);
        return [];
    }

    $etikedoj   = wp_get_post_terms($identigilo, 'v_etikedoj', ['fields' => 'ids']);
    $kategorioj = wp_get_post_terms($identigilo, 'v_kategorioj', ['fields' => 'ids']);

    // protokolu($etikedoj, 'jen la etikedoj:', $protokoloID);
    // protokolu($kategorioj, 'jen la kategorioj:', $protokoloID);

    if (empty($etikedoj) && empty($kategorioj)) {
        // protokolu(null, 'neniu etikedo aŭ kategorio. ignoras la afiŝon...', $protokoloID);
        return [];
    }

    $afisxoj_arg = [
        'post_type'     => PROJEKTNOMO . '_video',
        'post_status'   => 'publish',
        'numberposts'   => 1500, // pardonu nin, kara servilo <3
        'orderby'       => 'rand',
        'fields'        => 'ids',
        'no_found_rows' => true,
        'ignore_sticky_posts'    => true,
        'update_post_meta_cache' => false,
        'update_post_term_cache' => false,
        'tax_query'     => [
            'relation' => 'OR',
            [
                'taxonomy' => 'v_etikedoj',
                'field'    => 'term_id',
                'terms'    => $etikedoj,
                'operator' => 'IN'
            ],
            [
                'taxonomy' => 'v_kategorioj',
                'field'    => 'term_id',
                'terms'    => $kategorioj,
                'operator' => 'IN'
            ]
        ],
    ];
    $eble_rilataj_videoj = get_posts($afisxoj_arg);

    // protokolu($eble_rilataj_videoj, 'jen eble-rilataj afiŝoj:', $protokoloID);
    return $eble_rilataj_videoj;
}





/**
 * TODO:
 * a_spektejo_trovi_rilatajn_videojn
 *
 * @param int    $identigilo
 * @param int    $nombro
 *
 * @return array TODO:
 */
function a_spektejo_trovi_rilatajn_videojn($identigilo, $nombro = 12)
{

    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton!', $protokoloID);
    // protokolu(null, 'pritrkatas la afiŝon #' . $identigilo . '...', $protokoloID);

    $identigilo = intval($identigilo);
    // $rilateco   = wp_get_post_terms($identigilo, 'v_rilateco', ['fields' => 'slugs'])[0]; // ne uzata
    $afisxoj_eble_rilataj = get_post_meta($identigilo, 'v_afisxoj_rilataj_eble', true);

    if (empty($afisxoj_eble_rilataj)) {
        // protokolu(null, 'ne trovis liston de eble-rilataj afiŝoj. ignoras la afiŝon...', $protokoloID);

        return [];
    }

    // protokolu($afisxoj_eble_rilataj, 'jen eble-rilataj afiŝoj:', $protokoloID);
    // protokolu(null, 'pritraktas ĉiun eron...', $protokoloID);

    $rilataj_videoj = [];

    foreach ($afisxoj_eble_rilataj as $eble_rilata_identigilo) {
        // protokolu(null, 'pritraktas la afiŝon #' . $eble_rilata_identigilo . '...', $protokoloID);

        if ($eble_rilata_identigilo == $identigilo) {
            // protokolu(null, 'tio estas la sama afiŝo. ignoras ĝin...', $protokoloID);
            continue;
        }

        $afisxo_eble_rilata_rilateco = wp_get_post_terms($eble_rilata_identigilo, 'v_rilateco', ['fields' => 'slugs'])[0];

        if ($afisxo_eble_rilata_rilateco != 'esperanta' && $afisxo_eble_rilata_rilateco != 'priesperanta') {
            // protokolu(null, 'la rilateco estas nek esperanta, nek pri-esperanta. ignoras ĝin...', $protokoloID);
            continue;
        }

        $etikedoj   = wp_get_post_terms($identigilo, 'v_etikedoj', ['fields' => 'ids']);
        $kategorioj = wp_get_post_terms($identigilo, 'v_kategorioj', ['fields' => 'ids']);
        $afisxo_eble_rilata_etikedoj   = wp_get_post_terms($eble_rilata_identigilo, 'v_etikedoj', ['fields' => 'ids']);
        $afisxo_eble_rilata_kategorioj = wp_get_post_terms($eble_rilata_identigilo, 'v_kategorioj', ['fields' => 'ids']);

        $komunaj_etikedoj     = array_intersect($etikedoj, $afisxo_eble_rilata_etikedoj);
        $komunaj_kategorioj   = array_intersect($kategorioj, $afisxo_eble_rilata_kategorioj);
        $komunaj_etikedoj_n   = count($komunaj_etikedoj);
        $komunaj_kategorioj_n = count($komunaj_kategorioj);

        $komunaj_ecoj_n = $komunaj_etikedoj_n + (2 * $komunaj_kategorioj_n);

        // protokolu($afisxo_eble_rilata_etikedoj, 'jen la etikedoj de la afiŝo:', $protokoloID);
        // protokolu($komunaj_etikedoj, 'jen la komunaj etikedoj (' . $komunaj_etikedoj_n . ' eroj):', $protokoloID);
        // protokolu($afisxo_eble_rilata_kategorioj, 'jen la kategorioj de la afiŝo:', $protokoloID);
        // protokolu($komunaj_kategorioj, 'jen la komunaj kategorioj (' . $komunaj_kategorioj_n . ' eroj):', $protokoloID);
        // protokolu(null, 'entute estas ' . $komunaj_ecoj_n . ' komunaj ecoj inter la afiŝoj.', $protokoloID);

        $rilataj_videoj[strval($eble_rilata_identigilo)] = $komunaj_ecoj_n;
    }

    arsort($rilataj_videoj, SORT_NUMERIC);

    // protokolu($rilataj_videoj, 'jen la fina listo de rilataj videoj, ordigite laŭ nombro de komunaj etikedoj:', $protokoloID);

    $rilataj_videoj = array_slice(array_keys($rilataj_videoj), 0, $nombro, true);
    return $rilataj_videoj;
}




/* == KANALOJ == */



/**
 * Ĝisnunigi la nombron de publike-videblaj afiŝoj por kanalo
 * a_spektejo_gxisnunigi_kanalograndecon
 *
 * @param string  $kanalo_nometo
 */
function a_spektejo_gxisnunigi_kanalograndecon($kanalo_nometo)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton, mondo!', $protokoloID);
    // protokolu(null, 'la kanalonometo estas ' . $kanalo_nometo, $protokoloID);

    $publikaj_afisxoj_de_kanalo = get_posts(
        [
            'post_type'     => PROJEKTNOMO . '_video',
            'post_status'   => 'publish',
            'numberposts'   => -1,
            'fields'        => 'ids',
            'no_found_rows' => true,
            'ignore_sticky_posts'    => true,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'tax_query'     => [
                'relation' => 'AND',
                [
                    'taxonomy' => 'v_kanalo',
                    'field'    => 'slug',
                    'terms'    => $kanalo_nometo,
                ],
                [
                    'taxonomy' => 'v_rilateco',
                    'field'    => 'slug',
                    'terms'    => ['esperanta', 'priesperanta', 'nedifina'],
                ]
            ],
        ]
    );
    $kanalo_grandeco = count($publikaj_afisxoj_de_kanalo);

    // protokolu($publikaj_afisxoj_de_kanalo, 'jen la identigiloj por la publikaj afiŝoj de la kanalo:', $protokoloID);
    // protokolu($kanalo_grandeco, 'jen la grandeco:', $protokoloID);

    $kanalo_termino = get_term_by('slug', $kanalo_nometo, 'v_kanalo');
    if ($kanalo_termino && is_numeric($kanalo_grandeco)) {
        $kanalo_termino_id = $kanalo_termino->term_id;
        $rezulto = update_term_meta($kanalo_termino_id, 'v_kanalo_grandeco', $kanalo_grandeco);

        // protokolu($rezulto, 'jen la rezulto', $protokoloID);
    }
}



/**
 * Ĝisnunigi la daton de la lasta ŝanĝo por kanalo
 * a_spektejo_gxisnunigi_modifodaton_por_kanalo
 *
 * @param string  $kanalo_nometo
 */
function a_spektejo_gxisnunigi_modifodaton_por_kanalo($kanalo_nometo)
{
    $kanalo_termino = get_term_by('slug', $kanalo_nometo, 'v_kanalo');
    if ($kanalo_termino) {
        $kanalo_termino_id = $kanalo_termino->term_id;
        $rezulto = update_term_meta($kanalo_termino_id, 'v_kanalo_lasta_sxangxo', time()); // FIXME: Eble uzu la daton por la lasta afiŝo anstataŭe?

        // protokolu($rezulto, 'jen la rezulto', $protokoloID);
    }
}




/**
 * Ĝisnunigi la liston de la plej ofte uzataj kategorioj por la afiŝoj de la kanalo
 * a_spektejo_gxisnunigi_plejoftajn_kategoriojn_por_kanalo
 *
 * @param string  $kanalo_nometo
 */
function a_spektejo_gxisnunigi_plejoftajn_kategoriojn_por_kanalo($kanalo_nometo)
{
    $kanalo_termino = get_term_by('slug', $kanalo_nometo, 'v_kanalo');
    if ($kanalo_termino) {
        $kanalo_termino_id = $kanalo_termino->term_id;

        $plejoftaj_kategorioj = a_spektejo_determini_plejoftajn_kategoriojn_por_kanalo($kanalo_termino_id);

        if (!empty($plejoftaj_kategorioj)) {
            $rezulto = update_term_meta($kanalo_termino_id, 'v_kanalo_plejoftaj_kategorioj', $plejoftaj_kategorioj);
            a_spektejo_aldoni_kanalokategorion(array_keys($plejoftaj_kategorioj));

            // protokolu($rezulto, 'jen la rezulto', $protokoloID);
        }
    }
}




/**
 * // TODO:
 * a_spektejo_aldoni_kanalokategorion
 *
 * @param array  $kategorioj
 */
function a_spektejo_aldoni_kanalokategorion($kategorioj)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO

    $agordo = PROJEKTNOMO . '_k_kategorioj';
    $kategorioj_por_kanaloj = get_option($agordo, []);

    // protokolu($kategorioj_por_kanaloj, 'jen la kategorioj por kanaloj:', $protokoloID);
    // protokolu($kategorioj, 'jen la kategorioj:', $protokoloID);

    $kategorioj_por_kanaloj = array_merge($kategorioj_por_kanaloj, $kategorioj);
    $rezulto = update_option($agordo, array_unique($kategorioj_por_kanaloj));
}





/**
 * Determini kaj listigi la kategoriojn kiuj estas plej ofte uzataj por afiŝoj de la kanalo
 * a_spektejo_determini_plejoftajn_kategoriojn_por_kanalo
 *
 * @param int $kanalo_identigilo    la identigilo de la kanalo-termino
 *
 * @return array
 */
function a_spektejo_determini_plejoftajn_kategoriojn_por_kanalo($kanalo_identigilo)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton, mondo!', $protokoloID);
    // protokolu(null, 'la kanalo-identigilo estas ' . $kanalo_identigilo, $protokoloID);

    // 0. kontroli la minimuman grandecon de la kanalo por daŭrigi
    $kanalo_grandeco = intval(get_term_meta($kanalo_identigilo, 'v_kanalo_grandeco', true));
    if (empty($kanalo_grandeco)) {
        $kanalo_grandeco = get_term($kanalo_identigilo)->count;
    }
    if ($kanalo_grandeco <= 10) {
        // protokolu(null, 'la kanalo tro malgrandas (' . $kanalo_grandeco . ' publike-videblaj afiŝoj). ignoras ĝin...', $protokoloID);
        return [];
    }

    // 1. lisitig la afiŝojn de la kanalo
    $afisxoj_enkategoriigitaj_de_kanalo = get_posts(
        [
            'post_type'   => PROJEKTNOMO . '_video',
            'post_status' => 'publish',
            // 'fields'  => 'ids',
            'numberposts' => -1,
            'tax_query' => [
                'relation' => 'AND',
                [
                    'taxonomy' => 'v_kanalo',
                    'field'    => 'term_id',
                    'terms'    => $kanalo_identigilo,
                ],
                [
                    'taxonomy' => 'v_kategorioj',
                    'operator' => 'EXISTS',
                ]
            ]
        ]
    );
    if (count($afisxoj_enkategoriigitaj_de_kanalo) <= 10) {
        // protokolu(null, 'la la nombro de enkategoriigitaj afiŝoj de la kanalo tro malgrandas (' . count($afisxoj_enkategoriigitaj_de_kanalo) . ' afiŝoj). ignoras ĝin...', $protokoloID);
        return [];
    }

    // 2. listigi kategoriojn
    $kanalo_kategorioj = [];
    foreach ($afisxoj_enkategoriigitaj_de_kanalo as $afisxo) {
        $afisxo_identigilo = $afisxo->ID;
        $afisxo_kategorioj = array_keys(a_spektejo_retrovi_terminoliston_por_afisxo($afisxo_identigilo, 'v_kategorioj'));

        $kanalo_kategorioj = array_merge($kanalo_kategorioj, $afisxo_kategorioj);
    }

    // protokolu($kanalo_kategorioj, 'jen la kruda listo de la kategorioj de la kanalo (kun ' . $kanalo_grandeco . ' afiŝoj entute):', $protokoloID);

    $kanalo_kategorionombroj = array_filter(
        array_count_values($kanalo_kategorioj),
        function ($nombro_por_kategorio) use ($kanalo_grandeco) {
            if ($nombro_por_kategorio > 10 && ($nombro_por_kategorio / $kanalo_grandeco) >= 0.4) {
                return true;
            }
        }
    );
    arsort($kanalo_kategorionombroj, SORT_NUMERIC);

    // protokolu($kanalo_kategorionombroj, 'jen la filtrita listo de kategorioj, kun nombroj:', $protokoloID);

    return $kanalo_kategorionombroj;
}








/* == KOMENTOJ == */

/**
 * Re-traduki kampojn de la komentoformularo por konformi al WP-normoj.
 */
function a_spektejo_retraduki_komentokampojn()
{

    if ('POST' !== $_SERVER['REQUEST_METHOD'] || wp_doing_ajax() || empty($_POST)) {
        return null;
    }

    if (stripos($_SERVER['REQUEST_URI'], 'wp-comments-post.php') === false) {
        return null;
    }

    // $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton, mondo!', $protokoloID);
    // protokolu($_SERVER['REQUEST_URI'], 'jen la peto-adreso:', $protokoloID);
    // protokolu($_POST, 'jen la peto-argumentoj:', $protokoloID);

    $_POST['author']  = $_POST['komentoverkanto'] ?? '';
    $_POST['comment'] = $_POST['komentoteksto']   ?? '';

    // protokolu($_POST, 'jen la modifitaj peto-argumentoj:', $protokoloID);
}
add_filter('init', 'a_spektejo_retraduki_komentokampojn', 0);





/**
 * Kontroli ĵus-senditan komenton
 *
 * @param array  $komentodatumoj
 *
 * @return array
 */
function a_spektejo_antauxkontroli_komenton($komentodatumoj)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton, mondo!', $protokoloID);
    protokolu($_POST, 'jen la tuta _POST', $protokoloID);
    protokolu($komentodatumoj, 'jen la komentodatumaro:', $protokoloID);

    if (! empty($_POST['kaptilo'])) {
        wp_die();
    }


    // kontroli la komenton
    if (! isset($_POST['komentoteksto'])
        || empty(trim($_POST['komentoteksto']))
    ) {
        wp_die('Eraro: Vi ne skribis ion en la komentujo. Bonvolu reveni je la lasta paĝo kaj reafiŝi vian post entajpi vian komenton.');
    }


    // kontroli la nomon
    if (! is_user_logged_in()
        && (       ! isset($_POST['komentoverkanto'])
        || empty(trim($_POST['komentoverkanto'])))
    ) {
        wp_die('Eraro: Vi ne skribis nomon. Bonvolu reveni je la lasta paĝo kaj reafiŝi vian komenton post entajpi nomon/kromnomon.');
    }

    $kanaloj_nomoj = get_terms(
        [
            'taxonomy' => 'v_kanalo',
            'fields'   => 'names',
        ]
    );

    $nomoreguloj = [
        ' ' => '',
        '-' => '',
        '_' => ''
    ];

    $kanaloj_nomoj_m = array_map(
        function ($ero) use ($nomoreguloj) {
            return strtr(mb_strtolower($ero), $nomoreguloj);
        },
        $kanaloj_nomoj
    );
    $nomo_m = strtr(mb_strtolower($_POST['komentoverkanto']), $nomoreguloj);

    // protokolu($kanaloj_nomoj_m, 'jen la kanalonomoj, minuskle', $protokoloID);
    protokolu($nomo_m, 'jen la nomo, minuskle', $protokoloID);

    $nomo_kanalo_indekso = array_search($nomo_m, $kanaloj_nomoj_m);

    if ($nomo_kanalo_indekso !== false) {
        $eraromesagxo = 'La nomo <i>' . $_POST['komentoverkanto'] . '</i> estas rezervita por la aŭtoro de la kanalo <i>' . $kanaloj_nomoj[$nomo_kanalo_indekso] . '</i>. ' .
            'Bonvolu reveni al la lasta paĝo kaj reafiŝi vian komenton post entajpi alian nomon. <br>';

        $kontaktinformoj      = get_option(PROJEKTNOMO . '_kontaktinformoj', '');
        $administra_retadreso = $kontaktinformoj['retadreso-admin'] ?? '';
        if (!empty($administra_retadreso)) {
            $eraromesagxo .= 'Se ĉi tiu nomo apartenas al vi, bonvolu skribi al ' .
                '<a href="mailto:' . $administra_retadreso . '">' . $administra_retadreso . '</a> ' .
                'por aktivigi ĝin por vi.';
        }

        wp_die($eraromesagxo);
    }

    // kontroli la respondon al la demando
    $demandoindekso = (isset($_POST['demando-id'])) ? intval($_POST['demando-id']) : false;

    $demandoj = get_option(PROJEKTNOMO . '_sekurdemandoj', []);
    if (! empty($demandoj)) {
        $respondo = $demandoj[$demandoindekso]['respondo'];
    }

    if (! is_user_logged_in()
        && (empty($_POST['demando'])
        || ! isset($respondo)
        || mb_strtolower($_POST['demando']) !== mb_strtolower($respondo))
    ) {
        wp_die('Eraro: Vi ne respondis al la demando, aŭ via respondo estis malĝusta. Revenu je la lasta paĝo kaj reafiŝu vian komenton post respondi al la demando.');
    }

    $komentodatumoj['comment_author']  = $_POST['komentoverkanto'];
    $komentodatumoj['comment_content'] = $_POST['komentoteksto'];

    protokolu($komentodatumoj, 'jen la preparitaj komentodatumoj:', $protokoloID);

    return $komentodatumoj;
}
add_filter('preprocess_comment', 'a_spektejo_antauxkontroli_komenton');






/**
 * Konservi komentinton por video
 * a_spektejo_konservi_komentinton_por_video
 *
 * @param int        $komentoID
 * @param WP_COMMENT $komento
 */
// TODO: Konsideru ankaŭ forigon de komento
/* function a_spektejo_konservi_komentinton_por_video($komentoID, $komento)
{
    $komento_afisxo_ID = $komento->comment_post_ID;

    a_spektejo_aldoni_aganton_por_video($komento_afisxo_ID, 'v_komentintoj');

    // $komentinto  = a_spektejo_kiu_estas_tiu_cxi();
    // $komentintoj = get_post_meta($identigilo, 'v_komentintoj', true);
    // if (empty($komentintoj)) {
    //     $komentintoj = [];
    // }
    // $komentintoj[] = $komentinto;

    // $aldoni_komentinton_rezulto = update_post_meta($komento_afisxo_ID, 'v_komentintoj', array_unique($komentintoj));

    // protokolu($aldoni_komentinton_rezulto, 'jen la rezulto por aldoni la komentinton:', $protokoloID);
} */
// add_action('wp_insert_comment', 'a_spektejo_konservi_komentinton_por_video', 97, 2);


/**
 * Ĝisnunigi la komentoamplekson por video
 * a_spektejo_konservi_komentoamplekson_por_video
 *
 * @param int        $komentoID
 * @param WP_COMMENT $komento
 */
// TODO: Konsideru ankaŭ forigon de komento
function a_spektejo_konservi_komentoamplekson_por_video($komentoID, $komento)
{
    $komento_afisxo_ID = $komento->comment_post_ID;
    $komento_enhavo    = $komento->comment_content;

    $vortonombro       = count(preg_split('/[^\p{L}\p{N}\']+/iu', $komento_enhavo));

    $vortonombro_jama  = get_post_meta($komento_afisxo_ID, 'v_komentoamplekso', true);
    $vortonombro      += intval($vortonombro_jama);

    update_post_meta($komento_afisxo_ID, 'v_komentoamplekso', strval($vortonombro));
}
add_action('wp_insert_comment', 'a_spektejo_konservi_komentoamplekson_por_video', 98, 2);





/**
 * Ĝisdatigi la tempon por la lasta komento por video
 * a_spektejo_konservi_lastekomentitan_tempon_por_video
 *
 * @param int        $komentoID
 * @param WP_COMMENT $komento // FIXME
 */
// TODO: Konsideru ankaŭ forigon de komento
function a_spektejo_konservi_lastekomentitan_tempon_por_video($komentoID, $komento)
{
    $komento_afisxo_ID = $komento->comment_post_ID;
    $komento_tempo     = strtotime($komento->comment_date);

    update_post_meta($komento_afisxo_ID, 'v_komento_lasta', $komento_tempo);
}
add_action('wp_insert_comment', 'a_spektejo_konservi_lastekomentitan_tempon_por_video', 99, 2);
// add_action('delete_comment','a_spektejo_konservi_lastekomentitan_tempon_por_video', 99, 2); // FIXME





/**
 * Retrovi la uzantoidentigilon aŭ anoniman haketaĵon
 * de tiu, kiu verkis la komenton
 * a_spektejo_retrovi_auxtoron_de_komento
 *
 * @param int $identigilo
 */
function a_spektejo_retrovi_auxtoron_de_komento($identigilo)
{
    $komento = get_comment($identigilo);

    if (! empty($komento->$user_id)) {
        $komentinto = $komento->$user_id;
    } elseif (! empty($komento->comment_author_IP)) {
        $komentinto = $komento->comment_author_IP;
        // Ĉi tio ne estas la vera IP-adreso. Ĝi estas fakte anonima haketaĵo,
        // kiu estas konservita anstataŭ IP-adreso en la datumbazo.
        // (Rigardu la funkcion a_spektejo_malmemori_ip_en_komentoj)
    } else {
        $komentinto = '';
    }

    return $komentinto;
}





/*
 * Videbligi kaŝitajn komentojn por administrantoj
 * a_spektejo_montri_cxiujn_komentojn_por_administranto
 *
 * @param WP_Comment_Query    $komentopeto
 */
function a_spektejo_montri_cxiujn_komentojn_por_administranto($komentopeto)
{
    if (!a_spektejo_cxu_uzanto_rajtas('komento-marki-maltauxga') && !a_spektejo_cxu_uzanto_rajtas('komento-signalaprobi-maltauxga')) {
        return;
    }

    // $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu($komentopeto, 'jen la komentopeto:', $protokoloID);
    // protokolu($komentopeto->query_vars, 'jen la argumentaro por la komentopeto:', $protokoloID);

    $komentopeto->query_vars['status'] = 'all';

    // protokolu($komentopeto->query_vars, 'jen la modifita argumentaro:', $protokoloID);
}
add_action('pre_get_comments', 'a_spektejo_montri_cxiujn_komentojn_por_administranto', 99, 1);





/* == KTP == */


// _modifi_kanalan_videoliston // TODO: Kontroli
