<?php

/**
 * Antikvaj funkcioj
 *
 * @package Spektejo
 */







/**
 * Analizi aktivadon dum vizito por ekscii kiu parto ricevis pli da atento
 * a_spektejo_analizi_vizitopoziciojn
 *
 * @param array   $pozicioj
 *
 * @return array
 */
function a_spektejo_analizi_vizitopoziciojn($pozicioj)
{

    if (! is_array($pozicioj) || empty($pozicioj)) {
        return [];
    }

    $pozicioj_analizitaj = [];

    $pozicio_sekva_indekso = 1;
    foreach ($pozicioj as $pozicio) {
        if (isset($pozicioj[$pozicio_sekva_indekso])
            && isset($pozicioj[$pozicio_sekva_indekso]['tempo'])
            && isset($pozicio['tempo'])
            && isset($pozicio['supro'])
            && isset($pozicio['fundo'])
            && isset($pozicio['cxefzonoj'])
            && is_array($pozicio['cxefzonoj'])
        ) {
            $pozicio_analizita = [];

            $p_tempo       = intval($pozicio['tempo']);
            $p_sekva_tempo = intval($pozicioj[$pozicio_sekva_indekso]['tempo']);
            $p_dauxro      = $p_sekva_tempo - $p_tempo;

            $p_supro       = intval($pozicio['supro']);
            $p_fundo       = intval($pozicio['fundo']);
            $p_alteco      = $p_fundo - $p_supro;
            $p_cxefzonoj   = $pozicio['cxefzonoj'];

            $p_a_cxefzonoj = [];

            foreach ($p_cxefzonoj as $p_cxefzono => $p_cxefzono_limoj) {
                if (isset($p_cxefzono_limoj['supro'])
                    && isset($p_cxefzono_limoj['fundo'])
                ) {
                    $p_cz_supro = intval($p_cxefzono_limoj['supro']);
                    $p_cz_fundo = intval($p_cxefzono_limoj['fundo']);

                    if ($p_supro < $p_cz_fundo
                        && $p_fundo > $p_cz_supro
                    ) { // almenaŭ parto de la zono estis en la videjo
                        $p_cz_alteco = $p_cz_fundo - $p_cz_supro;

                        if ($p_supro < $p_cz_supro
                            && $p_fundo > $p_cz_fundo
                        ) { // la zono estis tute en la videjo
                            $p_cz_alteco_videbla = $p_cz_alteco;
                        } else {
                            if ($p_supro > $p_cz_supro && $p_fundo > $p_cz_fundo) {
                                // parte videbla supre en la videjo
                                $p_cz_alteco_videbla = $p_cz_fundo - $p_supro;
                            } elseif ($p_supro < $p_cz_supro && $p_fundo < $p_cz_fundo) {
                                // parte videbla malsupre en la videjo
                                $p_cz_alteco_videbla = $p_fundo - $p_cz_supro;
                            } elseif ($p_supro > $p_cz_supro && $p_fundo < $p_cz_fundo) {
                                // parte videbla en la tuta videjo (zono pli alta ol la videjo)
                                $p_cz_alteco_videbla = $p_fundo - $p_supro;
                            }
                        }

                        if ($p_cz_alteco_videbla > 100) {
                            $p_cz_alteco_videbla_proporcio  = $p_cz_alteco_videbla / $p_cz_alteco;
                            $p_a_cxefzonoj[$p_cxefzono]   = $p_cz_alteco_videbla_proporcio;
                        } else {
                            // La zono estis apenaŭ videbla
                        }
                        // fino: parto de la zono en la videjo
                    } else {
                        // La zono ne estis videbla
                    }
                }
            } // ĉiu ĉefzono


            if (! empty($p_a_cxefzonoj)) {
                $pozicio_analizita['dauxro'] = $p_dauxro;
                $pozicio_analizita['zonoj']  = $p_a_cxefzonoj;
                $pozicioj_analizitaj[]         = $pozicio_analizita;
            }
        }

        $pozicio_sekva_indekso++;
    } // ĉiu pozicio

    return $pozicioj_analizitaj;
}
















/**
 * Retrovas antaŭe-konservitan liston de afiŝoj rilataj al iu afiŝo
 * a_spektejo_retrovi_rilatajn_afisxojn
 *
 * @param int    $identigilo    identigilo de la afiŝo
 * @param int    $nombro        nombro de dezirataj rilataj afiŝoj
 *
 * @return array  aranĝaĵo de rilataj afisoj, en la formo de WP_POST-objektoj
 */
// FIXME: Forigu
function a_spektejo_retrovi_rilatajn_afisxojn($identigilo, $nombro = 12)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton!', $protokoloID);

    $rilataj_afisxoj_identigiloj = get_post_meta($identigilo, 'v_rilataj_videoj', true);

    // protokolu($rilataj_afisxoj_identigiloj, 'jen la identigiloj de la rilataj afiŝoj:', $protokoloID);

    // FIXME
    $rilataj_afisxoj = [];

    $rilataj_afisxoj = array_slice($rilataj_videoj, 0, $nombro, true);

    return $rilataj_afisxoj;
}
