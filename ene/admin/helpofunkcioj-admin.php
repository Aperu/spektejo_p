<?php

/**
 * Helpaj funkcioj por administraj agoj
 *
 * @package Spektejo
 */

if (! defined('ABSPATH')) {
    exit;
}





/**
 * == AFIŜOJ ==
 * funkcioj por procedi datumojn pri afiŝoj
 */




/**
 * Marki enhaveron (video, kanalo, komento ktp)
 * a_spektejo_marki_eron
 *
 * @param string $speco        la speco de la ero (ĉu 'video', 'kanalo', 'komento')
 * @param int    $identigilo   la identigilo de la ero
 * @param string $markovaloro  la valoro de la marko
 * @param string $markospeco   la eco, per kiu marki la eron
 *
 * @return array   raporto pri la rezulto
 */
function a_spektejo_marki_eron($speco, $identigilo, $markovaloro, $markospeco = '')
{
    // 1. prepari la datumojn

    if (empty($speco)
        || empty($identigilo)
        || $identigilo <= 0
        || empty($markovaloro)
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    $identigilo = intval($identigilo);


    // 2. Alvoki taŭgan funkcion laŭ la speco

    switch ($speco) {
        case 'video':
            $marki_eron_rezulto = a_spektejo_marki_videon($identigilo, $markovaloro, $markospeco);
            break;

        case 'kanalo':
            $marki_eron_rezulto = a_spektejo_marki_kanalon($identigilo, $markovaloro);
            break;

        case 'komento':
            $marki_eron_rezulto = a_spektejo_marki_komenton($identigilo, $markovaloro);
            break;

        default:
            return [
                'mesagxo_malsukcesa' => 'Nekonata ero-speco!',
                'kodo_malsukcesa'    => 'erospeco_malvalida'
            ];
    }

    // 3. Raporti pri la rezulto
    return $marki_eron_rezulto;
}





/**
 * Marki videoafiŝon
 * a_spektejo_marki_videon
 *
 * @param int    $identigilo   la identigilo de la videoafiŝo
 * @param string $valoro       la valoro de la marko
 * @param string $eco          la eco, per kiu marki la afiŝon
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_marki_videon($identigilo, $valoro, $eco)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton, mondo!', $protokoloID);
    // protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);

    // 1. prepari la datumojn

    if (! isset($identigilo)
        || $identigilo <= 0
        || empty($eco)
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    $identigilo = intval($identigilo);


    // 2. Alvoki taŭgan funkcion laŭ la eco

    switch ($eco) {
        case 'rilateco':
            $marki_videon_rezulto = a_spektejo_marki_videon_per_rilateco($identigilo, $valoro);
            break;
        case 'kategorio':
            $marki_videon_rezulto = a_spektejo_marki_videon_per_kategorio($identigilo, $valoro);
            break;
        case 'lingvo':
            $marki_videon_rezulto = a_spektejo_marki_videon_per_lingvo($identigilo, $valoro);
            break;
        case 'atribuo':
            $marki_videon_rezulto = a_spektejo_marki_videon_per_atribuo($identigilo, $valoro);
            break;
        default:
            return [
                'mesagxo_malsukcesa' => 'Nekonata ago!',
                'kodo_malsukcesa'    => 'markospeco_malvalida'
            ];
    }


    // 3. Raporti pri la rezulto
    return $marki_videon_rezulto;
}


/**
 * Forigi markon de videoafiŝo
 * a_spektejo_malmarki_videon
 *
 * @param int    $identigilo   la identigilo de la videoafiŝo
 * @param string $valoro       la valoro de la forigota marko
 * @param string $eco          la eco, per kiu la afiŝo estis markita (`rilateco`, `kategorio`, `lingvo`, `atribuo`)
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_malmarki_videon($identigilo, $valoro, $eco)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton, mondo!', $protokoloID);
    // protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);

    // 1. prepari la datumojn

    if (! isset($identigilo)
        || $identigilo <= 0
        || empty($valoro)
        || empty($eco)
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    $identigilo = intval($identigilo);

    switch ($eco) {
        case 'rilateco':
            $taksonomio = 'v_rilateco';
            break;
        case 'kategorio':
            $taksonomio = 'v_kategorioj';
            break;
        case 'lingvo':
            $taksonomio = 'v_lingvo';
            break;
        case 'atribuo':
            $taksonomio = 'v_atribuoj';
            break;
        default:
            return [
                'mesagxo_malsukcesa' => 'Nekonata ago!',
                'kodo_malsukcesa'    => 'markospeco_malvalida'
            ];
    }

    // 2. forigi la terminon el la terminaro de la ĝusta taksonomio

    $terminoj = a_spektejo_retrovi_terminoliston_por_afisxo($identigilo, $taksonomio);
    unset($terminoj[$valoro]);
    $malmarki_videon_rezulto = wp_set_object_terms($identigilo, array_keys($terminoj), $taksonomio, false);

    // 3. Raporti pri la rezulto

    if (is_wp_error($malmarki_videon_rezulto)) {
        return [
            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste',
            'kodo_malsukcesa'    => 'ne_sukcesis_malmarki_videon'
        ];
    }

    return [
        'mesagxo_sukcesa' => 'Sukcese malmarkis la filmon.',
        'kodo_sukcesa'    => 'sukcese_malmarkis_videon',
    ];
}




/**
 * Marki videoafiŝon per la taksonomio "rilateco"
 * a_spektejo_marki_videon_per_rilateco
 *
 * @param int    $identigilo
 * @param string $rilateco
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_marki_videon_per_rilateco($identigilo, $rilateco)
{

    // 0. Kontroli aliron
    if (!a_spektejo_cxu_uzanto_rajtas('afisxo-marki-rilateco')
        && !a_spektejo_cxu_uzanto_rajtas('afisxo-signalaprobi-problemo')
    ) {
        return [
            'mesagxo_malsukcesa' => 'Vi ne havas aliron al ĉi tio!',
            'kodo_malsukcesa'    => 'ago_malpermesite'
        ];
    }

    // 1. prepari la datumojn

    if (! isset($identigilo)
        || $identigilo <= 0
        || empty($rilateco)
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    $identigilo = intval($identigilo);


    // 2. Marki la videon (ŝanĝi la "rilatecon")

    switch ($rilateco) {
        case 'esperanta':
            $sxangxi_lingvon_rezulto = wp_set_object_terms($identigilo, 'eo', 'v_lingvo', false);
            break;
        case 'priesperanta':
            // TODO: Nur se la lingvo estas 'eo'. alikaze ne ŝanĝu la lingvon.
            $sxangxi_lingvon_rezulto = wp_set_object_terms($identigilo, 'xx', 'v_lingvo', false);
            break;
        case 'nerilata':
        case 'spama':
        case 'nedisponebla':
        case 'neoriginala':
            // forigi problemosignalojn por kaŝota afiŝo
            a_spektejo_forigi_signalon('video', $identigilo, 'nerilata');
            a_spektejo_forigi_signalon('video', $identigilo, 'spama');
            a_spektejo_forigi_signalon('video', $identigilo, 'nedisponebla');
            a_spektejo_forigi_signalon('video', $identigilo, 'neoriginala');
            break;

        default:
            return [
                'mesagxo_malsukcesa' => 'Nekonata ago!',
                'kodo_malsukcesa'    => 'rilateco_malvalida'
            ];
    }
    $marki_videon_rezulto = wp_set_object_terms($identigilo, $rilateco, 'v_rilateco', false);

    // 2.1. forigi la uzanto-signalojn por la markata rilateco
    a_spektejo_forigi_signalon('video', $identigilo, $rilateco);

    // 2.2. ĝisnunigi la grandecon de la kanalo
    $kanalo_nometo = wp_get_post_terms($identigilo, 'v_kanalo')[0]->slug;
    a_spektejo_gxisnunigi_kanalograndecon($kanalo_nometo);


    // 3. Raporti pri la rezulto

    if (is_wp_error($marki_videon_rezulto)) {
        return [
            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste',
            'kodo_malsukcesa'    => 'ne_sukcesis_marki_videon'
        ];
    }

    return [
        'mesagxo_sukcesa' => 'Sukcese markis la filmon.',
        'kodo_sukcesa'    => 'sukcese_markis_videon',
    ];
}





/**
 * Marki videoafiŝon per la taksonomio "rilateco"
 * a_spektejo_marki_videon_per_atribuo
 *
 * @param int    $identigilo
 * @param string $atribuo
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_marki_videon_per_atribuo($identigilo, $atribuo)
{

    // 0. Kontroli aliron
    /* if (!a_spektejo_cxu_uzanto_rajtas('afisxo-marki-atribuo')) {
        return [
            'mesagxo_malsukcesa' => 'Vi ne havas aliron al ĉi tio!',
            'kodo_malsukcesa'    => 'ago_malpermesite'
        ];
    } */

    // 1. prepari la datumojn

    if (! isset($identigilo)
        || $identigilo <= 0
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    $identigilo = intval($identigilo);


    // 2. Marki la videon (ŝanĝi la "rilatecon")

    switch ($atribuo) {
        case 'gxisnunigenda':
            break;

        // TODO!

        default:
            return [
                'mesagxo_malsukcesa' => 'Nekonata ago!',
                'kodo_malsukcesa'    => 'atribuo_malvalida'
            ];
    }
    $marki_videon_rezulto = wp_set_object_terms($identigilo, $atribuo, 'v_atribuoj', false);



    // 3. Raporti pri la rezulto

    if (is_wp_error($marki_videon_rezulto)) {
        return [
            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste',
            'kodo_malsukcesa'    => 'ne_sukcesis_marki_videon'
        ];
    }

    return [
        'mesagxo_sukcesa' => 'Sukcese markis la filmon.',
        'kodo_sukcesa'    => 'sukcese_markis_videon',
    ];
}






/**
 * Marki videoafiŝon per la taksonomio "kategorio"
 * a_spektejo_marki_videon_per_kategorio
 *
 * @param int    $identigilo
 * @param array  $kategorioj
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_marki_videon_per_kategorio($identigilo, $kategorioj)
{

    // 0. Kontroli aliron
    if (!a_spektejo_cxu_uzanto_rajtas('afisxo-marki-kategorio')
        && !a_spektejo_cxu_uzanto_rajtas('afisxo-signalaprobi-kategorio')
    ) {
        return [
            'mesagxo_malsukcesa' => 'Vi ne havas aliron al ĉi tio!',
            'kodo_malsukcesa'    => 'ago_malpermesite'
        ];
    }

    // 1. prepari la datumojn

    if (! isset($identigilo)
        || $identigilo <= 0
        || empty($kategorioj)
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    $identigilo = intval($identigilo);

    // FIXME
    // eble portempa maniero por decidi ĉu oni intencas aldoni al la jamaj kategorioj aŭ ĉu entute anstataŭigi
    if (is_array($kategorioj)) {
        $cxu_aldoni = false;
    } else {
        $cxu_aldoni = true;
    }


    // 2. Marki la videon (ŝanĝi la "kategoriojn")
    $marki_videon_rezulto = wp_set_object_terms($identigilo, $kategorioj, 'v_kategorioj', $cxu_aldoni);

    // 2.1. forigi la uzanto-signalojn por la markataj kategorioj
    if (is_array($kategorioj)) {
        foreach ($kategorioj as $kategorio) {
            a_spektejo_forigi_signalon('video', $identigilo, $kategorio);
        }
    } else {
        a_spektejo_forigi_signalon('video', $identigilo, $kategorioj);
    }

    // 2.2. envicigi la videon por poste rekalkuli ĝiajn rilatajn afiŝojn
    a_spektejo_envicigi_videon_por_rekalkuli_rilatajn_afisxojn($identigilo);


    // 2.3. ĝisnunigi la plej oftajn kategoriojn de la kanalo
    $kanalo_nometo = wp_get_post_terms($identigilo, 'v_kanalo')[0]->slug;
    a_spektejo_gxisnunigi_plejoftajn_kategoriojn_por_kanalo($kanalo_nometo);


    // 3. Raporti pri la rezulto

    if (is_wp_error($marki_videon_rezulto)) {
        return [
            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste',
            'kodo_malsukcesa'    => 'ne_sukcesis_marki_videon'
        ];
    }

    return [
        'mesagxo_sukcesa' => 'Sukcese markis la filmon.',
        'kodo_sukcesa'    => 'sukcese_markis_videon',
    ];
}





/**
 * Marki videoafiŝon per la taksonomio "lingvo"
 * a_spektejo_marki_videon_per_lingvo
 *
 * @param int    $identigilo
 * @param string $lingvo
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_marki_videon_per_lingvo($identigilo, $lingvo)
{
    // 0. Kontroli aliron
    if (!a_spektejo_cxu_uzanto_rajtas('afisxo-marki-lingvo')
        && !a_spektejo_cxu_uzanto_rajtas('afisxo-marki-rilateco')
        // INFO: Nur unu de tiuj permesiloj sufiĉas por atribui lingvon al afiŝo
    ) {
        return [
            'mesagxo_malsukcesa' => 'Vi ne havas aliron al ĉi tio!',
            'kodo_malsukcesa'    => 'ago_malpermesite'
        ];
    }

    // 1. prepari la datumojn

    if (! isset($identigilo)
        || $identigilo <= 0
        || empty($lingvo)
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    $identigilo = intval($identigilo);


    // 2. Marki la videon (ŝanĝi la "lingvon")

    if ($lingvo == 'eo') {
        $sxangxi_rilatecon_rezulto = wp_set_object_terms($identigilo, 'esperanta', 'v_rilateco', false);
    } else {
        $sxangxi_rilatecon_rezulto = wp_set_object_terms($identigilo, 'priesperanta', 'v_rilateco', false);
        // TODO: Konsideru ankaŭ eblan nerilatecon al esperanto
    }

    $marki_videon_rezulto = wp_set_object_terms($identigilo, $lingvo, 'v_lingvo', false);



    // 3. Raporti pri la rezulto

    if (is_wp_error($marki_videon_rezulto)) {
        return [
            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste',
            'kodo_malsukcesa'    => 'ne_sukcesis_marki_videon'
        ];
    }

    return [
        'mesagxo_sukcesa' => 'Sukcese markis la filmon.',
        'kodo_sukcesa'    => 'sukcese_markis_videon',
    ];
}







/**
 * Marki kanalon (kiel sekvata, ignorata aŭ normala)
 * a_spektejo_marki_kanalon
 *
 * @param int    $identigilo
 * @param string $inkluziveco
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_marki_kanalon($identigilo, $inkluziveco)
{

    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton!', $protokoloID);
    // protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);

    // 0. Kontroli aliron
    if (!a_spektejo_cxu_uzanto_rajtas('kanalo-marki-inkluziveco')
        && !a_spektejo_cxu_uzanto_rajtas('kanalo-signalaprobi-inkluziveco')
    ) {
        return [
            'mesagxo_malsukcesa' => 'Vi ne havas aliron al ĉi tio!',
            'kodo_malsukcesa'    => 'ago_malpermesite'
        ];
    }

    // 1. prepari la datumojn

    if (! isset($identigilo)
        || $identigilo <= 0
        || empty($inkluziveco)
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    $identigilo = intval($identigilo);

    if ($inkluziveco != 'inkludata'
        && $inkluziveco != 'ekskludata'
        && $inkluziveco != 'normala'
    ) {
        return [
            'mesagxo_malsukcesa' => 'Nekonata ago!',
            'kodo_malsukcesa'    => 'inkluziveco_malvalida'
        ];
    }



    // 2. Marki la kanalon (ŝanĝi la "inkluzivecon")

    $marki_kanalon_rezulto = update_term_meta($identigilo, 'v_kanalo_inkluziveco', $inkluziveco);

    // protokolu($marki_kanalon_rezulto, 'jen la rezulto:', $protokoloID);


    // 3. Raporti pri la rezulto

    if (! $marki_kanalon_rezulto || is_wp_error($marki_kanalon_rezulto)) {
        return [
            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste',
            'kodo_malsukcesa'    => 'ne_sukcesis_marki_kanalon'
        ];
    }

    return [
        'mesagxo_sukcesa' => 'Sukcese markis la kanalon.',
        'kodo_sukcesa'    => 'sukcese_markis_kanalon',
    ];
}





/**
 * Marki komenton (kiel maltaŭga aŭ spama)
 * a_spektejo_marki_komenton
 *
 * @param int    $identigilo
 * @param string $valoro
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_marki_komenton($identigilo, $valoro)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton!', $protokoloID);
    protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);

    // 0. Kontroli aliron
    if (!a_spektejo_cxu_uzanto_rajtas('komento-marki-maltauxga')
        && !a_spektejo_cxu_uzanto_rajtas('komento-signalaprobi-maltauxga')
    ) {
        return [
            'mesagxo_malsukcesa' => 'Vi ne havas aliron al ĉi tio!',
            'kodo_malsukcesa'    => 'ago_malpermesite'
        ];
    }

    // 1. prepari la datumojn

    if (! isset($identigilo)
        || $identigilo <= 0
        || empty($valoro)
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    $identigilo  = intval($identigilo);


    // 2. Marki la komenton (ŝanĝi la "staton")

    switch ($valoro) {
        case 'maltauxga':
            $stato = 'hold';
            break;
        case 'spama':
            $stato = 'spam';
            break;
        case 'normala':
            $stato = 'approve';
            break;
        default:
            return [
                'mesagxo_malsukcesa' => 'Nekonata ago!',
                'kodo_malsukcesa'    => 'markovaloro_malvalida'
            ];
    }

    $marki_komenton_rezulto = wp_set_comment_status($identigilo, $stato);

    protokolu($marki_komenton_rezulto, 'jen la rezulto:', $protokoloID);

    // 3. Raporti pri la rezulto

    if (!$marki_komenton_rezulto || is_wp_error($marki_komenton_rezulto)) {
        return [
            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste',
            'kodo_malsukcesa'    => 'ne_sukcesis_marki_komenton'
        ];
    }

    return [
        'mesagxo_sukcesa' => 'Sukcese markis la komenton.',
        'kodo_sukcesa'    => 'sukcese_markis_komenton',
    ];
}





/**
 * Marki uzanton
 * a_spektejo_marki_uzanton
 *
 * @param int    $identigilo
 * @param string $valoro
 * @param string $eco          la eco, per kiu la uzanto estis markita (`rajtoj`, `rolo`, `stato`,)
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_marki_uzanton($identigilo, $valoro, $eco)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton, mondo!', $protokoloID);
    // protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);

    // 1. prepari la datumojn

    if (! isset($identigilo)
        || $identigilo <= 0
        || empty($eco)
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    $identigilo = intval($identigilo);


    // 2. Alvoki taŭgan funkcion laŭ la eco

    switch ($eco) {
        case 'rolo':
            // $marki_uzanton_rezulto = a_spektejo_marki_uzanton_per_rolo($identigilo, $valoro);
            // TODO!
            break;
        case 'rajtoj':
            $marki_uzanton_rezulto = a_spektejo_marki_uzanton_per_rajtoj($identigilo, $valoro);
            break;
        case 'stato':
            // $marki_uzanton_rezulto = a_spektejo_marki_uzanton_per_stato($identigilo, $valoro);
            // TODO!
            break;
        default:
            return [
                'mesagxo_malsukcesa' => 'Nekonata ago!',
                'kodo_malsukcesa'    => 'markospeco_malvalida'
            ];
    }


    // 3. Raporti pri la rezulto
    return $marki_uzanton_rezulto;
}




/**
 * Atribui specifajn rajtojn al uzanto
 * a_spektejo_marki_uzanton_per_rajtoj
 *
 * @param int    $identigilo
 * @param string $rajtoj         la rajtoj, kiuj estos atribuitaj al la uzanto
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_marki_uzanton_per_rajtoj($identigilo, $rajtoj)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton!', $protokoloID);
    protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);

    // 1. prepari la datumojn

    if (empty($rajtoj) || empty(trim(strval($rajtoj)))) {
        $rajtoj = [];
    }

    if (!isset($identigilo)
        || $identigilo <= 0
        || !is_array($rajtoj)
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    $identigilo  = intval($identigilo);

    // 2. Marki la uzanton (ŝanĝi la valoron de la meta-kampo "rajtoj")
    $jamaj_rajtoj = get_user_meta($identigilo, 'u_rajtoj', true);
    if ($rajtoj == $jamaj_rajtoj) {
        // neniu ŝanĝo!
        return [
            'mesagxo_sukcesa' => 'Sukcese markis la uzanton.',
            'kodo_sukcesa'    => 'sama_markovaloro',
        ];
    }
    $marki_uzanton_rezulto = update_user_meta($identigilo, 'u_rajtoj', $rajtoj);

    protokolu($marki_uzanton_rezulto, 'jen la rezulto:', $protokoloID);

    // 3. Raporti pri la rezulto

    if (!$marki_uzanton_rezulto || is_wp_error($marki_uzanton_rezulto)) {
        return [
            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste',
            'kodo_malsukcesa'    => 'ne_sukcesis_marki_uzanton'
        ];
    }

    return [
        'mesagxo_sukcesa' => 'Sukcese markis la uzanton.',
        'kodo_sukcesa'    => 'sukcese_markis_uzanton',
    ];
}








/**
 * Aprobi kaj publikigi proponitan videon
 * a_spektejo_aprobi_videoproponon
 *
 * @param int   $identigilo
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_aprobi_videoproponon($identigilo)
{

    // 1. prepari la datumojn

    if (! isset($identigilo)
        || $identigilo <= 0
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    // SENCIMIGO
    protokolu($identigilo, 'jen la identigilo: ');

    $identigilo       = intval($identigilo);
    $identigilo_fonto = get_post_meta($identigilo, 'v_identigilo_fonto', true);
    $fonto            = wp_get_post_terms($identigilo, 'v_fonto')[0]->slug;

    switch ($fonto) {
        case 'youtube':
            $fonto_kodo = 'ytb';
            break;
        default:
            return [
                'mesagxo_malsukcesa' => 'Nekonata platformo!',
                'kodo_malsukcesa'    => 'fonto_malvalida'
            ];
    }

    $nometo = $fonto_kodo . '_' . $identigilo_fonto;



    // 2. Aprobi la videon (publikigi la afiŝon)

    // Malaktivigi la limojn por la titoloj por ne ŝanĝi majusklojn en nometoj
    remove_filter('sanitize_title', 'sanitize_title_with_dashes');
    remove_filter('sanitize_title', 'a_spektejo_ebligi_majusklon_punktojn_streketojn_en_titoloj');

    $afisxodatumoj = [
        'ID'          => $identigilo ,
        'post_name'   => $nometo ,
        'post_status' => 'publish'
    ];
    $aproborezulto = wp_update_post($afisxodatumoj);



    // 3. Raporti pri la rezulto

    // eraro
    if ($aproborezulto == 0 || is_wp_error($aproborezulto)) {
        return [
            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste',
            'kodo_malsukcesa'    => 'ne_sukcesis_aprobi_videon'
        ];
    }

    // sukceso
    return [
        'mesagxo_sukcesa' => 'Sukcese aprobis la videon.',
        'kodo_sukcesa'    => 'sukcese_aprobis_videon',
    ];
}





/**
 * Aprobi kaj publikigi proponitan kanalon
 * (uzas la funkcion `a_spektejo_marki_kanalon`)
 * a_spektejo_aprobi_kanaloproponon
 *
 * @param int   $identigilo
 *
 * @return array raporto pri la rezulto
 */
// FIXME: Forigi
function a_spektejo_aprobi_kanaloproponon($identigilo)
{

    // 1. Prepari la datumojn

    if (! isset($identigilo)
        || $identigilo <= 0
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }



    // 2. Aprobi la kanalon (marki ĝin kiel "inkludata")
    $aproborezulto = a_spektejo_marki_kanalon($identigilo, ['inkluziveco' => 'inkludata']);



    // 3. Raporti pri la rezulto
    return $aproborezulto;
}






/**
 * == UZANTOJ ==
 * funkcioj por konservi kaj modifi uzantojn
 */



/**
 * Krei aŭ modifi uzanton
 *
 * @param array $datumoj la datumoj por la uzanto
 * @param int $identigilo la identigilo de la uzanto, se la celo estas modifi ekzistantan uzanton
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_konservi_uzanton($datumoj, $identigilo = 0)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton, mondo!', $protokoloID);
    protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);

    $nomo       = $datumoj['nomo']       ?? '';
    $uzantonomo = $datumoj['uzantonomo'] ?? '';
    $retadreso  = $datumoj['retadreso']  ?? '';
    $pasvorto   = $datumoj['pasvorto']   ?? '';
    $rolo       = $datumoj['rolo']       ?? ''; // FIXME: Aldonu defaŭltan rolon
    $rajtoj     = $datumoj['rajtoj']     ?? [];

    $roloj_validaj = ['helpulo']; // la rolo `administrator` devas NENIAM esti ĉi tie
    if (!in_array($rolo, $roloj_validaj, true)
        || ($identigilo > 0 && user_can($identigilo, 'administrator')) // Malpermesi redakti administrajn uzantojn
    ) {
        return [
            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste.',
            'kodo_malsukcesa'    => 'rolo_malvalida'
        ];
    }

    $uzanto_datumoj = [
        'user_login' => $uzantonomo,
        // 'user_nicename' => '',
        'user_email' => $retadreso,
        'display_name' => $nomo,
        // 'nickname' => '',
        'role' => $rolo,
        'meta_input' => [
            'rajtoj' => $rajtoj
        ]
    ];

    if (is_int($identigilo) && $identigilo > 0) {
        // redaktas ekzistantan uzanton
        $uzanto = get_user_by('id', $identigilo);
        if (empty($pasvorto)) {
            // Ne ŝanĝi la pasvorton
            $pasvorto_preta = $uzanto->data->user_pass;
        } else {
            $pasvorto_preta = wp_hash_password($pasvorto);
        }
        $uzanto_datumoj['ID'] = $identigilo;
        $uzanto_datumoj['user_pass'] = $pasvorto_preta;
    } else {
        // kreas novan uzanton
        $uzanto_datumoj['user_pass'] = $pasvorto; // la funkcio `wp_insert_user` mem haketas la pasvorton por novaj uzantoj
    }

    protokolu($uzanto_datumoj, 'jen la datumoj laŭ kiu kreos uzanton:', $protokoloID);

    $rezulto = wp_insert_user($uzanto_datumoj);

    protokolu($rezulto, 'jen la rezulto:', $protokoloID);

    if (is_wp_error($rezulto)) {
        return [
            'mesagxo_malsukcesa' => 'Malsukcese. Provu poste.',
            'kodo_malsukcesa'    => 'ne_sukcesis_konservi_uzanton'
        ];
    }

    return [
        'mesagxo_sukcesa' => 'Sukcese konservis la uzanton.',
        'kodo_sukcesa'    => 'sukcese_konservis_uzanton'
    ];
}







/**
 * == AGORDOJ ==
 * funkcioj por konservi agordojn
 */





/**
 * Konservi valoron por agordo
 * a_spektejo_konservi_agordon
 *
 * @param string $agordo
 * @param string $valoro
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_konservi_agordon($agordo, $valoro)
{

    // 1. Prepari la datumojn

    if (! isset($agordo)
        || empty($agordo)
        || ! isset($valoro)
        || empty($valoro)
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }


    // 2. Konservi la valoron, se la jama valoro ne estas sama

    $malplena_valoro = is_array($valoro) ? [] : '';
    $jama_valoro     = get_option($agordo, $malplena_valoro);

    if ($valoro === $jama_valoro) {
        return [
            'mesagxo_sukcesa' => 'Tiu valoro estas same kiel antaŭe. Nenio ŝanĝiĝis.',
            'kodo_sukcesa'    => 'samvalora_agordo'
        ];
    } else {
        $konservorezulto = update_option($agordo, $valoro);

        if ($konservorezulto === false) {
            return [
                'mesagxo_malsukcesa' => 'Malsukcese. Provu poste.',
                'kodo_malsukcesa'    => 'ne_sukcesis_konservi_agordon'
            ];
        }

        return [
            'mesagxo_sukcesa' => 'Sukcese ŝanĝis la agordon.',
            'kodo_sukcesa'    => 'sukcese_konservis_agordon'
        ];
    }
}





/**
 * Ŝanĝi la mesaĝojn, kiuj aperas por vizitantoj
 * (uzas la funkcion `a_spektejo_konservi_agordon`)
 * a_spektejo_konservi_glitmesagxojn
 *
 * @param array $datumoj
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_konservi_glitmesagxojn($nombro, $datumoj)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu($datumoj, 'jen la datumoj:', $protokoloID);
    protokolu($nombro, 'jen la nombro:', $protokoloID);

    if (! isset($datumoj)
        || empty($datumoj)
        || ! is_array($datumoj)
        || ! isset($nombro)
        || empty($nombro)
        || ! is_numeric($nombro)
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    $eroj = [];

    for ($n = 1; $n <= $nombro; $n++) {
        $ero_enhavoindekso     = 'glitmesagxo-' . $n . '-enhavo';
        $ero_kasxeblecoindekso = 'glitmesagxo-' . $n . '-kasxebleco';
        $ero_enhavo     = (isset($datumoj[$ero_enhavoindekso]))     ? $datumoj[$ero_enhavoindekso]      : ''; // FIXME: Sanigi
        $ero_kasxebleco = (isset($datumoj[$ero_kasxeblecoindekso])) ? $datumoj[$ero_kasxeblecoindekso]  : ''; // FIXME: Sanigi

        if (! empty($ero_enhavo)) {
            $eroj[]  = [
                'mesagxo'    => $ero_enhavo,
                'kasxebleco' => $ero_kasxebleco,
                'haketajxo'  => hash('adler32', $ero_enhavo),
            ];
        }
    }

    protokolu($eroj, 'jen la eroj:', $protokoloID);

    $rezulto = a_spektejo_konservi_agordon(PROJEKTNOMO . '_glitmesagxoj', $eroj);

    return $rezulto;
}





/**
 * Ŝanĝi la ligilojn en la piedo de la retejo
 * (uzas la funkcion `a_spektejo_konservi_agordon`)
 * a_spektejo_konservi_piedoligilojn
 *
 * @param array $datumoj
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_konservi_piedoligilojn($nombro, $datumoj)
{

    if (! isset($datumoj)
        || empty($datumoj)
        || ! is_array($datumoj)
        || ! isset($nombro)
        || empty($nombro)
        || ! is_numeric($nombro)
    ) {
        // malsufiĉe da datumoj!

        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    protokolu($nombro, 'la nombro estas:');
    protokolu($datumoj, 'la datumoj estas:');

    $eroj = [];

    for ($n = 1; $n <= $nombro; $n++) {
        $ero_titolo_indekso = 'piedoligilo-' . $n . '-titolo';
        $ero_adreso_indekso = 'piedoligilo-' . $n . '-adreso';
        $ero_pagxo_indekso  = 'piedoligilo-' . $n . '-pagxoID';

        $ero_titolo = (isset($datumoj[$ero_titolo_indekso])) ? sanitize_text_field($datumoj[$ero_titolo_indekso]) : '';
        $ero_adreso = (isset($datumoj[$ero_adreso_indekso])) ? esc_url($datumoj[$ero_adreso_indekso])             : '';
        $ero_pagxo  = (isset($datumoj[$ero_pagxo_indekso]))  ? absint($datumoj[$ero_pagxo_indekso])               : '';

        if (! empty($ero_titolo)
            && (! empty($ero_adreso) || ! empty($ero_pagxo))
        ) {
            $ero = [];
            $ero['titolo'] = $ero_titolo;

            if (! empty($ero_pagxo)) {
                $ero['pagxo_ID'] = $ero_pagxo;
                $ero_adreso      = get_permalink($ero_pagxo);
            }

            $ero['adreso'] = $ero_adreso;
            $eroj[]  = $ero;
        }
    }

    protokolu($eroj, 'la eroj estas jene:');

    $rezulto = a_spektejo_konservi_agordon(PROJEKTNOMO . '_piedoligiloj', $eroj);
    return $rezulto;
}





/**
 * Konservi la api-ŝlosilojn por eksteraj platformoj
 * (uzas la funkcion `a_spektejo_konservi_agordon`)
 * a_spektejo_konservi_apikodojn
 *
 * @param array $datumoj
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_konservi_apikodojn($datumoj)
{

    if (empty($datumoj)) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    // TODO: Konsideru aliajn platformojn

    $apikodo_youtube = (isset($datumoj['apikodo-youtube'])) ? sanitize_text_field($datumoj['apikodo-youtube']) : '';

    $rezulto         = a_spektejo_konservi_agordon(PROJEKTNOMO . '_apikodo_youtube', $apikodo_youtube);
    return $rezulto;
}









/**
 * == TABELOENHAVOJ ==
 * funkcioj por prepari kaj konsrtui tabeloenhavon el strukturaj datumoj
 */




function a_spektejo_prepari_administran_tabelodatumaron($eroj, $enhavospeco)
{
    if (empty($eroj)) {
        // FIXME: Ebligu aldoni eron kiam la tabelo ne havas erojn!
        return false;
    }

    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton, mondo!', $protokoloID);
    // protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);
    protokolu($enhavospeco, 'la enhavospeco estas:', $protokoloID);

    $datumoj = [];

    switch ($enhavospeco) {
        case 'video':
            $datumoj['kapo'] = ['Titolo', 'Kanalo', 'Kiamo', 'Daŭro', 'Enhavo', 'Ecoj', 'Statistikoj', 'Raportoj'];
            $datumoj['erospeco'] = 'video';
            $datumoj['eroj'] = [];
            foreach ($eroj as $ero) {
                $v_identigilo       = $ero->ID;
                $v_identigilo_fonto = get_post_meta($v_identigilo, 'v_identigilo_fonto', true);
                $v_titolo           = $ero->post_title;
                $v_kanalotermino    = wp_get_post_terms($v_identigilo, 'v_kanalo')[0];
                $v_auxtoro          = $v_kanalotermino->name;
                $v_kanalo           = $v_kanalotermino->slug;
                $v_kanalo_url       = add_query_arg(['part' => 'enhav', 'subpart' => 'vide', 'kanaloj[]' => $v_kanalo], home_url('/panelo'));
                $v_rilateco_termino = wp_get_post_terms($v_identigilo, 'v_rilateco')[0];
                $v_rilateco         = $v_rilateco_termino->slug;
                $v_rilateco_nomo    = $v_rilateco_termino->name;
                $v_fonto            = wp_get_post_terms($v_identigilo, 'v_fonto')[0]->slug;
                $v_lingvoj          = array_keys(a_spektejo_retrovi_terminoliston_por_afisxo($v_identigilo, 'v_lingvo'));
                $v_kategorioj       = array_values(a_spektejo_retrovi_terminoliston_por_afisxo($v_identigilo, 'v_kategorioj'));
                $v_identigilo_fonto = get_post_meta($v_identigilo, 'v_identigilo_fonto', true);
                $v_estas_propono    = (get_post_status($ero) == 'pending') ? 'jes' : 'ne';
                $v_adreso           = get_permalink($ero);

                $v_alsxutdato       = strtotime(get_post_meta($v_identigilo, 'v_alsxutdato', true));
                $v_alsxutdato_tago  = date('Y-m-d', $v_alsxutdato);
                $v_alsxutdato_tempo = date('H:m:s', $v_alsxutdato);
                $v_afisxdato        = strtotime($ero->post_date);
                $v_afisxdato_tago   = date('Y-m-d', $v_afisxdato);
                $v_afisxdato_tempo  = date('H:m:s', $v_afisxdato);
                $v_dauxro           = get_post_meta($v_identigilo, 'v_dauxro', true);
                $v_dauxro_DI        = (strpos($v_dauxro, 'P') === 0) ? new DateInterval($v_dauxro) : new DateInterval('PT0S');
                $v_dauxro_hms       = $v_dauxro_DI->format('%H:%I:%S');
                $v_spektoj          = intval(get_post_meta($v_identigilo, 'v_spektajxoj', true));
                $v_vocxoj           = intval(get_post_meta($v_identigilo, 'v_vocxoj', true));
                $v_komentoj_nr      = get_comments_number();
                $v_komentoj_ap      = intval(get_post_meta($v_identigilo, 'v_komentoamplekso', true));
                $v_populareco       = intval(get_post_meta($v_identigilo, 'v_populareco', true));
                $v_signaloj         = a_spektejo_retrovi_signalojn_por_ero('video', $v_identigilo);

                $v_signaloj_problemo  = $v_signaloj['problemo']  ?? [];
                $v_signaloj_kategorio = $v_signaloj['kategorio'] ?? [];

                $v_signalonombroj_problemoj = array_map(
                    function ($problemo_nometo) use ($v_signaloj_problemo) {
                        $signalonombro = $v_signaloj_problemo[$problemo_nometo];
                        return [
                            'enhavo' => $problemo_nometo . ' (' . $signalonombro . ')',
                            'klasoj' => ['signalo-problemo', 'problemo-' . $problemo_nometo]
                        ];
                    },
                    array_keys($v_signaloj_problemo)
                );
                $v_signalonombroj_kategorioj = array_map(
                    function ($kategorio_nometo) use ($v_signaloj_kategorio) {
                        $signalonombro = $v_signaloj_kategorio[$kategorio_nometo];
                        return [
                            'enhavo' => $kategorio_nometo . ' (' . $signalonombro . ')',
                            'klasoj' => ['signalo-kategorio', 'kategorio-' . $kategorio_nometo]
                        ];
                    },
                    array_keys($v_signaloj_kategorio)
                );

                protokolu($v_signaloj, 'jen la signaloj:', $protokoloID);

                $datumoj['eroj'][] = [
                    'identigilo' => $v_identigilo,
                    'kolumnoj'   => [
                        'titolo' => [
                            'enhavo'  => $v_titolo,
                            'url'     => $v_adreso,
                            'klasoj'  => []
                        ],
                        'auxtoro' => [
                            'enhavo'  => [
                                'kanalo' => [
                                    'enhavo' => $v_auxtoro,
                                    'url'    => $v_kanalo_url
                                ],
                                'fonto' => [
                                    'enhavo' => '(de "' . $v_fonto . '")'
                                ]
                            ],
                            'url'     => '',
                            'klasoj'  => []
                        ],
                        'tempoj' => [
                            'enhavo'  => [
                                'alsxuto' => [
                                    'titolo' => 'Alŝuto',
                                    'enhavo' => $v_alsxutdato_tago . ' ' . $v_alsxutdato_tempo
                                ],
                                'afisxo' => [
                                    'titolo' => 'Afiŝo',
                                    'enhavo' => $v_afisxdato_tago . ' ' . $v_afisxdato_tempo
                                ]
                            ],
                            'klasoj'  => []
                        ],
                        'dauxro' => [
                            'enhavo'  => $v_dauxro_hms,
                            'klasoj'  => []
                        ],
                        'enhavoecoj' => [
                            'enhavo'  => [
                                'lingvoj' => [
                                    'titolo' => 'Lingvoj',
                                    'enhavo' => $v_lingvoj,
                                    'klasoj' => ['lingvoj']
                                ],
                                'kategorioj' => [
                                    'titolo' => 'Kategorioj',
                                    'enhavo' => $v_kategorioj,
                                    'klasoj' => ['kategorioj']
                                ],
                            ],
                            'klasoj'  => []
                        ],
                        'sistemoecoj' => [
                            'enhavo'  => [
                                'rilateco' => [
                                    'titolo' => 'Rilateco',
                                    'enhavo' => $v_rilateco_nomo,
                                    'klasoj' => ['rilateco']
                                ],
                                'atribuoj' => [
                                    'titolo' => 'Atribuoj',
                                    'enhavo' => '',
                                    'klasoj' => ['atribuoj']
                                ],
                            ],
                            'klasoj'  => []
                        ],
                        'statistikoj' => [
                            'enhavo'  => [
                                'spektoj' => [
                                    'enhavo' => 'S: ' . $v_spektoj,
                                    'klasoj' => ['statistikoj-spektoj']
                                ],
                                'vocxoj' => [
                                    'enhavo' => 'V: ' . $v_vocxoj,
                                    'klasoj' => ['statistikoj-vocxoj']
                                ],
                                'komentoj' => [
                                    'enhavo' => 'K: ' . $v_komentoj_nr,
                                    'klasoj' => ['statistikoj-komentonombro']
                                ],
                                'komentoamplekso' => [
                                    'enhavo' => 'K-a: ' . $v_komentoj_ap,
                                    'klasoj' => ['statistikoj-komentoamplekso']
                                ],
                                'populareco' => [
                                    'enhavo' => 'P: ' . $v_populareco,
                                    'klasoj' => ['statistikoj-populareco']
                                ],
                            ],
                            'klasoj'  => []
                        ],
                        'signaloj' => [
                            'enhavo'  => [
                                'problemoj' => [
                                    'titolo' => 'Problemoj',
                                    'enhavo' => $v_signalonombroj_problemoj,
                                    'klasoj' => ['signaloj-problemoj']
                                ],
                                'kategorioj' => [
                                    'titolo' => 'Kategorioj',
                                    'enhavo' => $v_signalonombroj_kategorioj,
                                    'klasoj' => ['signaloj-kategorioj']
                                ],
                            ],
                            'klasoj'  => []
                        ],
                    ]
                ];
            }
            $datumoj['agoj'] = [
                // 'forigi'                        => 'Forigi',
                'marki--spama--rilateco'        => 'Marki kiel spamaĵo (kaj kaŝi)',
                'marki--nerilata--rilateco'     => 'Marki kiel nerilata/maltaŭga (kaj kaŝi)',
                'marki--nedisponebla--rilateco' => 'Marki kiel nedisponebla (kaj kaŝi)',
                'marki--neoriginala--rilateco'  => 'Marki kiel neoriginala (kaj kaŝi)',
                'marki--esperanta--rilateco'    => 'Marki kiel esperanta (kaj malkaŝi)',
                'marki--priesperanta--rilateco' => 'Marki kiel alilingva, rilata (kaj malkaŝi)',
                'marki--gxisnunigenda--atribuo' => 'Marki kiel renovigenda',
                // 'marki--nedifina--rilateco'     => 'Marki kiel nedifina (kaj malkaŝi)',
            ];
            break;

        case 'kanalo':
            // $datumoj['kapo'] = ['Titolo', 'Grandeco', 'Inkluziveco', 'Reagoj', 'Populareco', 'Raportoj'];
            $datumoj['kapo'] = ['Titolo', 'Grandeco', 'Inkluziveco'];
            $datumoj['erospeco'] = 'kanalo';
            $datumoj['eroj'] = [];
            foreach ($eroj as $ero) {
                $k_identigilo  = $ero->term_id;
                $k_titolo      = $ero->name;
                $k_url         = get_term_link($k_identigilo);
                $k_grandeco    = $ero->count;
                $k_fonto       = get_term_meta($k_identigilo, 'v_kanalo_fonto', true);
                $k_inkluziveco = get_term_meta($k_identigilo, 'v_kanalo_inkluziveco', true);

                $datumoj['eroj'][] = [
                    'identigilo' => $k_identigilo,
                    'kolumnoj'   => [
                        'titolo' => [
                            'enhavo'  => [
                                'titolo' => [
                                    'enhavo' => $k_titolo,
                                    'url'    => $k_url
                                ],
                                'fonto' => [
                                    'enhavo' => '(de "' . $k_fonto . '")'
                                ],
                            ],
                            'klasoj'  => []
                        ],
                        'grandeco' => [
                            'enhavo'  => $k_grandeco,
                            'klasoj'  => []
                        ],
                        'inkluziveco' => [
                            'enhavo'  => $k_inkluziveco,
                            'klasoj'  => []
                        ],
                    ]
                ];
            }
            $datumoj['agoj'] = [
                'marki--inkludata--inkluziveco'  => 'Sekvi',
                'marki--ekskludata--inkluziveco' => 'Ignori',
                'marki--normala--inkluziveco'    => 'Normaligi (malsekvi/malignori)'
            ];
            break;

        case 'komento':
            $datumoj['kapo'] = ['Aŭtoro', 'Afiŝo', 'Kiamo', 'Enhavo', 'Stato', 'Raportoj'];
            $datumoj['erospeco'] = 'komento';

            foreach ($eroj as $ero) {
                $komento_identigilo         = $ero->comment_ID;
                $komento_auxtoro            = $ero->comment_author;
                $komento_auxtoro_identigilo = $ero->comment_author_IP; // tio ne estas la vera IP, sed la anonima haketaĵo
                $komento_afisxo_ID          = $ero->comment_post_ID;
                $komento_afisxo_titolo      = get_post_field('post_title', $komento_afisxo_ID);
                $komento_afisxo_adreso      = get_permalink($komento_afisxo_ID);
                $komento_tempo              = $ero->comment_date;
                $komento_enhavo             = $ero->comment_content;
                $komento_amplekso           = count(preg_split('/[^\p{L}\p{N}\']+/iu', $komento_enhavo));
                $komento_resumo             = preg_replace('/\S+ ?$/', '…', substr($komento_enhavo, 0, 40));
                $komento_statokodo          = $ero->comment_approved;
                $komento_adreso             = get_comment_link($ero, ['per_page' => 30]); // FIXME: Dinamikigu la numeron
                $komento_deveno             = $ero->comment_parent;
                $komento_respondoj          = count($ero->get_children());
                $komento_signaloj           = a_spektejo_retrovi_signalojn_por_ero('komento', $komento_identigilo);
                $komento_signaloj_problemo  = $komento_signaloj['problemo']  ?? [];

                $komento_signalonombroj = array_map(
                    function ($problemo_nometo) use ($komento_signaloj_problemo) {
                        $signalonombro = $komento_signaloj_problemo[$problemo_nometo];
                        return [
                            'enhavo' => $problemo_nometo . ' (' . $signalonombro . ')',
                            'klasoj' => ['signalo-problemo', 'problemo-' . $problemo_nometo]
                        ];
                    },
                    array_keys($komento_signaloj_problemo)
                );

                protokolu($komento_signaloj, 'jen la signaloj:', $protokoloID);

                switch ($komento_statokodo) {
                    case '1':
                        $komento_stato = 'normala';
                        break;

                    case '0':
                        $komento_stato = 'kaŝita';
                        break;

                    case 'spam':
                        $komento_stato = 'spama';
                        break;

                    case 'trash':
                        $komento_stato = 'rubuje';
                        break;

                    default:
                        $komento_stato = '?';
                }

                $komento_deveno_respondoj  = '';
                $komento_deveno_respondoj .= ($komento_deveno > 0) ? '(Estas respondo, ' : '(';
                $komento_deveno_respondoj .= ($komento_respondoj > 0) ? $komento_respondoj . ' respondo(j))' : 'neniu respondo)';

                $datumoj['eroj'][] = [
                    'identigilo' => $komento_identigilo,
                    'kolumnoj'   => [
                        'auxtoro' => [
                            'enhavo'  => [
                                'nomo' => [
                                    'enhavo' => $komento_auxtoro
                                ],
                                'identigilo' => [
                                    'enhavo' => '(' . $komento_auxtoro_identigilo . ')'
                                ],
                            ],
                            'klasoj'  => []
                        ],
                        'afisxo' => [
                            'enhavo'  => $komento_afisxo_titolo,
                            'url'     => $komento_afisxo_adreso,
                            'klasoj'  => []
                        ],
                        'tempo' => [
                            'enhavo'  => $komento_tempo,
                            'klasoj'  => []
                        ],
                        'enhavo' => [
                            'enhavo'  => [
                                'teksto' => [
                                    'enhavo' => $komento_resumo,
                                    'klasoj' => []
                                ],
                                'amplekso' => [
                                    'enhavo' => $komento_amplekso . ' vorto(j)',
                                    'klasoj' => []
                                ],
                                'deveno-respondoj' => [
                                    'enhavo' => $komento_deveno_respondoj,
                                    'klasoj' => []
                                ],
                                'ligilo' => [
                                    'enhavo' => 'Rigardu',
                                    'url'    => $komento_adreso,
                                    'klasoj' => ['butoneto']
                                ],
                            ],
                            'klasoj'  => []
                        ],
                        'stato' => [
                            'enhavo'  => $komento_stato,
                            'klasoj'  => []
                        ],
                        'signaloj' => [
                            'enhavo' => $komento_signalonombroj,
                            'klasoj' => ['signaloj-problemoj'],
                        ],
                    ]
                ];
            }

            $datumoj['agoj'] = [
                'marki--maltauxga--stato' => 'Marki kiel maltaŭga (kaj kaŝi)',
                // 'marki--spama--stato'     => 'Marki kiel spamaĵo (kaj kaŝi)',
                'marki--normala--stato'   => 'Normaligi (kaj malkaŝi)'
            ];
            break;

        case 'uzanto':
            $datumoj['kapo'] = ['Nomo', 'Uzantonomo', 'Retadreso', 'Rolo(j)', 'Aldonaj rajtoj'];
            $datumoj['erospeco'] = 'uzanto';
            $datumoj['eroj'] = [];
            foreach ($eroj as $ero) {
                $u_identigilo  = $ero->ID;
                $u_nomo        = $ero->display_name;
                $u_uzantonomo  = $ero->user_login;
                $u_retadreso   = $ero->user_email;
                $u_roloj       = $ero->roles;
                $u_rajtoj_m    = get_user_meta($u_identigilo, 'u_rajtoj', true);
                $u_rajtoj      = (!empty($u_rajtoj_m) && is_array($u_rajtoj_m)) ? $u_rajtoj_m : [];
                $cxiuj_rajtoj = a_spektejo_retrovi_uzantorajtojn();
                $u_rajtoj_nomoj = array_map(
                    function ($rajto) use ($cxiuj_rajtoj) {
                        return $cxiuj_rajtoj[$rajto]['nomo'] ?? $rajto;
                    },
                    $u_rajtoj
                );

                $datumoj['eroj'][] = [
                    'identigilo' => $u_identigilo,
                    'datumoj'    => [
                        'rajtoj'     => implode(';', $u_rajtoj),
                        'identigilo' => $u_identigilo,
                        'nomo'       => $u_nomo,
                        'uzantonomo' => $u_uzantonomo,
                        'retadreso'  => $u_retadreso,
                        'roloj'      => $u_roloj,
                    ],
                    'kolumnoj' => [
                        'nomo' => [
                            'enhavo' => $u_nomo,
                            'klasoj'  => []
                        ],
                        'uzantonomo' => [
                            'enhavo'  => $u_uzantonomo,
                            'klasoj'  => []
                        ],
                        'retadreso' => [
                            'enhavo'  => $u_retadreso,
                            'klasoj'  => []
                        ],
                        'roloj' => [
                            'enhavo' => array_map(
                                function ($rolo) {
                                    return [
                                        'enhavo' => $rolo,
                                        'klasoj' => []
                                    ];
                                },
                                $u_roloj
                            ),
                            'klasoj' => []
                        ],
                        'rajtoj' => [
                            'enhavo'  => $u_rajtoj_nomoj,
                            'klasoj'  => []
                        ],
                    ],
                    'agilodatumoj' => [
                        'uzantoredaktilo' => [
                            'formularo' => 'uzantoredaktilo-formularo',
                            'kampo-uzanto_identigilo' => $u_identigilo,
                            'kampo-uzanto_nomo'       => $u_nomo,
                            'kampo-uzanto_uzantonomo' => $u_uzantonomo,
                            'kampo-uzanto_retadreso'  => $u_retadreso,
                            // 'kampo-roloj'       => $u_roloj,
                            'kampoatribuoj' => [
                                'uzanto_uzantonomo' => ['readonly' => 'readonly'],
                                'uzanto_pasvorto'   => ['placeholder' => '-- Ne tuŝate --']
                            ]
                        ],
                        'rajtoredaktilo' => [
                            'kampo-uzanto_identigilo' => $u_identigilo,
                            'kampo-uzanto-rajtoj' => implode(';', $u_rajtoj),
                            'formularo' => 'rajtoredaktilo-formularo',
                        ]
                    ]
                ];
            }
            $datumoj['eroagiloj'] = [
                'uzantoredaktilo' => [
                    'titolo'     => 'Redakti',
                    'identigilo' => 'redakti',
                    'malkasxato' => 'uzanto-redaktilo',
                    'klasoj'     => [
                        'plenigi-formularon'
                    ],
                ],
                'rajtoredaktilo' => [
                    'titolo'     => 'Rajtoj',
                    'identigilo' => 'rajtoj',
                    'malkasxato' => 'uzanto-rajtoelektilo',
                    'klasoj'     => [
                        'plenigi-formularon'
                    ],
                ]
            ];
            $datumoj['agoj'] = [];
            $datumoj['aldonilo'] = [
                'titolo'       => 'Aldoni uzanton',
                'eroredaktilo' => 'uzanto-redaktilo'
            ];
            break;

        default:
            return false;
    }

    protokolu($datumoj, 'jen la enhavo:', $protokoloID);

    return $datumoj;
}

function a_spektejo_konstrui_administran_tabelokolumnon_html($kolumnodatumoj, $kolumnoidentigilo = '')
{
    // $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // if (!empty($kolumnoidentigilo)) {
    //     $protokoloID .= '_' . $kolumnoidentigilo;
    // }
    // protokolu(null, 'Saluton, mondo!', $protokoloID);
    // protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);

    if (is_array($kolumnodatumoj)) {
        $kolumno_enhavo   = $kolumnodatumoj['enhavo'] ?? '';
        $kolumnero_titolo = $kolumnodatumoj['titolo'] ?? '';
        $kolumno_url      = $kolumnodatumoj['url']    ?? '';
        $kolumno_klasoj   = $kolumnodatumoj['klasoj'] ?? [];
    } else {
        $kolumno_enhavo   = $kolumnodatumoj;
    }

    if (empty($kolumno_enhavo)) {
        return '<td class="kolumno-' . $kolumnoidentigilo . '"></td>';
    }

    $kolumno_html = '<td class="';
    if (!empty($kolumnoidentigilo)) {
        $kolumno_html .= 'kolumno-' . $kolumnoidentigilo;
    }
    if (!empty($kolumno_klasoj)) {
        $kolumno_html .= ' ' . implode(' ', array_map('esc_attr', $kolumno_klasoj));
    }
    $kolumno_html .= '">';

    if (!empty($kolumno_url)) {
        $kolumno_html .= '<a href="' . esc_url($kolumno_url) . '">';
    }

    if (!empty($kolumnero_titolo)) {
        $kolumno_html .= '<span class="kolumnero-titolo">' . esc_html($kolumnero_titolo) . '</span> ';
    }

    if (is_array($kolumno_enhavo)) {
        $kolumno_html .= '<table>';
        foreach ($kolumno_enhavo as $kolumnero_identigilo => $kolumnero_datumoj) {
            $kolumno_html .= '<tr>';
            $kolumno_html .= a_spektejo_konstrui_administran_tabelokolumnon_html($kolumnero_datumoj, $kolumnero_identigilo);
            $kolumno_html .= '</tr>';
        }
        $kolumno_html .= '</table>';
    } else {
        $kolumno_html .= esc_html($kolumno_enhavo);
    }

    if (!empty($kolumno_url)) {
        $kolumno_html .= '</a>';
    }

    $kolumno_html .= '</td>';

    // protokolu($kolumno_html, 'jen la konstruita html:', $protokoloID);

    return $kolumno_html;
}

function a_spektejo_retrovi_uzantorajtojn()
{
    return [
        'afisxo-aldoni' => [
            'nomo' => 'Aldoni afiŝon aŭ aprobi proponon',
            'postulas' => [],
        ],
        // 'afisxo-forigi' => [
        //     'nomo' => 'Forigi afiŝon',
        //     'postulas' => [],
        // ],
        'afisxo-antauxrigardi' => [
            'nomo' => 'Rigardi neaprobitajn afiŝojn',
            'postulas' => [],
        ],
        'afisxo-marki-rilateco' => [
            'nomo' => 'Atribui rilatecon al afiŝo',
            'postulas' => [],
        ],
        'afisxo-marki-kategorio' => [
            'nomo' => 'Atribui ĝenrojn al afiŝo',
            'postulas' => [],
        ],
        'afisxo-marki-lingvo' => [
            'nomo' => 'Atribui lingvojn al afiŝo',
            'postulas' => [],
        ],
        'afisxo-signalaprobi-problemo' => [
            'nomo' => 'Pritrakti raportojn pri afiŝo',
            'postulas' => ['afisxo-marki-rilateco'],
        ],
        'afisxo-signalaprobi-kategorio' => [
            'nomo' => 'Pritrakti ĝenroproponojn pri afiŝo',
            'postulas' => ['afisxo-marki-kategorio'],
        ],
        // 'afisxo-signalaprobi-lingvo' => [
        //     'nomo' => 'Pritrakti lingvoraportojn pri afiŝo',
        //     'postulas' => ['afisxo-marki-lingvo'],
        // ],
        'kanalo-marki-inkluziveco' => [
            'nomo' => 'Sekvi/malsekvi kanalon',
            'postulas' => [],
        ],
        // 'kanalo-signalaprobi-inkluziveco' => [
        //     'nomo' => 'Pritrakti problemoraportojn',
        //     'postulas' => [],
        // ],
        'komento-marki-maltauxga' => [
            'nomo' => 'Marki komenton kiel maltaŭga',
            'postulas' => [],
        ],
        'komento-signalaprobi-maltauxga' => [
            'nomo' => 'Pritrakti raportojn pri komentoj',
            'postulas' => [],
        ],
        // 'komento-forigi' => [
        //     'nomo' => 'Forigi',
        //     'postulas' => [],
        // ],
    ];
}
