<?php

/**
 * Protokolado!
 *
 * @package Spektejo
 */




function protokolu($variablo = '', $mesagxo = '', $dosieroidentigilo = '')
{
    // 1. Elekti/krei la dosieron
    $sencimigilo = debug_backtrace();
    $alvokinto   = $sencimigilo[1];
    if (isset($alvokinto['function'])) {
        $dosieronomo = $alvokinto['function'];
    } else {
        $dosieronomo = 'gxenerale';
    }
    if (! empty($dosieroidentigilo)) {
        $dosieronomo .= '__' . $dosieroidentigilo;
    }
    $dosieroloko = ABSPATH . '/protokolado/' . $dosieronomo . '.log';
    if (! file_exists($dosieroloko)) {
        file_put_contents($dosieroloko, "Dosiero kreita je " . date('c') . "\n(" . time() . ")\n\n");
    }

    // Konservi la protokolaĵojn
    error_log("\n\n\n***\n\n\n", 3, $dosieroloko);

    error_log("\n\n--------------- KOMENCO ---------------\n\n", 3, $dosieroloko);

    if (! empty($mesagxo)) {
        error_log($mesagxo .  "\n", 3, $dosieroloko);
    }
    if ($variablo) {
        error_log("[" . gettype($variablo) . "]\n", 3, $dosieroloko);
        if (is_string($variablo) || is_numeric($variablo) || is_bool($variablo)) {
            error_log($variablo, 3, $dosieroloko);
        } else {
            error_log(print_r($variablo, true), 3, $dosieroloko);
        }
    }
    error_log("\n\n--------------- FINO ---------------\n\n", 3, $dosieroloko);
}
