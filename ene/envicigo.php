<?php

/**
 * Funkcioj rilataj al envicigi erojn por ripetaj operacioj
 *
 * @package Spektejo
 */


/**
 * Envicigi videoafiŝon por rekalkuli rilatajn afiŝojn
 * a_spektejo_envicigi_videon_por_rekalkuli_rilatajn_afisxojn
 *
 * @param int|array $identigilo aŭ identigiloj
 */
function a_spektejo_envicigi_videon_por_rekalkuli_rilatajn_afisxojn($identigilo)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton, mondo!', $protokoloID);
    // protokolu($identigilo, 'jen la identigilo:', $protokoloID);

    $vico = get_option(PROJEKTNOMO . '_eble_rilataj_rekalkulendaj_videoj', []); // FIXME

    // protokolu($vico, 'jen la jama vico:', $protokoloID);

    if (! $vico) {
        $vico = [];
    }
    if (is_array($identigilo)) {
        $vico = array_unique(
            array_merge(
                $vico,
                $identigilo
            )
        );
    } else {
        $vico[] = $identigilo;
        $vico = array_unique($vico);
    }

    // protokolu($vico, 'jen la ŝanĝita vico:', $protokoloID);

    update_option(PROJEKTNOMO . '_eble_rilataj_rekalkulendaj_videoj', $vico); // FIXME
}
