<?php

/**
 * Funkcioj rilate al signaloj de uzantoj.
 * Tio inkluzivas raporti problemon aŭ proponi kategorion
 *
 * @package Spektejo
 */




/**
 * Aldoni signalon kaj signalinton por diveraj enhaveroj
 * a_spektejo_aldoni_signalon
 *
 * @param string       $erospeco       speco de la signalita ero, ekz. `video`, `kanalo` aŭ `komento`
 * @param int          $eroidentigilo  identigilo de la signalita ero
 * @param string       $signalospeco   speco de la signalo, ekz. `problemo`, `kategorio`, `lingvo`
 * @param string|array $valoro         la valoro de la signalo, ekz. `maltauxga`, `spama`, ktp.
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_aldoni_signalon($erospeco, $eroidentigilo, $signalospeco, $valoro, $kiu)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton!', $protokoloID);
    // protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);

    if (empty($erospeco)
        || empty($eroidentigilo)
        || empty($signalospeco)
        || empty($valoro)
        || empty($kiu)
        || !isset($kiu['uzanto'])
    ) {
        return false;
    }

    $uzanto = $kiu['uzanto'];
    /* $anon   = $kiu['anon']; */

    global $wpdb;
    $tabelo_nomo = $wpdb->prefix . PROJEKTNOMO . '_signaloj';

    /* $aldoni_signalon = $wpdb->insert(
        $tabelo_nomo,
        [
            'speco'    => $signalospeco,
            'valoro'   => $valoro,
            'erospeco' => $erospeco,
            'ero_wpid' => $eroidentigilo,
            'uzanto'   => $uzanto,
            'tempo'    => current_time('mysql'),
        ]
    ); */

    $nuntempo = current_time('mysql');
    $sql_peto = "
        INSERT INTO $tabelo_nomo
            (speco, valoro, erospeco, ero_wpid, uzanto, tempo)
        VALUES
    ";
    $sql_peto_parametroj = [];

    if (is_array($valoro)) {
        $valoro = array_unique($valoro);
        $signalonombro = count($valoro);
        foreach ($valoro as $indekso => $ero) {
            $sql_peto .= "(%s, %s, %s, %d, %s, %s)";
            if ($indekso + 1 < $signalonombro) {
                $sql_peto .= ", ";
            }
            $sql_peto_parametroj = array_merge(
                $sql_peto_parametroj,
                [
                    $signalospeco,
                    $ero,
                    $erospeco,
                    $eroidentigilo,
                    $uzanto,
                    $nuntempo
                ]
            );
        }
        $sql_peto .= ";";
    } else {
        $sql_peto .= "(%s, %s, %s, %d, %s, %s);";
        $sql_peto_parametroj += [
            $signalospeco,
            $valoro,
            $erospeco,
            $eroidentigilo,
            $uzanto,
            $nuntempo
        ];
    }

    // protokolu($sql_peto, 'jen la sql-peto:', $protokoloID);
    // protokolu($sql_peto_parametroj, 'jen la parametraro:', $protokoloID);


    $aldoni_signalon = $wpdb->query(
        $wpdb->prepare($sql_peto, $sql_peto_parametroj)
    );

    // protokolu($aldoni_signalon, 'jen la rezulto de la aldono:', $protokoloID);

    if ($aldoni_signalon === false) {
        return false;
    }



    // Protokoli la eventon
    if ($signalospeco == 'kategorio') {
        a_spektejo_protokoli_eventon('kategoriosignalo', $eroidentigilo);
    }

    return true;
}





/**
 * Aldoni kategorio-signalon kaj signalinton por videoafiŝoj
 * a_spektejo_aldoni_kategoriosignalon
 *
 * @param int          $eroidentigilo  identigilo de la signalita ero
 * @param string|array $valoro         la valoro de la signalo, ekz. `maltauxga`, `spama`, ktp.
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_aldoni_kategoriosignalon($eroidentigilo, $kategorioj, $kiu)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton!', $protokoloID);
    // protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);

    if (empty($eroidentigilo)
        || empty($kiu)
        || !isset($kiu['uzanto'])
    ) {
        return false;
    }

    $v_kategorioterminoj = get_the_terms($eroidentigilo, 'v_kategorioj');
    $v_kategorioj        = is_array($v_kategorioterminoj) ? array_column($v_kategorioterminoj, 'slug') : [];
    $kategoriosignaloj  = [];

    foreach ($kategorioj as $kategorio) {
        if (!in_array($kategorio, $v_kategorioj, true)) {
            $kategoriosignaloj[] = $kategorio;
        }
    }

    if (!empty($kategoriosignaloj)) {
        return a_spektejo_aldoni_signalon('video', $eroidentigilo, 'kategorio', $kategoriosignaloj, ['uzanto' => PROJEKTNOMO]);
    } else {
        return true;
    }
}





/**
 * Forigi signalon kaj signalinton por diversaj enhaveroj
 * a_spektejo_forigi_signalon
 *
 * @param string  $erospeco       speco de la signalita ero, ekz. `video`, `kanalo` aŭ `komento`
 * @param int     $eroidentigilo  identigilo de la signalita ero
 * @param string  $signalo        la signalo, ekz. `maltauxga`, `spama`, ktp.
 * @param string  $uzanto         la identigilo aŭ anonima haketaĵo de la signalinta uzanto
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_forigi_signalon($erospeco, $eroidentigilo, $signalo, $uzanto = '')
{

    // $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton!', $protokoloID);
    // protokolu(func_get_args(), 'la argumentaro estas jene:', $protokoloID);


    if (empty($erospeco)
        || empty($eroidentigilo)
        || empty($signalo)
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }


    global $wpdb;
    $tabelo_nomo = $wpdb->prefix . PROJEKTNOMO . '_signaloj';

    $forigoparametroj = [
        'erospeco' => $erospeco,
        'ero_wpid' => $eroidentigilo,
        'valoro'   => $signalo
    ];

    // protokolu($forigoparametroj, 'jen la forigoparametroj:', $protokoloID);

    if (!empty($uzanto)) {
        $forigoparametroj['uzanto'] = $uzanto;
    }

    $forigi_signalon = $wpdb->delete(
        $tabelo_nomo,
        $forigoparametroj
    );

    // protokolu($forigi_signalon, 'jen la rezulto por forigi la jaman signalon:', $protokoloID);

    if ($forigi_signalon === false) {
        return [
            'mesagxo_malsukcesa' => 'Ne eblas forigi la signalon.',
            'kodo_malsukcesa'    => 'ne_sukcesis_malsignali'
        ];
    }

    return [
        'mesagxo_sukcesa' => 'Sukcese forigis la signalon.',
        'kodo_sukcesa'    => 'sukcese_malsignalis',
        'operacio'        => 'malsignali_eron'
    ];
}






/**
 * Pritrakti signalon de uzanto por enhaveroj
 * a_spektejo_pritrakti_signalon_de_uzanto
 *
 * @param string       $erospeco       speco de la signalita ero, ekz. `video`, `kanalo` aŭ `komento`
 * @param int          $eroidentigilo  identigilo de la signalita ero
 * @param string       $signalospeco   speco de la signalo, ekz. `problemo`, `kategorio`, `lingvo`
 * @param string|array $valoro         la valoro de la signalo, ekz. `maltauxga`, `spama`, ktp.
 *
 * @return array raporto pri la rezulto
 */
function a_spektejo_pritrakti_signalon_de_uzanto($erospeco, $eroidentigilo, $signalospeco, $valoro)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton!', $protokoloID);
    protokolu(func_get_args(), 'jen la erospeco, ero-identigilo, signalospeco kaj la valoro:', $protokoloID);


    // 1. kontroli kaj prepari la datumojn

    if (empty($erospeco)
        || empty($eroidentigilo)
        || $eroidentigilo <= 0
        || empty($signalospeco)
        || empty($valoro)
    ) {
        // malsufiĉe da datumoj!
        return [
            'mesagxo_malsukcesa' => 'Malsufiĉe da datumoj!',
            'kodo_malsukcesa'    => 'datumoj_malsuficxas'
        ];
    }

    $kiu    = a_spektejo_kiu_estas_tiu_cxi(true);
    $uzanto = $kiu['uzanto'];
    $anon   = $kiu['anon'];

    $signaloj_eblaj_cxiuj = [
        'video' => [
            'problemo'  => [
                'spama',
                'nerilata',
                'nedisponebla',
                'neoriginala',
                'gxisnunigenda',
                'esperanta',
                'priesperanta',
            ],
            'kategorio' => array_keys(a_spektejo_retrovi_terminoliston('v_kategorioj')),
            'lingvo'    => array_keys(a_spektejo_retrovi_terminoliston('v_lingvo')),
        ],
        'kanalo' => [
            'problemo' => [
                'spama',
                'nerilata',
                'nedisponebla',
                'gxisnunigenda',
            ]
        ],
        'komento' => [
            'problemo' => [
                'maltauxga'
            ]
        ]
    ];

    $signaloj_jam_markitaj = [
        'video' => [
            'problemo'  => [],
            'kategorio' => array_keys(a_spektejo_retrovi_terminoliston_por_afisxo($eroidentigilo, 'v_kategorioj')),
            'lingvo'    => array_keys(a_spektejo_retrovi_terminoliston_por_afisxo($eroidentigilo, 'v_lingvo')),
        ],
        'kanalo' => [
            'problemo'    => [],
            'inkluziveco' => []
        ],
        'komento' => [
            'problemo' => []
        ],
    ];


    protokolu($signaloj_eblaj_cxiuj, 'jen ĉiuj eblaj signaloj:', $protokoloID);
    protokolu(($signaloj_eblaj_cxiuj[$erospeco][$signalospeco]), 'jen la eblaj signaloj por la erospeco kaj signalospeco:', $protokoloID);
    protokolu($signaloj_jam_markitaj, 'jen ĉiuj jam-aprobitaj signaloj:', $protokoloID);
    protokolu(($signaloj_jam_markitaj[$erospeco][$signalospeco]), 'jen la jam-aprobitaj signaloj por la erospeco kaj signalospeco:', $protokoloID);


    // kontroli validecon de la signalo
    if (!isset($signaloj_eblaj_cxiuj[$erospeco][$signalospeco])
        // sola valoro ne troviĝas inter validaj signaloj
        || (!is_array($valoro) && !in_array($valoro, $signaloj_eblaj_cxiuj[$erospeco][$signalospeco]))
        // unu el la valoroj ne troviĝas inter validaj signaloj
        || (is_array($valoro) && !empty(array_diff($valoro, $signaloj_eblaj_cxiuj[$erospeco][$signalospeco])))
    ) {
        return [
            'mesagxo_malsukcesa' => 'Tiu signalo ne estas valida. Reprovu kun valida signalo.',
            'kodo_malsukcesa'    => 'nekonata_signalo'
        ];
    }

    if (is_array($valoro)) {
        $signaloj_jam_markitaj_por_erospeco_kaj_signalospeco = $signaloj_jam_markitaj[$erospeco][$signalospeco];
        $valoro = array_filter(
            $valoro,
            function ($valorero) use ($signaloj_jam_markitaj_por_erospeco_kaj_signalospeco) {
                return !in_array($valorero, $signaloj_jam_markitaj_por_erospeco_kaj_signalospeco, true);
            }
        );
        if (empty($valoro)) {
            return [
                'mesagxo_malsukcesa' => 'Tiu signalo estas jam aprobita.',
                'kodo_malsukcesa'    => 'jam_aprobita_signalo'
            ];
        }
    } else {
        if (in_array($valoro, $signaloj_jam_markitaj[$erospeco][$signalospeco], true)) {
            return [
                'mesagxo_malsukcesa' => 'Tiu signalo estas jam aprobita.',
                'kodo_malsukcesa'    => 'jam_aprobita_signalo'
            ];
        }
    }

    $signaloj_jamaj = a_spektejo_retrovi_signalojn_por_ero($erospeco, $eroidentigilo, $uzanto);

    // protokolu($signaloj_jamaj, 'jen la jamaj signaloj de la uzanto:', $protokoloID);

    if (!is_array($signaloj_jamaj)) { // ĉu tio estas la ĝusta kondiĉo?
        return [
            'mesagxo_malsukcesa' => 'Ne eblas sendi la signalon. Provu poste.',
            'kodo_malsukcesa'    => 'ne_sukcesis_scii_jaman_signalon'
        ];
    } elseif (!is_array($valoro)  // por unuopaj valoroj, ekz. problemoraportoj
        && isset($signaloj_jamaj[$signalospeco][$valoro])
    ) {
        return [
            'mesagxo_malsukcesa' => 'Ŝajnas, ke vi jam sendis la saman signalon.',
            'kodo_malsukcesa'    => 'uzanto_jam_signalis_samvalore'
        ];
    } elseif (is_array($valoro)  // por oblaj valoroj, ekz. kategorioj
        && isset($signaloj_jamaj[$signalospeco])
    ) {
        return [
            'mesagxo_malsukcesa' => 'Ŝajnas, ke vi jam signalis pri tio.',
            'kodo_malsukcesa'    => 'uzanto_jam_signalis_samspece'
        ];
    }



    // 2. aldoni signalon laŭ la peto kaj forigi malnovan signalon de la uzanto, se iu ekzistas

    // forigi jaman signalon de la sama signalospeco
    if (!is_array($valoro)
        && !empty($signaloj_jamaj[$signalospeco])
    ) { // FIXME: Ĉu tio estas la ĝusta kondiĉo?
        protokolu(null, 'forigas la jaman signalon...', $protokoloID);

        $signalo_samspeca_jama = array_keys($signaloj_jamaj[$signalospeco])[0];

        $forigi_signalon = a_spektejo_forigi_signalon($erospeco, $eroidentigilo, $signalo_samspeca_jama, $uzanto);
        if ($forigi_signalon === false) {
            return [
                'mesagxo_malsukcesa' => 'Ne eblas sendi la signalon. Provu poste.',
                'kodo_malsukcesa'    => 'ne_sukcesis_forigi_jaman_signalon'
            ];
        }
    }
    // TODO: Ankaŭ pritrakti la multvalorajn signalojn


    // aldoni la novan signalon

    protokolu(null, 'aldonas la novan signalon...', $protokoloID);

    $aldoni_signalon = a_spektejo_aldoni_signalon($erospeco, $eroidentigilo, $signalospeco, $valoro, $kiu);
    if ($aldoni_signalon === false) {
        return [
            'mesagxo_malsukcesa' => 'Ne eblas sendi la signalon. Provu poste.',
            'kodo_malsukcesa'    => 'ne_sukcesis_aldoni_signalon'
        ];
    }

    return [
        'mesagxo_sukcesa' => 'Sukcese sendis la signalon al la administrantoj. Dankon pro via helpo :-)',
        'kodo_sukcesa'    => 'sukcese_signalis',
        'operacio'        => 'signali_eron'
    ];
}




/**
 * Retrovi la jamajn signalojn senditajn por enhavero (videoafiŝo, kanalo, komento, ktp)
 * a_spektejo_retrovi_signalojn_por_ero
 *
 * @param string  $erospeco       speco de la ero, ekz. `video`, `kanalo` aŭ `komento`
 * @param int     $eroidentigilo  la identigilo de la ero (afiŝo, termino, komento)
 * @param string  $uzanto         la identigilo aŭ anonima haketaĵo de uzanto
 *
 * @return array aranĝaĵo enhavante erojn kiel `int $indekso => int $nombro`
 */
function a_spektejo_retrovi_signalojn_por_ero($erospeco, $eroidentigilo, $uzanto = '')
{
    // $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton!', $protokoloID);
    // protokolu([$erospeco, $eroidentigilo, $uzanto], 'jen la erospeco, eroidentigilo kaj la uzanto:', $protokoloID);

    if (empty($erospeco)
        || empty($eroidentigilo)
        || $eroidentigilo <= 0
    ) {
        return false;
    }

    global $wpdb;
    $tabelo_nomo = $wpdb->prefix . PROJEKTNOMO . '_signaloj';

    $sql_peto = "SELECT * FROM $tabelo_nomo WHERE ";
    $sql_peto_parametroj = [];

    if (!empty($uzanto)) {
        $sql_peto .= " uzanto = %s AND ";
        $sql_peto_parametroj[] = $uzanto;
    }

    $sql_peto .= " erospeco = %s AND ";
    $sql_peto_parametroj[] = $erospeco;

    $sql_peto .= " ero_wpid = %d ";
    $sql_peto_parametroj[] = $eroidentigilo;

    // protokolu($sql_peto, 'jen la sql-peto:', $protokoloID);
    // protokolu($sql_peto_parametroj, 'jen la parametraro por la peto:', $protokoloID);

    $signaloj = $wpdb->get_results(
        $wpdb->prepare($sql_peto, $sql_peto_parametroj),
        'ARRAY_A'
    );

    // protokolu($signaloj, 'jen la petorezulto:', $protokoloID);

    if (!empty($signaloj)) {
        $signaloj_lauxspece = [];

        foreach ($signaloj as $signalo) {
            $speco = $signalo['speco'];
            $signaloj_lauxspece[$speco][] = $signalo['valoro'];
        }

        // protokolu($signaloj_lauxspece, 'jen la signaloj por la ero, laŭspece:', $protokoloID);

        $signalonombroj_lauxspece = array_map('array_count_values', $signaloj_lauxspece);
    } else {
        $signalonombroj_lauxspece = [];
    }

    // protokolu($signalonombroj_lauxspece, 'jen la nombroj de signaloj:', $protokoloID);

    return $signalonombroj_lauxspece;
}





/**
 * Retrovi enhaverojn (ekz. videoafiŝo, kanalo, aŭ komento), kiuj havas signalon (ekz. problemoraportoj)
 * a_spektejo_retrovi_signalitajn_erojn
 *
 * @param string  $erospeco       speco de la ero, ekz. `video`, `kanalo` aŭ `komento`
 * @param string  $signalospecoj  la speco de la signalo (ekz. `problemo`, `kategorio`, `lingvo`)
 *
 * @return array aranĝaĵo enhavante la vortpresajn identigilojn por la eroj (ekz. post-id, term-id aŭ comment-id depende de la ero-speco)
 */
function a_spektejo_retrovi_signalitajn_erojn($erospeco, $signalospeco)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton!', $protokoloID);

    if (empty($erospeco) || empty($signalospeco)) {
        return false;
    }

    global $wpdb;
    $tabelo_nomo = $wpdb->prefix . PROJEKTNOMO . '_signaloj';

    $sql_peto = "SELECT ero_wpid ";
    $sql_peto .= " FROM $tabelo_nomo WHERE ";
    $sql_peto .= " erospeco = %s AND ";
    $sql_peto .= " speco = %s ";
    $sql_peto .= " GROUP BY ero_wpid ";
    $sql_peto_parametroj = [$erospeco, $signalospeco];

    protokolu($sql_peto, 'jen la sql-peto:', $protokoloID);
    protokolu($sql_peto_parametroj, 'jen la parametraro por la peto:', $protokoloID);

    $signalitaj_eroj = $wpdb->get_results(
        $wpdb->prepare($sql_peto, $sql_peto_parametroj),
        'ARRAY_A'
    );

    protokolu($signalitaj_eroj, 'jen la signalitaj eroj:', $protokoloID);

    return array_column($signalitaj_eroj, 'ero_wpid');
}
