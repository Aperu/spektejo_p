<?php

/**
 * Renovigisto: Renovigas datumojn se necese
 *
 * @package Spektejo
 */

/**
 * Ĝisnunigi la liston de sekvataj kanaloj
 */
add_action('ago_renovigado', 'a_spektejo_renovigisto_renovigi');
function a_spektejo_renovigisto_renovigi()
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton, mondo!', $protokoloID);

    // $komencotempo = time();

    // 1. legi la kolektitajn novajn datumojn

    $dos_novaj_datumoj         = ABSPATH . 'datumoj/renovigado/novaj_datumoj.json';
    $datumoj_renovigendaj_json = file_get_contents($dos_novaj_datumoj);
    $datumoj_renovigendaj      = json_decode($datumoj_renovigendaj_json, true);
    if (! $datumoj_renovigendaj) {
        protokolu(null, "Ne eblas legi datumojn el la dosiero, aŭ la dosiero malplenas!", $protokoloID);
        return false;
    }
    $videoj_renovigendaj_identigiloj = array_keys($datumoj_renovigendaj);


    // 2. kompari datumojn kun afiŝoj, kaj renovigi afiŝojn laŭnecese

    $nombro = 100;
    $videoj_renovigendaj_identigiloj_parte = array_slice($videoj_renovigendaj_identigiloj, 0, $nombro);

    $videoafisxoj_renovigendaj = a_spektejo_trovi_videoafisxojn_laux_fonta_identigilo($videoj_renovigendaj_identigiloj_parte, ($nombro * 2), false);

    protokolu(count($videoafisxoj_renovigendaj), 'la nombro de pritraktotaj renovigendaj afiŝoj:', $protokoloID);

    $renovigoraporto_videoj = [];
    $renovigoraporto        = [
        'titolo'    => [],
        'priskribo' => [],
        'etikedoj'  => [],
        'auxtoro'   => []
    ];

    // Malaktivigi la limojn por la titoloj por ne ŝanĝi majusklojn en nometoj de afiŝoj kaj kanaloj
    remove_filter('sanitize_title', 'sanitize_title_with_dashes');
    remove_filter('sanitize_title', 'a_spektejo_ebligi_majusklon_punktojn_streketojn_en_titoloj');

    foreach ($videoafisxoj_renovigendaj as $videoafisxo) {
        protokolu(null, 'pritraktas la afiŝon #' . $videoafisxo->ID . '...', $protokoloID);

        // arigi la jamajn datumojn por la afiŝo
        $v_afisxID    = $videoafisxo->ID;
        $v_nometo     = $videoafisxo->post_name;
        $v_identigilo = get_post_meta($v_afisxID, 'v_identigilo_fonto', true); // la identigilo ĉe la fonto, ekz. la jutuba 11-karaktra videoID

        if (! isset($datumoj_renovigendaj[$v_identigilo])) {
            continue;
        }

        $v_renovigo_raporto = [];

        $v_titolo         = trim(wp_specialchars_decode($videoafisxo->post_title));
        $v_priskribo      = trim(wp_specialchars_decode(wp_strip_all_tags($videoafisxo->post_content)));
        $v_kanalotermino  = get_the_terms($v_afisxID, 'v_kanalo')[0];
        $v_auxtoro        = wp_specialchars_decode($v_kanalotermino->name);
        $v_etikedoj_krude = get_post_meta($v_afisxID, 'v_etikedoj_krude', true);
        $v_etikedoj       = empty(trim($v_etikedoj_krude)) ? [] : explode(',', $v_etikedoj_krude);

        // FIXME: La valoro de la metakampo `v_etikedoj_krude` devas esti ŝnuro ĉiam
        if (is_array($v_etikedoj_krude)) {
            $v_etikedoj = $v_etikedoj_krude;
        }

        // arigi la novajn datumojn por la afiŝo
        $v_datumoj_nova       = $datumoj_renovigendaj[$v_identigilo];
        $v_titolo_nova        = trim($v_datumoj_nova['titolo']);
        $v_priskribo_nova     = trim($v_datumoj_nova['priskribo']);
        $v_etikedoj_nova      = $v_datumoj_nova['etikedoj'];
        $v_auxtoro_nova       = trim($v_datumoj_nova['auxtoro']);
        $v_kanaloID_nova      = $v_datumoj_nova['kanalo'];
        $v_fonto_nova         = $v_datumoj_nova['fonto'];
        $v_renovigotempo_nova = $v_datumoj_nova['renovigotempo'];

        switch ($v_fonto_nova) {
            case 'youtube':
                $v_fonto_kodo = 'ytb';
                break;

            default:
                $v_fonto_kodo = '';
                break;
        }

        // kompari la titolon
        // protokolu($v_titolo, 'jen la malnova titolo por la afiŝo #' . $v_afisxID . '...', $protokoloID);
        // protokolu($v_titolo_nova, 'jen la nova titolo por la afiŝo #' . $v_afisxID . '...', $protokoloID);

        if ($v_titolo !== $v_titolo_nova) {
            $titolo_sxangxite = true;

            $v_renovigo_raporto['titolo'] = [
                'malnova' => $v_titolo,
                'nova'    => $v_titolo_nova
            ];
            $renovigoraporto['titolo'][] = $v_identigilo;
        } else {
            $titolo_sxangxite = false;
        }

        // kompari la priskribon
        // protokolu($v_priskribo, 'jen la malnova priskribo por la afiŝo #' . $v_afisxID . '...', $protokoloID);
        // protokolu($v_priskribo_nova, 'jen la nova priskribo por la afiŝo #' . $v_afisxID . '...', $protokoloID);

        if ($v_priskribo !== $v_priskribo_nova) {
            $priskribo_sxangxite = true;

            $v_renovigo_raporto['priskribo'] = [
                'malnova' => $v_priskribo,
                'nova'    => $v_priskribo_nova
            ];
            $renovigoraporto['priskribo'][] = $v_identigilo;
        } else {
            $priskribo_sxangxite = false;
        }

        // renovigi la titolon kaj/aŭ la priskribon, laŭnecese
        if ($titolo_sxangxite === true || $priskribo_sxangxite === true) {
            $afisxodatumoj = [
                'ID'        => $v_afisxID,
                'post_name' => $v_nometo
            ];

            if ($titolo_sxangxite === true) {
                $afisxodatumoj['post_title'] = wp_strip_all_tags($v_titolo_nova);
            }

            if ($priskribo_sxangxite === true) {
                $afisxodatumoj['post_content'] = wp_strip_all_tags($v_priskribo_nova);
            }

            // protokolu(null, 'ŜANĜO de titolo/priskribo ĉe #' . $v_afisxID, $protokoloID);
            protokolu(null, 'konservas la afiŝon #' . $v_afisxID . 'kun la nova titolo/priskribo...', $protokoloID);

            wp_update_post($afisxodatumoj);
        }

        // kompari la etikedojn
        $v_etikedoj = array_map('strtolower', $v_etikedoj);
        $v_etikedoj = array_map('html_entity_decode', $v_etikedoj);
        $v_etikedoj = array_unique($v_etikedoj);
        $v_etikedoj_nova = array_map('strtolower', $v_etikedoj_nova);
        $v_etikedoj_nova = array_unique($v_etikedoj_nova);
        sort($v_etikedoj);
        sort($v_etikedoj_nova);

        if (((!empty($v_etikedoj) || !empty($v_etikedoj_nova))
            && ($v_etikedoj != $v_etikedoj_nova))
            || $titolo_sxangxite === true
        ) {
            $etikedoj_sxangxite = true;

            // protokolu(null, 'ŜANĜO de etikedoj ĉe #' . $v_afisxID, $protokoloID);
            protokolu(null, 'ĝisdatigas la etikedojn...', $protokoloID);

            $v_etikedoj_preparitaj_nova = a_spektejo_prepari_etikedaron(array_merge($v_etikedoj_nova, [$v_titolo_nova]));

            // renovigi la etikedojn
            update_post_meta($v_afisxID, 'v_etikedoj_krude', implode(',', $v_etikedoj_nova));
            wp_set_object_terms($v_afisxID, $v_etikedoj_preparitaj_nova, 'v_etikedoj', false);

            // re-proponi kategoriojn laŭ etikedoj
            $v_kategorioj_laux_etikedoj = a_spektejo_proponi_kategoriojn_laux_etikedoj($v_etikedoj_preparitaj_nova);

            if (! empty($v_kategorioj_laux_etikedoj)) {
                protokolu(null, 'proponas novajn kategoriojn laŭ la novaj etikedoj...', $protokoloID);

                a_spektejo_aldoni_kategoriosignalon($v_afisxID, $v_kategorioj_laux_etikedoj, ['uzanto' => PROJEKTNOMO]);
            }

            $v_renovigo_raporto['etikedoj'] = [
                'malnova' => $v_etikedoj,
                'nova'    => $v_etikedoj_nova,
            ];
            if ($titolo_sxangxite === true) {
                $v_renovigo_raporto['etikedoj']['titolo_malnova'] = $v_titolo;
                $v_renovigo_raporto['etikedoj']['titolo_nova'] = $v_titolo_nova;
            }
            $renovigoraporto['etikedoj'][] = $v_identigilo;
        } else {
            $etikedoj_sxangxite = false;
        }


        /* kompari la kanalonomon */
        if ($v_auxtoro !== $v_auxtoro_nova) {
            // renovigi la kanalonomon
            $kanalotermino_ID     = $v_kanalotermino->term_id;
            // $kanalotermino_nometo = $v_kanalotermino->slug;
            $kanalotermino_nometo = $v_fonto_kodo . '_' . $v_kanaloID_nova;

            // protokolu(null, 'ŜANĜO de kanalonomo ĉe #' . $v_afisxID, $protokoloID);
            protokolu(null, "rekonservas la kanaloterminon #" . $kanalotermino_ID . " kun la nometo " . $kanalotermino_nometo . "...\n", $protokoloID);

            wp_update_term(
                $kanalotermino_ID,
                'v_kanalo',
                [
                    'name' => $v_auxtoro_nova,
                    'slug' => $kanalotermino_nometo
                ]
            );
            update_term_meta($kanalotermino_ID, 'v_kanalo_identigilo_fonto', $v_kanaloID_nova);

            $v_renovigo_raporto['auxtoro'] = [
                'malnova' => $v_auxtoro,
                'nova'    => $v_auxtoro_nova
            ];
            $renovigoraporto['auxtoro'][] = $v_identigilo;
        }

        if (! empty($v_renovigo_raporto)) {
            $renovigoraporto_videoj[$v_identigilo] = $v_renovigo_raporto;
        }

        // konservi la renovigotempon
        // $nuntempo = time();
        // update_post_meta($v_afisxID, 'v_laste_renovigita', $nuntempo);
        $rezulto_konservi_renovigodaton = update_post_meta($v_afisxID, 'v_laste_renovigita', $v_renovigotempo_nova);

        protokolu($rezulto_konservi_renovigodaton, 'jen la rezulto por konservi la renovigotempon ' . $v_renovigotempo_nova . ' por la afiŝo #' . $v_afisxID . ':', $protokoloID);

        // forigi la eron el la datumaro
        unset($datumoj_renovigendaj[$v_identigilo]);

        // forigi markon pri renovigendeco
        a_spektejo_malmarki_videon($v_afisxID, 'gxisnunigenda', 'atribuo');

        // envicigi la videon por poste rekalkuli ĝiajn rilatajn afiŝojn
        if ($etikedoj_sxangxite === true) {
            // protokolu(null, 'envicigas la afiŝon por poste kalkuli rilatajn afiŝojn...', $protokoloID);
            a_spektejo_envicigi_videon_por_rekalkuli_rilatajn_afisxojn($v_afisxID);
        }

        // envicigi la rilatajn afiŝojn por poste rekalkuli iliajn rilatajn afiŝojn por ili
        // a_spektejo_envicigi_videon_por_rekalkuli_rilatajn_afisxojn($videoj_rilataj); // FIXME: Ĉu necesas?
    }

    protokolu($renovigoraporto, "jen ĝenerala renovigoraporto por " . date('ymd-His')  . ":\n", $protokoloID . '_gxenerale');
    protokolu($renovigoraporto_videoj, "jen detala renovigoraporto por " . date('ymd-His')  . ":\n", $protokoloID . '_detale');


    // 3. rekonservi la nekontrolitajn datumojn
    $datumoj_nekontrolitaj_json = json_encode($datumoj_renovigendaj, JSON_PRETTY_PRINT);
    file_put_contents($dos_novaj_datumoj, $datumoj_nekontrolitaj_json);

    // $dauxro = time() - $komencotempo;

    protokolu(null, "Finite!", $protokoloID);
}


// FIXME: GRAVE movu la funkcion en la taŭgan dosieron (ĉu `enhaveroj.php`?)
function a_spektejo_trovi_videoafisxojn_laux_fonta_identigilo($fontaj_identigiloj, $limo = -1, $nur_identigiloj = false)
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton, mondo!', $protokoloID);
    // protokolu(func_get_args(), 'jen la argumentaro:', $protokoloID);

    $arg_meta  = [];
    foreach ($fontaj_identigiloj as $fonta_identigilo) {
        $arg_meta[] = [
            'key'   => 'v_identigilo_fonto',
            'value' => $fonta_identigilo,
        ];
    }
    if (count($arg_meta) > 1) {
        $arg_meta['relation'] = 'OR';
    }

    $arg = [
        'post_type'   => PROJEKTNOMO . '_video',
        'meta_query'  => $arg_meta,
        'numberposts' => $limo,
    ];
    // redoni nur afiŝo-identigilojn, anstataŭ tuatajn afiŝo-objektojn (WP_Post)
    if ($nur_identigiloj === true) {
        $arg['fields'] = 'ids';
    }

    // protokolu($arg, 'jen la argumentaro por get_posts()', $protokoloID);

    $videoafisxoj = get_posts($arg);

    // protokolu($videoafisxoj, 'jen la trovitaj eroj:', $protokoloID);

    return $videoafisxoj;
}


/**
 * Fari kaj konservi liston de renovigendaj afiŝoj
 */
add_action('ago_datumoricevado_por_renovigado', 'a_spektejo_renovigisto_ricevi_datumojn');
function a_spektejo_renovigisto_ricevi_datumojn()
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO

    // 1. listigi renovigendajn afiŝojn

    // legi la antaŭe-konservitajn datumojn el dosiero kaj listigi la erojn
    $dos_novaj_datumoj                 = ABSPATH . 'datumoj/renovigado/novaj_datumoj.json';
    $afisxoj_renovigendaj_antauxe_json = file_get_contents($dos_novaj_datumoj);
    $afisxoj_renovigendaj_antauxe      = json_decode($afisxoj_renovigendaj_antauxe_json, true);
    if (! $afisxoj_renovigendaj_antauxe) {
        $afisxoj_renovigendaj_antauxe = [];
    }

    $afisxoj_renovigendaj_antauxe_afisxoID = a_spektejo_trovi_videoafisxojn_laux_fonta_identigilo(array_keys($afisxoj_renovigendaj_antauxe), 500, true);

    // protokolu($afisxoj_renovigendaj_antauxe_afisxoID, 'jen la afiŝoidentigiloj de ekskludotaj eroj, ĉar ili estis antaŭe kolektitaj:', $protokoloID);

    $nombro   = 50; // laŭ la limo de la jutuba API // FIXME: Konsideru limojn por aliaj platformoj
    $nuntempo = time();

    // 1.1. listigi la afiŝojn markitaj kiel renovigendaj

    $arg_markitaj = [
        'post_type'    => PROJEKTNOMO . '_video',
        'numberposts'  => $nombro,
        'post__not_in' => $afisxoj_renovigendaj_antauxe_afisxoID, // por ne denove peti datumojn pri antaŭe-faritaj afiŝoj
        'fields'       => 'ids',
        'tax_query'    => [
            [
                'taxonomy' => 'v_atribuoj',
                'field'    => 'slug',
                'terms'    => ['gxisnunigenda'],
            ]
        ],
    ];
    $afisxoj_markitaj_identigiloj = get_posts($arg_markitaj);
    $afisxoj_markitaj_nombro = count($afisxoj_markitaj_identigiloj);

    protokolu($afisxoj_markitaj_identigiloj, "La afiŝoidentigiloj de la markitaj videoj estas jene:", $protokoloID);


    // 1.2. listigi afiŝojn, kiuj ne estas renovigitaj lastatempe

    $arg_malnovaj = [
        'post_type'    => PROJEKTNOMO . '_video',
        'numberposts'  => max(1, ($nombro - $afisxoj_markitaj_nombro)),
        'post__not_in' => $afisxoj_renovigendaj_antauxe_afisxoID, // por ne denove peti datumojn pri antaŭe-faritaj afiŝoj
        'fields'       => 'ids',
        'orderby'      => 'meta_value_num',
        'meta_key'     => 'v_laste_renovigita',
        'order'        => 'ASC',
        'tax_query'    => [
            [
                'taxonomy'  => 'v_rilateco',
                'field'     => 'slug',
                'terms'     => ['esperanta', 'priesperanta', 'nedifina'],
            ]
        ],
        'meta_query'  => [
            [
                'key'     => 'v_laste_renovigita',
                'value'   => $nuntempo - 2500000, // proksimume unu monato
                'compare' => '<=',
                'type'    => 'NUMERIC'
            ],
        ]
    ];
    $afisxoj_malnovaj_identigiloj = get_posts($arg_malnovaj);

    protokolu($afisxoj_malnovaj_identigiloj, "La afiŝoidentigiloj de la malnovaj videoj estas jene:", $protokoloID);

    $afisxoj_renovigendaj_identigiloj = array_unique(array_merge($afisxoj_malnovaj_identigiloj, $afisxoj_markitaj_identigiloj));

    if (empty($afisxoj_renovigendaj_identigiloj)) {
        protokolu(null, "ERARO: Ne trovis renovigendajn afiŝojn!", $protokoloID);
        return false;
    }

    $afisxoj_nombro = count($afisxoj_renovigendaj_identigiloj);
    if ($afisxoj_nombro >= 50) {
        $afisxoj_renovigendaj_identigiloj = array_slice($afisxoj_renovigendaj_identigiloj, 0, 50);
    }

    protokolu(null, $afisxoj_nombro . " renovigendaj afiŝoj troviĝis.", $protokoloID);
    protokolu($afisxoj_renovigendaj_identigiloj, "La (unuaj 50) afiŝoidentigiloj de la renovigendaj videoj estas jene:", $protokoloID);

    $afisxoj_renovigendaj_videoID = [];
    foreach ($afisxoj_renovigendaj_identigiloj as $video_afisxID) {
        $v_identigilo = get_post_meta($video_afisxID, 'v_identigilo_fonto', true);
        if (!empty($v_identigilo)) {
            $afisxoj_renovigendaj_videoID[$v_identigilo] = $video_afisxID;
            // FIXME: PORTEMPE: konservu kontrolan tempon por la afiŝo (portempe, ĉar la konservado estas farita en la renoviga funkcio)
            // update_post_meta($video_afisxID, 'v_laste_renovigita', $nuntempo);
        }
    }


    // 2. peti kaj kolekti datumojn
    $peto_parametroj = [
        'identigilaro' => array_keys($afisxoj_renovigendaj_videoID),
        'partoj'       => ['snippet']
    ];

    $peto_adreso = a_spektejo_konstrui_api_adreson_por_videodatumoj_ytb($peto_parametroj);
    if (! $peto_adreso) {
        return false;
    }

    $peto_datumoj = a_spektejo_elsxuti_json($peto_adreso);
    if (! isset($peto_datumoj['items']) || ! is_array($peto_datumoj['items'])) {
        protokolu(null, "ERARO: Ricevitaj datumoj ne validas!", $protokoloID);
        return false;
    }

    // 3. pritrakti ĉiun datumeron
    $api_datumoj_eroj                = $peto_datumoj['items'];
    $afisxoj_renovigendaj            = [];
    $afisxoj_disponeblaj_identigiloj = [];

    foreach ($api_datumoj_eroj as $indekso => $ero) {
        if (!isset($ero['id']) || !isset($ero['snippet'])) {
            // unset($api_datumoj_eroj[$indekso]); // FIXME: Ĉu ne necesas?
            continue;
        }

        $afisxo_renovigenda = [];

        $ero_id      = $ero['id'];
        $ero_peceto  = $ero['snippet'];
        //$ero_detaloj = $ero['contentDetails'];
        $video_titolo    = $ero_peceto['title'];
        $video_priskribo = $ero_peceto['description'];
        $video_auxtoro   = $ero_peceto['channelTitle'];
        $video_kanalo    = $ero_peceto['channelId'];
        $video_etikedoj  = isset($ero_peceto['tags']) ? $ero_peceto['tags']   : [];
        //$video_enkorpigebleco = '';

        $afisxo_renovigenda['titolo']     = $video_titolo;
        $afisxo_renovigenda['priskribo']  = $video_priskribo;
        $afisxo_renovigenda['auxtoro']    = $video_auxtoro;
        $afisxo_renovigenda['kanalo']     = $video_kanalo;
        $afisxo_renovigenda['etikedoj']   = $video_etikedoj;
        //$afisxo_renovigenda['enkorpigebleco'] =  $video_enkorpigebleco;
        $afisxo_renovigenda['fonto'] = 'youtube'; // FIXME
        $afisxo_renovigenda['renovigotempo'] = $nuntempo;

        $afisxoj_renovigendaj[$ero_id]     = $afisxo_renovigenda;
        $afisxoj_disponeblaj_identigiloj[] = $afisxoj_renovigendaj_videoID[$ero_id];
    }

    // 4. konservi la datumojn en dosieron

    // kombini la antaŭajn kaj novajn datumojn
    $afisxoj_renovigendaj_cxiuj      = array_merge($afisxoj_renovigendaj_antauxe, $afisxoj_renovigendaj);
    $afisxoj_renovigendaj_cxiuj_json = json_encode($afisxoj_renovigendaj_cxiuj, JSON_PRETTY_PRINT);

    // konservi en dosieron
    file_put_contents($dos_novaj_datumoj, $afisxoj_renovigendaj_cxiuj_json);


    // 5. forigi troajn signalojn

    protokolu(null, "La disponeblaj video-afiŝoj estas jene:\n<---\n" . print_r($afisxoj_disponeblaj_identigiloj, true) . "\n--->\n\n", $protokoloID);

    // forigi raportojn pri nedisponebleco
    if (! empty($afisxoj_disponeblaj_identigiloj) && is_array($afisxoj_disponeblaj_identigiloj)) {
        foreach ($afisxoj_disponeblaj_identigiloj as $video_disponebla_ID) {
            // protokolu(null, "\tforigas signalojn pri nedisponebleco por la afiŝo " . $video_disponebla_ID . "...\n", $protokoloID);

            a_spektejo_forigi_signalon('video', $video_disponebla_ID, 'nedisponebla');
        }
    }

    // 6. signali la nedisponeblajn afiŝojn

    // listigi la videojn kiuj ne ekzistas ĉe la platformo
    $afisxoj_nedisponeblaj_identigiloj = array_diff(array_values($afisxoj_renovigendaj_videoID), $afisxoj_disponeblaj_identigiloj);

    protokolu($afisxoj_nedisponeblaj_identigiloj, 'jen la identigiloj de ŝajne-nedisponeblaj afiŝoj:', $protokoloID);

    if (! empty($afisxoj_nedisponeblaj_identigiloj)) {
        foreach ($afisxoj_nedisponeblaj_identigiloj as $v_afisxID) {
            // FIXME: Devos ne aldoni signalon se tia signalo jam ekzistas!
            a_spektejo_aldoni_signalon('video', $v_afisxID, 'problemo', 'nedisponebla', ['uzanto' => PROJEKTNOMO]);
        }
    }

    protokolu(null, "Finite!", $protokoloID);
}
