<?php

/**
 * Poentigisto: Kontrolas statistikojn pri afiŝoj kaj poentigas ilin laŭe.
 *
 * @package Spektejo
 */

/**
 * Aldoni envicigitajn poentojn por videoj, kiuj estas envicigitaj por repoentigo
 */
// add_action('ago_poentigado_por_videoj', 'a_spektejo_poentigisto_repoentigi');
function a_spektejo_poentigisto_repoentigi()
{

    // 1. pretigi liston de videoj kies poentojn endas rekalkuli

    // legi la vicon el la datumbazo
    $videoj_pritraktendaj = get_option(PROJEKTNOMO . '_repoentigendaj_videoj', []);

    // malplenigi la vicon en la datumbazo
    update_option(PROJEKTNOMO . '_repoentigendaj_videoj', []);



    // 2. rekalkuli la poentojn por ĉiu video

    if (is_array($videoj_pritraktendaj) && ! empty($videoj_pritraktendaj)) {
        $videoj_pritraktendaj  = array_unique($videoj_pritraktendaj);

        foreach ($videoj_pritraktendaj as $identigilo) {
            $identigilo    = intval($identigilo);

            // rekalkuli kaj atribui poentojn al la video
            $populareco = a_spektejo_kalkuli_popularecon($identigilo);

            update_post_meta($identigilo, 'v_populareco', strval($populareco));
        }
    }
}
