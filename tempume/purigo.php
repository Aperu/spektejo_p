<?php

/**
 * Purigisto: Purigas nenecesaĵojn de tempo al tempo
 *
 * @package Spektejo
 */

/**
 * // TODO
 */
add_action('ago_purigado', 'a_spektejo_purigisto_prizorgi_nedisponeblajn_videojn');
function a_spektejo_purigisto_prizorgi_nedisponeblajn_videojn()
{
    // a. listigi raportitajn videojn
    $arg = [
        'post_type'   => PROJEKTNOMO . '_video',
        'post_status' => 'publish',
        'meta_query'  => [
            [
                'key'    => 'v_raporto',
                'value'  => 'nedisponebla',
            ],
        ],
        'tax_query'   => [
            [
                'taxonomy' => 'v_rilateco',
                'field'    => 'slug',
                'terms'    => ['esperanta', 'priesperanta', 'nedifina'],
            ]
        ],
        'orderby'     => 'date',
        'order'       => 'DESC',
        'numberposts' => 25,
        'fields'      => 'ids',
    ];

    $videoj_raportitaj  = get_posts($arg);



    // b. listigi videojn de la lastaj 48 horoj
    $arg = [
        'post_type'    => PROJEKTNOMO . '_video',
        'post_status'  => 'publish',
        'date_query'  => [
            [
                'after'    => '48 hours ago',
            ],
        ],
        'tax_query'    => [
            [
                'taxonomy'  => 'v_rilateco',
                'field'    => 'slug',
                'terms'    => ['esperanta', 'priesperanta', 'nedifina'],
            ]
        ],
        'orderby'    => 'date',
        'order'      => 'DESC',
        'numberposts'  => 25,
        'fields'    => 'ids',
    ];

    $videoj_lastatempaj  = get_posts($arg);



    $videoj_kontrolendaj  = array_merge($videoj_raportitaj, $videoj_lastatempaj);

    a_spektejo_kontroli_disponeblecon_ytb($videoj_kontrolendaj);
}
