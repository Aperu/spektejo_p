<?php

/**
 * Rekalkulisto: Rekalkulas diversajn datumarojn laŭ daŭraj ŝanĝoj en la sistemo
 *
 * @package Spektejo
 */

/**
 * Rekalkuli kaj konservi eble-rilatajn afiŝojn
 */
add_action('ago_rekalkulado_de_eble_rilataj_afisxoj', 'a_spektejo_rekalkuli_eble_rilatajn_afisxojn');
function a_spektejo_rekalkuli_eble_rilatajn_afisxojn()
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton, mondo!', $protokoloID);

    $eble_rilataj_rekalkulendaj_afisxoj = get_option(PROJEKTNOMO . '_eble_rilataj_rekalkulendaj_videoj', []);
    if (empty($eble_rilataj_rekalkulendaj_afisxoj)) {
        // protokolu(null, 'nenio por rekalkuli!', $protokoloID);

        return;
    }

    // protokolu($eble_rilataj_rekalkulendaj_afisxoj, 'jen listo de videoj por kiuj necesas retrovi eble-rilatajn afiŝojn:', $protokoloID);

    $nombro_konservite = 0;
    foreach ($eble_rilataj_rekalkulendaj_afisxoj as $indekso => $identigilo) {
        // krei liston de eble-rilataj afiŝoj por la afiŝo
        $videoj_rilataj_eble = a_spektejo_trovi_eble_rilatajn_videojn($identigilo);
        if (!empty($videoj_rilataj_eble)) {
            // konservi la liston de eble-rilataj afiŝoj por la afiŝo
            update_post_meta($identigilo, 'v_afisxoj_rilataj_eble', $videoj_rilataj_eble);
            // aldoni la eron en la liston de rekalkulendaj eroj (por rilataj afiŝoj)
            // FIXME: Anstataŭ listo, uzu WP-peton en la alia funkcio por trovi tian liston
            $rilataj_rekalkulendaj_afisxoj = get_option(PROJEKTNOMO . '_rilataj_rekalkulendaj_videoj', []);
            $rilataj_rekalkulendaj_afisxoj[] = $identigilo;
            update_option(PROJEKTNOMO . '_rilataj_rekalkulendaj_videoj', array_values(array_unique($rilataj_rekalkulendaj_afisxoj)));

            $nombro_konservite++;
        }

        // protokolu($videoj_rilataj_eble, 'por la afiŝo #' . $identigilo . ', jen la novaj eble-rilataj afiŝoj trovitaj:', $protokoloID);

        // forigi la eron el listo de rekalkulendaj eroj (por eble-rilataj afiŝoj) kaj rekonservi la liston
        unset($eble_rilataj_rekalkulendaj_afisxoj[$indekso]);
        update_option(PROJEKTNOMO . '_eble_rilataj_rekalkulendaj_videoj', array_values($eble_rilataj_rekalkulendaj_afisxoj));

        if ($nombro_konservite >= 10) {
            break; // ne daŭrigu la operacion, se sufiĉe da eroj estis pritraktitaj
        }
    }

    // protokolu(null, 'ĉio kalkulite kaj konservite!', $protokoloID);
}

/**
 * Rekalkuli kaj konservi rilatajn afiŝojn
 */
add_action('ago_rekalkulado_de_rilataj_afisxoj', 'a_spektejo_rekalkuli_rilatajn_afisxojn');
function a_spektejo_rekalkuli_rilatajn_afisxojn()
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    // protokolu(null, 'Saluton, mondo!', $protokoloID);

    // FIXME: Anstataŭ konservita listo, trovu tiajn afiŝojn per WP-peto:
    // petu afiŝojn (kun limo pri la nombro), kiuj havas la meta-kampon `v_afisxoj_rilataj_eble`
    $rekalkulendaj_afisxoj = get_option(PROJEKTNOMO . '_rilataj_rekalkulendaj_videoj', []);
    if (empty($rekalkulendaj_afisxoj)) {
        // protokolu(null, 'nenio por rekalkuli!', $protokoloID);

        return;
    }

    // protokolu($rekalkulendaj_afisxoj, 'jen listo de videoj por kiuj necesas retrovi rilatajn afiŝojn:', $protokoloID);

    $nombro_konservite = 0;
    foreach ($rekalkulendaj_afisxoj as $indekso => $identigilo) {
        // krei liston de rilataj afiŝoj por la afiŝo kaj konservi ĝin por la afiŝo
        $videoj_rilataj = a_spektejo_trovi_rilatajn_videojn($identigilo);

        // protokolu($videoj_rilataj, 'por la afiŝo #' . $identigilo . ', jen la novaj rilataj afiŝoj trovitaj:', $protokoloID);

        if (!empty($videoj_rilataj)) {
            update_post_meta($identigilo, 'v_afisxoj_rilataj', $videoj_rilataj);

            // envicigi la rilatajn afiŝojn por poste rekalkuli iliajn rilatajn afiŝojn por ili
            // a_spektejo_envicigi_videon_por_rekalkuli_rilatajn_afisxojn($videoj_rilataj);

            $nombro_konservite++;
        }

        // forigi la liston de eble-rilataj afiŝoj por la afiŝo
        delete_post_meta($identigilo, 'v_afisxoj_rilataj_eble');

        // forigi la eron el la liston de rekalkulendaj eroj (pri rilataj afiŝoj)
        unset($rekalkulendaj_afisxoj[$indekso]);
        update_option(PROJEKTNOMO . '_rilataj_rekalkulendaj_videoj', array_values($rekalkulendaj_afisxoj));

        if ($nombro_konservite >= 5) {
            break; // ne daŭrigu la operacion, se sufiĉe da eroj estis pritraktitaj
        }
    }

    // protokolu($rekalkulendaj_afisxoj, 'jen la restantaj videoj, kiujn ne pritraktis:', $protokoloID);
}


/**
 * Rekalkuli kaj konservi plej oftajn kategoriojn por la sekvataj kanaloj
 */
// FIXME: Forigi?
// add_action('ago_rekalkulado_de_plejoftaj_kategorioj_por_kanaloj', 'a_spektejo_rekalkuli_plejoftajn_kategoriojn_por_kanaloj');
function a_spektejo_rekalkuli_plejoftajn_kategoriojn_por_kanaloj()
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton, mondo!', $protokoloID);

    // listigi sekvatajn kanalojn
    $kanaloj = get_terms(
        [
            'taxonomy'   => 'v_kanalo',
            'fields'     => 'ids',
            'number'     => 0, // FIXME: Ne faru ĉion samtempe
            'hide_empty' => true,
            'meta_key'   => 'v_kanalo_inkluziveco',
            'meta_value' => 'inkludata'
        ]
    );

    protokolu($kanaloj, 'jen la listo de la sekvataj kanaloj:', $protokoloID);

    // trovi plejoftajn kategoriojn
    foreach ($kanaloj as $kanalo_identigilo) {
        // protokolu(null, 'pritraktas la kanalon #' . $kanalo_identigilo . '...', $protokoloID);

        $plejoftaj_kategorioj = a_spektejo_determini_plejoftajn_kategoriojn_por_kanalo($kanalo_identigilo);

        if (empty($plejoftaj_kategorioj)) {
            continue;
        }

        protokolu($plejoftaj_kategorioj, 'jen la plejoftaj kategorioj por la kanalo #' . $kanalo_identigilo . ':', $protokoloID);

        // $rezulto = update_term_meta($kanalo_identigilo, 'v_kanalo_plejoftaj_kategorioj', $plejoftaj_kategorioj);

        // protokolu($rezulto, 'jen la rezulto pri konservi la plejoftajn kategoriojn por la kanalo:', $protokoloID);
    }
}


/**
 * Rekalkuli liston de lingvoj en kiuj ekzistas plej multe ol minimumo da afiŝoj
 */
add_action('ago_rekalkulado_de_plejuzataj_lingvoj', 'a_spektejo_rekalkuli_plejuzatajn_lingvojn');
function a_spektejo_rekalkuli_plejuzatajn_lingvojn()
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton, mondo!', $protokoloID);

    $lingvoj = a_spektejo_retrovi_terminoliston('v_lingvo', 'count', true);
    $lingvoj_plejuzataj = array_filter(
        $lingvoj,
        function ($lingvo_datumoj, $lingvo_kodo) {
            $minimuma_grandeco  = 100;
            if ($lingvo_datumoj['grandeco'] > $minimuma_grandeco
                && $lingvo_kodo !== 'eo'
                && $lingvo_kodo !== 'xx'
            ) {
                return true;
            } else {
                return false;
            }
        },
        ARRAY_FILTER_USE_BOTH
    );

    protokolu($lingvoj_plejuzataj, 'jen la nemalplenaj lingvoterminoj:', $protokoloID);

    $rezulto = update_option(PROJEKTNOMO . '_lingvoj_plejuzataj', $lingvoj_plejuzataj);

    protokolu($rezulto, 'jen la rezulto pri ĝisnunigi la datumbazokampon:', $protokoloID);
}


/**
 * Rekalkuli la statistikojn de la retejo
 */
add_action('ago_rekalkulado_de_statistikoj', 'a_spektejo_rekalkuli_gxeneralajn_statistikojn');
function a_spektejo_rekalkuli_gxeneralajn_statistikojn()
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton, mondo!', $protokoloID);

    $statistikoj = [
        'dato' => date('y-m-d H:i:s'),
        'datumoj' => [
            'videoj'  => [],
            'kanaloj' => []
        ]
    ];

    // Videoj

    $videoj_sumo = 0;

    $arg_baze_filmoj = [
        'post_type'     => PROJEKTNOMO . '_video',
        'post_status'   => 'publish',
        'numberposts'   => -1, // kara servilo, ni vere amas vin, sed ni ankaŭ bezonas ĉi tion <3
        'fields'        => 'ids',
        'no_found_rows' => true,
        'ignore_sticky_posts'    => true,
        'update_post_meta_cache' => false,
        'update_post_term_cache' => false,
    ];

    $rilatecoj = get_terms(
        [
            'taxonomy' => 'v_rilateco',
            'fields'   => 'id=>name',
            'number'     => 0,
            'hide_empty' => false,
        ]
    );
    foreach ($rilatecoj as $rilateco_identigilo => $rilateco_nomo) {
        $arg_rilateco = array_merge(
            $arg_baze_filmoj,
            [
                'tax_query' => [
                    [
                        'taxonomy' => 'v_rilateco',
                        'field'    => 'term_id',
                        'terms'    => $rilateco_identigilo,
                    ]
                ]
            ]
        );
        $rilateco_nombro = count(get_posts($arg_rilateco));
        $statistikoj['datumoj']['videoj']['parametroj']['rilateco'][$rilateco_nomo] = $rilateco_nombro;
        $videoj_sumo += $rilateco_nombro;
    }

    $lingvoj_plejuzataj = get_option(PROJEKTNOMO . '_lingvoj_plejuzataj', []);
    $lingvoj_plejuzataj['eo'] = ['nomo' => 'Esperanto'];

    protokolu($lingvoj_plejuzataj, 'jen la plejuzataj lingvoj:', $protokoloID);

    $lingvoj_sumo = 0;

    foreach ($lingvoj_plejuzataj as $lingvo_kodo => $lingvo_datumoj) {
        $lingvo_nomo = $lingvo_datumoj['nomo'];
        $arg_lingvo = array_merge(
            $arg_baze_filmoj,
            [
                'tax_query' => [
                    [
                        'taxonomy' => 'v_lingvo',
                        'field'    => 'slug',
                        'terms'    => $lingvo_kodo,
                    ]
                ]
            ]
        );
        $lingvo_nombro = count(get_posts($arg_lingvo));
        $statistikoj['datumoj']['videoj']['parametroj']['lingvoj'][$lingvo_nomo] = $lingvo_nombro;
        $lingvoj_sumo += $lingvo_nombro;
    }
    $statistikoj['datumoj']['videoj']['parametroj']['lingvoj']['Aliaj'] = $videoj_sumo - $lingvoj_sumo;
    $statistikoj['datumoj']['videoj']['sumo'] = $videoj_sumo;


    // Kanaloj

    $kanaloj_sumo = 0;

    $arg_baze_kanaloj = [
        'taxonomy'   => 'v_kanalo',
        'fields'     => 'ids',
        'number'     => 0,
        'hide_empty' => false,
    ];

    $inkluzivecoj = [
        'normala'    => 'Normala',
        'inkludata'  => 'Sekvata',
        'ekskludata' => 'Ignorata',
        'neaprobita' => 'Ne aprobita'
    ];

    foreach ($inkluzivecoj as $inkluziveco => $inkluziveco_nomo) {
        $arg_inkluziveco = array_merge(
            $arg_baze_kanaloj,
            [
                'meta_key'   => 'v_kanalo_inkluziveco',
                'meta_value' => $inkluziveco
            ]
        );
        $inkluziveco_nombro = count(get_terms($arg_inkluziveco));
        $statistikoj['datumoj']['kanaloj']['parametroj']['inkluziveco'][$inkluziveco_nomo] = $inkluziveco_nombro;
        $kanaloj_sumo += $inkluziveco_nombro;
    }
    $statistikoj['datumoj']['kanaloj']['sumo'] = $kanaloj_sumo;

    protokolu($statistikoj, 'jen la statistikoj:', $protokoloID);

    $rezulto = update_option(PROJEKTNOMO . '_statistikoj', $statistikoj);

    protokolu($rezulto, 'jen la rezulto pri ĝisnunigi la datumbazokampon:', $protokoloID);
}
