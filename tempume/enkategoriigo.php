<?php

/**
 * Enkategoriigisto: Kontrolas signalojn de uzantoj pri enkategoriigado de videoj, kaj enkategoriigas videojn laŭe.
 *
 * @package Spektejo
 */

/**
 * Enkategoriigi videojn kiuj ricevis signalojn de uzantoj pri kategorioj
 */
// FIXME: Ĉu forigi?
// add_action('ago_prizorgado_de_kategoriosignaloj', 'a_spektejo_enkategoriigisto');
function a_spektejo_enkategoriigisto()
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton!', $protokoloID);

    global $wpdb;
    $tabelo_nomo = $wpdb->prefix . PROJEKTNOMO . '_signaloj';

    // 1. listigi kategorie-signalojn afiŝojn de la lasta tago

    $sql_peto = "
        SELECT ero_wpid FROM $tabelo_nomo WHERE
            tempo > %s AND
            speco = %s
    ";
    $sql_peto_parametroj = [
        date('Y-m-d H:i:s', time() - (24 * 3600)), // iom antaŭe
        'kategorio'
    ];

    protokolu($sql_peto, 'jen la sql-peto por listigi la afiŝo-identigiloj:', $protokoloID);
    protokolu($sql_peto_parametroj, 'jen la peto-parametroj:', $protokoloID);

    $laste_signalitaj_afisxoj = $wpdb->get_col(
        $wpdb->prepare($sql_peto, $sql_peto_parametroj)
    );

    protokolu($laste_signalitaj_afisxoj, 'jen la rezulto:', $protokoloID);

    if (empty($laste_signalitaj_afisxoj)) {
        protokolu(null, 'Ŝajne neniu afiŝo estis signalita por kategorioj lastatempe. Nuligas la operacion...', $protokoloID);
        return;
    }

    // 2. eltiri ĉiujn kategoriajn signalojn por tiuj afiŝoj

    $sql_peto = " SELECT * FROM $tabelo_nomo WHERE speco = %s ";
    $sql_peto_parametroj = ['kategorio'];

    $sql_peto .= "AND ero_wpid IN (";

    $laste_signalitaj_afisxoj = array_unique($laste_signalitaj_afisxoj);
    $afisxonombro = count($laste_signalitaj_afisxoj);
    foreach ($laste_signalitaj_afisxoj as $indekso => $afisxoidentigilo) {
        $sql_peto .=  "%d";
        if ($indekso + 1 < $afisxonombro) {
            $sql_peto .= ", ";
        }
        $sql_peto_parametroj[] = $afisxoidentigilo;
    }
    $sql_peto .= ")";

    protokolu($sql_peto, 'jen la sql-peto por eltiri la kategoriosignalojn por la afiŝoj:', $protokoloID);
    protokolu($sql_peto_parametroj, 'jen la peto-parametroj:', $protokoloID);

    $kategoriaj_signaloj = $wpdb->get_results(
        $wpdb->prepare($sql_peto, $sql_peto_parametroj),
        'ARRAY_A'
    );

    protokolu($kategoriaj_signaloj, 'jen la rezulto:', $protokoloID);

    if (empty($kategoriaj_signaloj)) {
        protokolu(null, 'Eraro: Neniu kategoria signalo troviĝis por la listigitaj afiŝoidentigiloj. Nuligas la operacion...', $protokoloID);
        return;
    }

    $signaloj_lauxafisxe = [];

    foreach ($kategoriaj_signaloj as $signalo) {
        $afisxo = $signalo['ero_wpid'];
        $signaloj_lauxafisxe[$afisxo][] = $signalo['valoro'];
    }

    protokolu($signaloj_lauxafisxe, 'jen la kategoriaj signaloj por ĉiu afiŝo:', $protokoloID);

    $signalonombroj_lauxafisxe = array_map('array_count_values', $signaloj_lauxafisxe);

    protokolu($signalonombroj_lauxafisxe, 'jen la nombro de kategoriaj signaloj por ĉiu afiŝo:', $protokoloID);


    // 3. por ĉiu afiŝo, atribui la kategoriojn signalitajn pli ol minimumfoje
    // TODO: Reverku pli elegante, uzante array_filter

    $minimuma_signalonombro = 3;
    foreach ($signalonombroj_lauxafisxe as $afisxoidentigilo => $kategorioj) {
        foreach ($kategorioj as $kategorio => $signalonombro) {
            if ($signalonombro >= $minimuma_signalonombro) {
                protokolu(null, 'atribuas la kategorion ' . $kategorio . ' (' . $signalonombro . ' signaloj) al la afiŝo #' . $afisxoidentigilo . '...', $protokoloID);

                $enkategoriigorezulto = wp_set_object_terms(intval($afisxoidentigilo), $kategorio, 'v_kategorioj', true);
                a_spektejo_envicigi_videon_por_rekalkuli_rilatajn_afisxojn($afisxoidentigilo);

                protokolu($enkategoriigorezulto, 'jen la rezulto:', $protokoloID);

                if (is_wp_error($enkategoriigorezulto)) {
                    protokolu(null, 'Eraro: ne sukcesis atribui kategoriojn.', $protokoloID);
                }
            }
        }
    }
}
