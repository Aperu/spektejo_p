<?php

/**
 * Pordisto: Procesas novajn kolektaĵojn, decidas kiun enlasi.
 *
 * @package Spektejo
 */

/**
 * Ĝisnunigi la liston de sekvataj kanaloj
 */
add_action('ago_pordumado', 'a_spektejo_pordisto_gxisnunigi_inkludajn_kanalojn');
function a_spektejo_pordisto_gxisnunigi_inkludajn_kanalojn()
{

    // 0. Kontroli ĉu la dosiero ekzistas

    $dosiero = ABSPATH . 'robotoj/kolektistoj/kanaloj_inkludaj.json';
    if (! is_file($dosiero)) {
        return;
    }



    // 1. Kolekti la sekvatajn kanalojn

    $arg =  [
        'taxonomy'    => 'v_kanalo',
        'orderby'     => 'count',
        'order'       => 'DESC',
        'hide_empty'  => false,
        'meta_key'    => 'v_kanalo_inkluziveco',
        'meta_value'  => 'inkludata',
    ];
    $kanaloterminoj_inkludaj = get_terms($arg);

    if (empty($kanaloterminoj_inkludaj) || is_wp_error($kanaloterminoj_inkludaj)) {
        return;
    }



    // 2. Krei JSON-datumaron

    $inkludaj_kanaloj = [];
    foreach ($kanaloterminoj_inkludaj as $termino) {
        $inkluda_kanalo_term_id    = $termino->term_id;
        $inkluda_kanalo_nomo       = $termino->name;
        $inkluda_kanalo_nometo     = $termino->slug;
        $inkluda_kanalo_identigilo = preg_replace("/^[a-z]{3}_/i", "", $inkluda_kanalo_nometo);
        $inkluda_kanalo_fonto      = get_term_meta($inkluda_kanalo_term_id, 'v_kanalo_fonto', true);

        $inkluda_kanalo = [
            'nomo'       => $inkluda_kanalo_nomo,
            'identigilo' => $inkluda_kanalo_identigilo,
            'fonto'      => $inkluda_kanalo_fonto
        ];
        $inkludaj_kanaloj[] = $inkluda_kanalo;
    }
    $inkludaj_kanaloj_json = json_encode($inkludaj_kanaloj, JSON_PRETTY_PRINT);



    // 3. Konservi la datumaron

    file_put_contents($dosiero, $inkludaj_kanaloj_json);
}





/**
 * Pritrakti la novajn kolektitajn filmojn
 */
add_action('ago_pordumado', 'a_spektejo_pordisto_pritrakti_kolektajxojn');
function a_spektejo_pordisto_pritrakti_kolektajxojn()
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(__FUNCTION__, 'Saluton! la funkcio estas alvokita', $protokoloID);

    // 0. Kontroli ĉu la dosiero ekzistas

    $dosiero = ABSPATH . 'robotoj/kolektistoj/kolektajxoj.json';
    if (! is_file($dosiero)) {
        return;
    }



    // 1. Legi novajn kolektaĵojn el dosiero

    $json_dosiero = file_get_contents($dosiero);
    if (empty($json_dosiero)) {
        return;
    }
    // $kolektajxoj  = array_reverse(json_decode($json_dosiero, true));
    $kolektajxoj  = json_decode($json_dosiero, true);
    if (empty($kolektajxoj)) {
        return;
    }

    //protokolu($kolektajxoj, 'Pritraktante ĉiun eron de la kolektaĵoj:' , $protokoloID);


    // 2. Pritrakti ĉiun eron de la kolektaĵoj

    foreach ($kolektajxoj as $kolektajxo) {
        protokolu(null, 'pritraktante la eron ' .  $kolektajxo['identigilo'] . '...', $protokoloID);

        // 2.1. Inici afiŝodatumojn

        // titolo kaj priskribo
        $v_titolo    = $kolektajxo['titolo'];
        $v_priskribo = $kolektajxo['priskribo'];

        // fonto kaj fontokodo
        $v_fonto = $kolektajxo['fonto'];
        switch ($v_fonto) {
            case 'youtube':
                $v_fonto_kodo  = 'ytb';
                break;
            default:
                $v_fonto_kodo  = '_';
        }

        // lingvo kaj rilateco
        if (! empty($kolektajxo['lingvo'])) {
            $v_lingvo = substr($kolektajxo['lingvo'], 0, 2);
        } else {
            $v_lingvo = 'xx';
        }

        // provi diveni la lingvon, se ĝi estas specifita kiel EO (aŭ ne estas specifita)
        if ($v_lingvo != 'eo'
            && (! empty($v_priskribo) || str_word_count($v_titolo) > 5)
        ) {
            $lingvo_specimeno = $v_titolo . ' ' . $v_priskribo;
            $lingvo_specimeno = a_spektejo_prepari_tekstospecimenon(a_spektejo_seniksigi_vorton($lingvo_specimeno));
            $v_lingvo         = a_spektejo_e_detekti_lingvon($lingvo_specimeno);
        }

        if (! empty($v_lingvo)) {
            if ($v_lingvo == 'eo') {
                $v_rilateco = 'esperanta';
            } else {
                $v_rilateco = 'priesperanta';
            }
        } else {
            $v_rilateco = 'nedifina';
        }

        $v_kanalo = $kolektajxo['kanalo'];

        // identigiloj
        $v_identigilo_platforma = $kolektajxo['identigilo'];
        $v_nometo               = $v_fonto_kodo . '_' . $v_identigilo_platforma;
        $v_kanalo_nometo        = $v_fonto_kodo . '_' . $v_kanalo;

        // afiŝodatumaro
        $videodatumoj = [
            'identigilo'     => $v_identigilo_platforma,
            'titolo'         => $v_titolo,
            'priskribo'      => $v_priskribo,
            'auxtoro'        => $kolektajxo['auxtoro'],
            'kanalo'         => $v_kanalo,
            'lingvo'         => $v_lingvo,
            'dato'           => $kolektajxo['dato'],
            'dauxro'         => $kolektajxo['dauxro'],
            'etikedoj'       => $kolektajxo['etikedoj'],
            'fonto'          => $v_fonto ,
            'fonto-kodo'     => $v_fonto_kodo ,
            'nometo'         => $v_nometo ,
            'kanalo-nometo'  => $v_kanalo_nometo ,
            'rilateco'       => $v_rilateco ,
        ];

        protokolu($videodatumoj, 'jen la preparitaj datumoj:', $protokoloID);

        // 2.2. Aldoni afiŝon

        // kontroli, ĉu la afiŝo jam ne ekzistas en la datumbazo
        $arg = [
            'post_type'   => PROJEKTNOMO . '_video',
            'name'        => $v_nometo,
            'post_status' => ['publish', 'pending'],
            //'fields'  => 'ids',
            'numberposts' => 1
        ];
        $samnomaj_afisxoj = get_posts($arg);
        if (!empty($samnomaj_afisxoj)) {
            protokolu(null, 'la afiŝo ' . $v_nometo . ' jam ekzistas. ignorante...', $protokoloID);
            continue;
        }

        // kontroli, ĉu la afiŝinta kanalo ne estas ekskludata
        $v_kanalotermino = get_term_by('slug', $v_kanalo_nometo, 'v_kanalo');
        if ($v_kanalotermino) {
            $v_kanalotermino_ID = $v_kanalotermino->term_id;
            $v_kanalo_inkluziveco = get_term_meta($v_kanalotermino_ID, 'v_kanalo_inkluziveco', true);
            if ($v_kanalo_inkluziveco == 'ekskludata') {
                protokolu(null, 'la afiŝinta kanalo ' . $v_kanalo_nometo . ' estas ekskludata. ignorante...', $protokoloID);
                continue;
            }
        }

        // Krei afiŝon, se ĉio enordas
        protokolu(null, 'kreos la afiŝon...', $protokoloID);

        $publikeco   = ($v_rilateco == 'nedifina') ? false : true;
        $kreorezulto = a_spektejo_krei_videoafisxon($videodatumoj, $publikeco);

        // fari la restajn necesaĵojn
        if (!empty($kreorezulto['identigilo'])) {
            $v_afisxo_n_id = $kreorezulto['identigilo'];

            protokolu(null, 'kreis la afiŝon ' . $v_nometo . ' kun la identigilo ' . $v_afisxo_n_id . '.', $protokoloID);
            protokolu(null, 'trovos rilatajn afiŝojn...', $protokoloID);

            // envicigi la videon por poste rekalkuli ĝiajn rilatajn afiŝojn
            a_spektejo_envicigi_videon_por_rekalkuli_rilatajn_afisxojn($v_afisxo_n_id);
            // $videoj_rilataj = a_spektejo_trovi_rilatajn_videojn($v_afisxo_n_id);
            // update_post_meta($v_afisxo_n_id, 'v_afisxoj_rilataj', $videoj_rilataj);

            // envicigi la rilatajn afiŝojn por poste rekalkuli iliajn rilatajn afiŝojn por ili
            // a_spektejo_envicigi_videon_por_rekalkuli_rilatajn_afisxojn($videoj_rilataj);

            // protokolu($videoj_rilataj, 'trovis la jenajn rilatajn afiŝojn:', $protokoloID);
        } else {
            protokolu($kreorezulto, 'krei la afiŝon malsukcesis. jen la rezulto:', $protokoloID);
        }
    }
}
