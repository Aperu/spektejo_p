<?php

/**
 * Helpaj funkcioj, kiuj uzas eksterajn programojn aŭ servojn
 *
 * @package Spektejo
 */

// Ŝargi la aldonaĵojn
include plugin_dir_path(__FILE__) . '/aldone/composer/vendor/autoload.php';




/* == LINGVODETEKTILO == */
// https://github.com/patrickschur/language-detection

// Inici la lingvodetektilon
use LanguageDetection\Language;


/**
 * Provi detekti la lingvon uzante la programon `language-detection`
 * https://github.com/patrickschur/language-detection
 * a_spektejo_e_detekti_lingvon
 *
 * @param string   $specimeno
 *
 * @return string   du-litera lingvokodo
 */
function a_spektejo_e_detekti_lingvon($specimeno)
{
    if (!class_exists('LanguageDetection\Language')) {
        return 'xx';
    }

    if (empty(trim($specimeno))) {
        return 'xx';
    }

    $ld = new Language();
    $ld->setMaxNgrams(9000); // https://github.com/patrickschur/language-detection#how-can-i-improve-the-detection-phase & https://github.com/patrickschur/language-detection/issues/10
    // $lingvo = $ld->detect($specimeno)->bestResults()->close();
    $lingvo = $ld->detect($specimeno);
    $lingvo = $lingvo->blacklist('io', 'ch'); // forigi Idon kaj la ĉamoran lingvon (pro tro da falsaj pozitivaĵoj)
    $lingvo = substr($lingvo, 0, 2);

    return $lingvo;
}





/* == Jutuba API kaj rilataj funkcioj == */




/**
 * Tiri jutuban video-identigilon el jutuba adreso
 * a_spektejo_eltiri_videoidentigilon_el_adreso_ytb
 *
 * @param string $adreso   jutuba adreso
 *
 * @return string   la jutuba "videoID"
 */
function a_spektejo_eltiri_videoidentigilon_el_adreso_ytb($adreso)
{

    if (empty($adreso)) {
        return false;
    }

    $re_vID_trov = [];
    $re_vID      = '/(?:[?&]v=|\/embed\/|\/1\/|\/v\/|(?:www\.)?youtu\.be\/)([^&\n?#]+)/i';
    if (! preg_match($re_vID, $adreso, $re_vID_trov)) {
        return false;
    }
    $identigilo = $re_vID_trov[1];

    return $identigilo;
}





/**
 * Konstrui adreson laŭ parametroj, por konektiĝi al la jutuba videa API "videos"
 * a_spektejo_konstrui_api_adreson_por_videodatumoj_ytb
 *
 * @param array   $parametroj
 *
 * @return string  adreso por konektiĝi al la API
 */
function a_spektejo_konstrui_api_adreson_por_videodatumoj_ytb($parametroj = [])
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton, mondo!', $protokoloID);
    protokolu($parametroj, 'jen la parametroj:', $protokoloID);

    // 1. Kontroli la enigaĵon

    if (empty($parametroj) || ! is_array($parametroj)) {
        return false;
    }

    if (isset($parametroj['identigilo']) && ! empty($parametroj['identigilo'])) {
        $v_identigilo = $parametroj['identigilo'];
    } elseif (isset($parametroj['identigilaro'])) {
        $v_identigilaro = $parametroj['identigilaro'];
        $v_identigilo   = urlencode(implode(',', $v_identigilaro)); // Jutubo akceptas maksimume 50 identigilojn ĉiufoje (Dec. 2018);
    } elseif (isset($parametroj['adreso']) && ! empty($parametroj['adreso'])) {
        $v_adreso     = $parametroj['adreso'];
        $v_identigilo = a_spektejo_eltiri_videoidentigilon_el_adreso_ytb($v_adreso);
    } else {
        return false;
    }

    protokolu($v_identigilo, 'jen la identigilo(j):', $protokoloID);



    // 2. Konstrui la adreson

    $api_kodo      = get_option(PROJEKTNOMO . '_apikodo_youtube', '');
    $api_bazo      = 'https://www.googleapis.com/youtube/v3/';
    $api_finpunkto = 'videos';
    if (isset($parametroj['partoj'])) {
        $api_partoj = urlencode(implode($parametroj['partoj']));
    } else {
        $api_partoj = urlencode('snippet,contentDetails');
    }

    protokolu($api_partoj, 'je la la API-partoj:', $protokoloID);


    if (isset($parametroj['kampoj']) && $parametroj['kampoj'] == 'id') {
        $api_kampoj = urlencode('items(id)');
    } else {
        $api_kampoj = urlencode('items(contentDetails(caption,duration),id,snippet(channelId,channelTitle,description,publishedAt,tags,title))');
    }

    protokolu($api_kampoj, 'je la la API-kampoj:', $protokoloID);

    $peto_adreso   = $api_bazo . $api_finpunkto . '?part=' . $api_partoj . '&id=' . $v_identigilo . '&fields=' . $api_kampoj . '&key=' . $api_kodo;

    protokolu($peto_adreso, 'jen la fina adreso por la peto:', $protokoloID);

    return $peto_adreso;
}





/**
 * Konstrui adreson laŭ parametroj, por konektiĝi al la jutuba kanala API "channels"
 * a_spektejo_konstrui_api_adreson_por_kanalodatumoj_ytb
 *
 * @param array $parametroj
 *
 * @return adreso por konektiĝi al la API
 */
function a_spektejo_konstrui_api_adreson_por_kanalodatumoj_ytb($parametroj = [])
{

    protokolu($parametroj, 'la parametroj estas jene:');

    // kontroli la enigaĵon
    if (empty($parametroj) || ! is_array($parametroj)) {
        return false;
    }


    if (isset($parametroj['identigilo']) && ! empty($parametroj['identigilo'])) {
        $k_identigilo = $parametroj['identigilo'];
    } elseif (isset($parametroj['adreso']) && ! empty($parametroj['adreso'])) {
        $k_adreso = $parametroj['adreso'];

        $re_kID_trov   = [];

        if (strpos($k_adreso, '/channel')) {
            $re_kID = '/channel\/([a-zA-Z0-9\-_]{21,})/';
            if (! preg_match($re_kID, $k_adreso, $re_kID_trov)) {
                return false;
            }
            $k_identigilo = $re_kID_trov[1];
        } elseif (strpos($k_adreso, '/user')) {
            $re_kID = '/user\/\@?([a-zA-Z0-9\-_]{1,})/';
            if (! preg_match($re_kID, $k_adreso, $re_kID_trov)) {
                return false;
            }
            $k_uzantonomo = $re_kID_trov[1];
        } else {
            return false;
        }
    } else {
        return false;
    }

    // konstrui la adreson
    $api_kodo      = get_option(PROJEKTNOMO . '_apikodo_youtube', '');
    $api_bazo      = 'https://www.googleapis.com/youtube/v3/';
    $api_finpunkto = 'channels';
    $api_partoj    = urlencode('snippet');
    $peto_adreso   = $api_bazo . $api_finpunkto . '?part=' . $api_partoj . '&key=' . $api_kodo;

    if (isset($k_identigilo)) {
        $peto_adreso  .= '&id=' . $k_identigilo;
    } elseif (isset($k_uzantonomo)) {
        $peto_adreso  .= '&forUsername=' . $k_uzantonomo;
    } else {
        return false;
    }

    protokolu($peto_adreso, 'la petoadreso estas jene:');

    return $peto_adreso;
}





/**
 * Tiri necesajn datumojn por videoafiŝo el datumoj el la jutuba API
 * a_spektejo_konstrui_videodatumojn_el_ricevajxo_ytb
 *
 * @param array  $datumoj
 *
 * @return array  preparitaj video-datumoj
 */
function a_spektejo_konstrui_videodatumojn_el_ricevajxo_ytb($datumoj = [])
{

    if (empty($datumoj) || ! is_array($datumoj)) {
        return false;
    }

    if (!isset($datumoj['items'][0]['id'])) {
        return false;
    }

    $v_identigilo          = $datumoj['items'][0]['id'];
    $datumoj_peceto        = $datumoj['items'][0]['snippet'];
    $datumoj_enhavodetaloj = $datumoj['items'][0]['contentDetails'];

    $v_titolo    = $datumoj_peceto['title'];
    $v_priskribo = $datumoj_peceto['description'];
    $v_dato      = $datumoj_peceto['publishedAt'];
    $v_kanalo    = $datumoj_peceto['channelId'];
    $v_auxtoro   = $datumoj_peceto['channelTitle'];
    $v_dauxro    = $datumoj_enhavodetaloj['duration'];
    $v_etikedoj  = (isset($datumoj_peceto['tags'])) ? $datumoj_peceto['tags'] : [];

    $videodatumoj  = [
        'identigilo' => $v_identigilo,
        'titolo'     => $v_titolo,
        'priskribo'  => $v_priskribo,
        'dato'       => $v_dato,
        'kanalo'     => $v_kanalo,
        'auxtoro'    => $v_auxtoro,
        'dauxro'     => $v_dauxro,
        'etikedoj'   => $v_etikedoj,
    ];

    return $videodatumoj;
}





/**
 * Tiri necesajn datumojn por kanalo el datumoj el la jutuba API
 * a_spektejo_konstrui_kanalodatumojn_el_ricevajxo_ytb
 *
 * @param array $datumoj
 *
 * @return array  preparitaj kanalo-datumoj
 */
function a_spektejo_konstrui_kanalodatumojn_el_ricevajxo_ytb($datumoj = [])
{

    if (empty($datumoj) || ! is_array($datumoj)) {
        return false;
    }

    if (!isset($datumoj['items'][0]['id'])) {
        return false;
    }

    $k_identigilo   = $datumoj['items'][0]['id'];
    $datumoj_peceto = $datumoj['items'][0]['snippet'];
    $k_titolo       = $datumoj_peceto['title'];
    // TODO: Eltiri ankaŭ aliajn informojn

    $kanalodatumoj  = [
        'identigilo' => $k_identigilo,
        'titolo'     => $k_titolo,
    ];

    return $kanalodatumoj;
}





/**
 * Kontroli, ĉu videoj estas disponeblaj, kaj marki la nedisponeblajn
 * (Ricevas aranĝaĵon da afiŝoidentigiloj)
 * a_spektejo_kontroli_disponeblecon_ytb
 *
 * @param array $videoj
 */
// FIXME: Ĉu forigi?
function a_spektejo_kontroli_disponeblecon_ytb($videoj)
{

    if (! is_array($videoj)) {
        return;
    }

    // TODO: Apartigi ĉiun 30 videon
    $videoj_kontrolendaj_id  = [];

    foreach ($videoj as $identigilo) {
        if (is_numeric($identigilo)) {
            $kv_identigilo = get_post_meta($identigilo, 'v_identigilo_fonto', true);
            $videoj_kontrolendaj_id[]  = $kv_identigilo;
        }
    }

    $videoj_kontrolendaj_id  = array_unique($videoj_kontrolendaj_id);

    if (empty($videoj_kontrolendaj_id)) {
        return;
    }


    $peto_adreso  = a_spektejo_konstrui_api_adreson_por_videodatumoj_ytb(['identigilaro' => $videoj_kontrolendaj_id, 'kampoj' => 'id']);
    if (! $peto_adreso) {
        return;
    }

    $peto_datumoj = a_spektejo_elsxuti_json($peto_adreso);
    if (! $peto_datumoj) {
        return;
    }
    if (! isset($peto_datumoj['items']) || ! is_array($peto_datumoj['items'])) {
        return;
    }

    $eroj = $peto_datumoj['items'];
    $videoj_disponeblaj_id = [];

    foreach ($eroj as $ero) {
        if (isset($ero['id'])) {
            $videoj_disponeblaj_id[] = $ero['id'];
        }
    }


    if (! empty($videoj_disponeblaj_id)) {
        $arg = [
            'post_type'   => PROJEKTNOMO . '_video',
            //'meta_query'  => $arg_m,
            'numberposts' => -1,
            'fields'      => 'ids',
        ];

        $arg_m  = [];
        foreach ($videoj_disponeblaj_id as $identigilo) {
            $arg_m[]  = [
                'key'   => 'v_identigilo_fonto',
                'value' => $identigilo,
            ];
        }
        if (count($arg_m) > 1) {
            $arg_m['relation']  = 'OR';
        }

        if (! empty($arg_m)) {
            $arg['meta_query']  = $arg_m;
        }

        $videoafisxoj_disponeblaj = get_posts($arg);


        // forigi raportojn pri nedisponebleco por disponeblaj videoj
        if (! empty($videoafisxoj_disponeblaj) && is_array($videoafisxoj_disponeblaj)) {
            foreach ($videoafisxoj_disponeblaj as $identigilo) {
                a_spektejo_forigi_signalon('video', $identigilo, 'nedisponebla');
            }
        }
    }


    // listigi la videojn kiuj ne ekzistas ĉe la fonto (la nedisponeblaj)

    $videoj_nedisponeblaj_id = array_diff($videoj_kontrolendaj_id, $videoj_disponeblaj_id);

    if (! empty($videoj_nedisponeblaj_id)) {
        $arg = [
            'post_type'   => PROJEKTNOMO . '_video',
            'numberposts' => -1,
            'fields'      => 'ids',
        ];

        $arg_m  = [];
        foreach ($videoj_nedisponeblaj_id as $identigilo) {
            $arg_m[] = [
                'key'   => 'v_identigilo_fonto',
                'value' => $identigilo,
            ];
        }
        if (count($arg_m) > 1) {
            $arg_m['relation']  = 'OR';
        }

        if (! empty($arg_m)) {
            $arg['meta_query']  = $arg_m;
        }
        $videoafisxoj_nedisponeblaj = get_posts($arg);

        // marki la videojn kiel nedisponeblajn
        if (! empty($videoafisxoj_nedisponeblaj) && is_array($videoafisxoj_nedisponeblaj)) {
            foreach ($videoafisxoj_nedisponeblaj as $identigilo) {
                wp_set_object_terms($identigilo, 'nedisponebla', 'v_rilateco');
            }
        }
    }
}
