<?php

/**
 * Baza starigado, inkluzive:
 * difini bazan datumostrukturon (afiŝospecoj kaj taksonomioj)
 * kaj pretigi kromajn datumokampojn laŭ agordodosieroj
 *
 * @package Spektejo
 */


/* == Taksonomioj == */

/**
 * Krei taksonomiojn
 */
function a_spektejo_taksonomioj()
{

    // Fonto
    $labels = [
        'name'                => 'Fonto',
        'singular_name'       => 'Fonto',
        'popular_items'       => 'Popularaj fontoj',
        'all_items'           => 'Ĉiuj fontoj',
        'parent_item'         => null,
        'parent_item_colon'   => null,
        'edit_item'           => 'Redakti fonton',
        'add_new_item'        => 'Krei novan fonton',
        'new_item_name'       => 'La nomo de la nova fonto',
        'add_or_remove_items' => 'Aldoni/forigi fonton',
        'menu_name'           => 'Fontoj',
    ];
    $args = [
        'hierarchical'          => false,
        'labels'                => $labels,
        'show_ui'               => false,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => ['slug' => 'fonto'],
    ];
    register_taxonomy('v_fonto', PROJEKTNOMO . '_video', $args);


    // Kanalo
    $labels = [
        'name'              => 'Kanalo',
        'singular_name'     => 'Kanalo',
        'search_items'      => 'Serĉi kanalon',
        'popular_items'     => 'Popularaj kanaloj',
        'all_items'         => 'Ĉiuj kanaloj',
        'parent_item'       => null,
        'parent_item_colon' => null,
        'edit_item'         => 'Redakti kanalon',
        'update_item'       => 'Konservi ŝanĝojn',
        'add_new_item'      => 'Aldoni novan kanalon',
        'new_item_name'     => 'La nomo de la nova kanalo',
        'not_found'         => 'Neniu kanalo troviĝis.',
        'menu_name'         => 'Kanaloj',
    ];
    $args = [
        'hierarchical'          => false,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => ['slug' => 'k'],
    ];
    register_taxonomy('v_kanalo', PROJEKTNOMO . '_video', $args);


    // Kategorioj
    $labels = [
        'name'                       => 'Kategorioj',
        'singular_name'              => 'Kategorio',
        'search_items'               => 'Serĉi kategoriojn',
        'popular_items'              => 'Plejuzataj kategorioj',
        'all_items'                  => 'Ĉiuj kategorioj',
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => 'Redakti kategorion',
        'update_item'                => 'Konservi ŝanĝojn',
        'add_new_item'               => 'Krei novan kategorion',
        'new_item_name'              => 'La nova kategorio',
        'separate_items_with_commas' => 'Apartigu kategoriojn per komoj',
        'add_or_remove_items'        => 'Aldoni/forigi kategoriojn',
        'choose_from_most_used'      => 'Elekti el la plej uzataj',
        'not_found'                  => 'Neniu kategorio troviĝis.',
        'menu_name'                  => 'Kategorioj',
    ];
    $args = [
        'hierarchical'          => false,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => ['slug' => 'kategorioj'],
    ];
    register_taxonomy('v_kategorioj', PROJEKTNOMO . '_video', $args);


    // Etikedoj
    $labels = [
        'name'                       => 'Etikedoj',
        'singular_name'              => 'Etikedo',
        'search_items'               => 'Serĉi etikedojn',
        'popular_items'              => 'Plejuzataj etikedoj',
        'all_items'                  => 'Ĉiuj etikedoj',
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => 'Redakti etikedon',
        'update_item'                => 'Konservi ŝanĝojn',
        'add_new_item'               => 'Krei novan etikedon',
        'new_item_name'              => 'La nova etikedo',
        'separate_items_with_commas' => 'Apartigu etikedojn per komoj',
        'add_or_remove_items'        => 'Aldoni/forigi etikedojn',
        'choose_from_most_used'      => 'Elekti el la plej uzataj',
        'not_found'                  => 'Neniu etikedo troviĝis.',
        'menu_name'                  => 'Etikedoj',
    ];
    $args = [
        'hierarchical'          => false,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => ['slug' => 'etikedoj'],
    ];
    register_taxonomy('v_etikedoj', PROJEKTNOMO . '_video', $args);


    // Rilateco
    $labels = [
        'name'              => 'Rilateco',
        'singular_name'     => 'Rilateco',
        'parent_item'       => null,
        'parent_item_colon' => null,
        'edit_item'         => 'Redakti',
        'update_item'       => 'Konservi ŝanĝojn',
        'menu_name'         => 'Rilateco',
    ];
    $args = [
        'hierarchical'          => false,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => ['slug' => 'rilateco'],
    ];
    register_taxonomy('v_rilateco', PROJEKTNOMO . '_video', $args);


    // Atribuoj
    $labels = [
        'name'              => 'Atribuoj',
        'singular_name'     => 'Atribuoj',
        'parent_item'       => null,
        'parent_item_colon' => null,
        'edit_item'         => 'Redakti',
        'update_item'       => 'Konservi ŝanĝojn',
        'menu_name'         => 'Atribuoj',
    ];
    $args = [
        'hierarchical'          => false,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => ['slug' => 'atribuoj'],
    ];
    register_taxonomy('v_atribuoj', PROJEKTNOMO . '_video', $args);



    // Lingvo
    $labels = [
        'name'              => 'Lingvo',
        'singular_name'     => 'Lingvo',
        'parent_item'       => null,
        'parent_item_colon' => null,
        'edit_item'         => 'Redakti',
        'update_item'       => 'Konservi ŝanĝojn',
        'menu_name'         => 'Lingvo',
    ];
    $args = [
        'hierarchical'          => false,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => ['slug' => 'lingvo'],
    ];
    register_taxonomy('v_lingvo', PROJEKTNOMO . '_video', $args);
}
add_action('init', 'a_spektejo_taksonomioj', 0);


/**
 * Krei defaŭltajn terminojn por taksonomioj
 */
function a_spektejo_aldoni_terminojn()
{

    // Rilatecoj
    $rilatecoj = [
        ['Esperantlingva',    'esperanta',    'Esperantlingva filmo, ĉu pri Esperanto aŭ ne.',                         1],
        ['Alilingva, rilata', 'priesperanta', 'Alilingva filmo pri Esperanto aŭ alie rilata al Esperanto.',            2],
        ['Nerilata/Maltaŭga', 'nerilata',     'Neesperanta filmo kun enhavo senrilata al esperanto.',                  3],
        ['Spama',             'spama',        'Trompa reklamo kaj spamo.',                                             4],
        ['Neoriginala',       'neoriginala',  'Neoriginala kopio de alia filmo',                                       5],
        ['Nedisponebla',      'nedisponebla', 'Forigita aŭ alie ne plu disponebla filmo.',                             6],
        ['Nedifina',          'nedifina',     'Video kun nedetektita lingvo aŭ alie nedifina stato. Bezonas atenton.', 10],
    ];
    foreach ($rilatecoj as $rilateco) {
        if (!term_exists($rilateco[1], 'v_rilateco')) {
            wp_insert_term($rilateco[0], 'v_rilateco', ['description' => $rilateco[2], 'slug' => $rilateco[1]]);
        }
    }

    // Atribuoj
    $atribuoj = [
        ['Renovigenda', 'gxisnunigenda', 'La datumoj por la filmo bezonas ĝisnunigon.'],
    ];
    foreach ($atribuoj as $atribuo) {
        if (!term_exists($atribuo[1], 'v_atribuoj')) {
            wp_insert_term($atribuo[0], 'v_atribuoj', ['description' => $atribuo[2], 'slug' => $atribuo[1]]);
        }
    }

    // Kategorioj
    $kategorioj = [
        [
            'Arto k Literaturo',
            'arto-literaturo',
            'Filmoj pri arto kaj literaturo'
        ],
        [
            'Muziko',
            'muziko',
            'Muzikvideoj kaj koncertoj'
        ],
        [
            'Filmo',
            'filmo',
            'Kinaj filmoj'
        ],
        [
            'Kulturo k Historio',
            'kulturo-historio',
            'Filmoj pri kulturo kaj historio'
        ],
        [
            'Filozofio',
            'filozofio',
            'Filmoj pri filozofiaj temoj'
        ],
        [
            'Religio k Supernaturo',
            'religio-supernaturo',
            'Filmoj pri religio kaj supernaturaj temoj'
        ],
        [
            'Politiko k Ekonomio',
            'politiko-ekonomio',
            'Filmoj pri politiko kaj ekonomio'
        ],
        [
            'Socio k Aktivismo',
            'socio-aktivismo',
            'Filmoj pri socio kaj aktivismo'
        ],
        [
            'Lingvoj',
            'lingvo',
            'Filmoj pri lingvoj'
        ],
        [
            'Lernado k Edukado',
            'lernado',
            'Edukaj kaj instruaj filmoj'
        ],
        [
            'Scienco k Teĥnologio',
            'scienco-tehxnologio',
            'Filmoj pri scienco kaj teĥnologio'
        ],
        [
            'Naturo k Medio',
            'naturo-medio',
            'Filmoj pri naturo kaj medio'
        ],
        [
            'Vojaĝo',
            'vojagxo',
            'Filmoj de vojaĝoj kaj ekskursoj'
        ],
        [
            'Manĝoj k Kuirado',
            'mangxado',
            'Filmoj pri manĝaĵoj, trinkaĵoj kaj kuirado'
        ],
        [
            'Sporto',
            'sporto',
            'Filmoj pri sporto'
        ],
        [
            'Ŝercoj k Amuziĝo',
            'sxerco',
            'Ŝercaj kaj amuzaj filmoj'
        ],
        [
            'Ludoj',
            'ludado',
            'Filmoj de videoludoj kaj aliaj ludoj'
        ],
        [
            'Esperanta Movado',
            'eo-movado',
            'Filmoj de kongresoj kaj ĉio rilate al la movado'
        ],
        [
            'Novaĵo',
            'novajxo',
            'Novaĵoj kaj anoncoj'
        ],
        [
            'Vlogoj k Podkastoj',
            'vlogo-podkasto',
            'Videoblogoj, podkastoj kaj radioelsendoj'
        ]
    ];
    foreach ($kategorioj as $kategorio) {
        if (!term_exists($kategorio[0], 'v_kategorioj')) {
            wp_insert_term($kategorio[0], 'v_kategorioj', ['slug' => $kategorio[1], 'description' => $kategorio[2] ?? '']);
            /* $identigiloj = wp_insert_term($kategorio[0], 'v_kategorioj', ['slug' => $kategorio[1]]);
            if (! is_wp_error($identigiloj)) {
                $terminoidentigilo = $identigiloj['term_id'];
                if (isset($kategorio[2]) && is_array($kategorio[2])) {
                    $kromdatumoj = $kategorio[2];
                    foreach ($kromdatumoj as $indekso => $valoro) {
                        update_term_meta($terminoidentigilo, $indekso, $valoro);
                    }
                }
            } */
        }
    }

    // Lingvoj
    $lingvoj = [
        'eo' => 'Esperanto',
        'xx' => 'Nespecifa'
    ];


    foreach ($lingvoj as $lingvokodo => $lingvonomo) {
        if (!term_exists($lingvokodo, 'v_lingvo')) {
            wp_insert_term($lingvonomo, 'v_lingvo', ['slug' => $lingvokodo]);
        }
    }
}
add_action('init', 'a_spektejo_aldoni_terminojn');

/**
 * Aldoni aliajn lingvoterminojn, el JSON-dosiero
 * (nur je instalo de la kromprogramo)
 */
function a_spektejo_enporti_lingvoterminojn()
{
    if (!current_user_can('administrator') || wp_doing_ajax()) {
        return;
    }

    $lingvoj_dosiero = file_get_contents(ABSPATH . 'datumoj/lingvoj.json');
    if ($lingvoj_dosiero === false) {
        return;
    }

    $lingvoj = json_decode($lingvoj_dosiero, true);
    foreach ($lingvoj as $lingvokodo => $lingvonomo) {
        if (term_exists($lingvokodo, 'v_lingvo')) {
            continue;
        }

        wp_insert_term($lingvonomo, 'v_lingvo', ['slug' => $lingvokodo]);
    }
}
// add_action('init', 'a_spektejo_enporti_lingvoterminojn');



/* == Afiŝospecoj == */

/**
 * Difini afiŝosspecojn
 */
function a_spektejo_afisxospecoj()
{

    // Videoj
    $labels = [
        'name'               => 'Videoj',
        'singular_name'      => 'Video',
        'menu_name'          => 'Videoj',
        'name_admin_bar'     => 'Video',
        'add_new'            => 'Proponi',
        'add_new_item'       => 'Proponi videon',
        'new_item'           => 'Nova videoafiŝo',
        'edit_item'          => 'Redakti videoafiŝon',
        'view_item'          => 'Rigardi videoafiŝon',
        'all_items'          => 'Ĉiuj videoj',
        'search_items'       => 'Serĉi videon',
        'not_found'          => 'Neniu video troviĝis.',
        'not_found_in_trash' => 'Neniu video troviĝis rubuje.'
    ];
    $args = [
        'labels'             => $labels,
        'description'        => 'Filmoj afiŝitaj en la retejo',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'menu_icon'          => 'dashicons-format-video',
        'query_var'          => true,
        'rewrite'            => ['slug' => 'v'],
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'taxonomies'         => ['v_fonto', 'v_kanalo', 'v_kategorioj', 'v_etikedoj', 'v_rilateco', 'v_lingvo'],
        'menu_position'      => 5,
        'supports'           => ['title', 'editor', 'author', 'comments']
        // 'supports'           => ['title', 'editor', 'author', 'custom-fields', 'comments']
    ];

    register_post_type(PROJEKTNOMO . '_video', $args);
}
add_action('init', 'a_spektejo_afisxospecoj');


/**
 * Restarigi la reskriboregulojn por adresoj je la aktivigo de la kromprogramo
 */
function a_spektejo_replenigi_reskriboregulojn()
{
    a_spektejo_afisxospecoj();
    flush_rewrite_rules();
}
register_activation_hook(LOKO_DE_KROMPROGRAMO, 'a_spektejo_replenigi_reskriboregulojn');



/* == Signaloj == */

/**
 * Krei datumbazotabelon por signaloj faritaj de uzantoj
 * (ekz. problemoraportoj, kategorioproponoj, ktp)
 */
function a_spektejo_krei_tabelon_por_signaloj()
{
    global $wpdb;
    $kodoprezenta_ordigo = $wpdb->get_charset_collate();
    $tabelo_nomo = $wpdb->prefix . PROJEKTNOMO . '_signaloj';

    $sql = "CREATE TABLE $tabelo_nomo (
      id bigint(11) NOT NULL AUTO_INCREMENT,
      speco varchar(20) NOT NULL,
      valoro varchar(20) NOT NULL,
      erospeco varchar(20) NOT NULL,
      ero_wpid bigint(11) NOT NULL,
      uzanto varchar(20) NOT NULL,
      tempo datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
      PRIMARY KEY  (id),
      KEY ero (erospeco,ero_wpid),
      KEY uzanto (uzanto)
    ) $kodoprezenta_ordigo;";

    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    $rezulto = dbDelta($sql);
}
register_activation_hook(LOKO_DE_KROMPROGRAMO, 'a_spektejo_krei_tabelon_por_signaloj');





/* == Forigitaj Eroj == */


/**
 * Krei datumbazotabelon por konservi bazajn datumojn pri forigitaj enhaveroj
 */
function a_spektejo_krei_tabelon_por_forigitaj_eroj()
{
    global $wpdb;
    $kodoprezenta_ordigo = $wpdb->get_charset_collate();
    $tabelo_nomo = $wpdb->prefix . PROJEKTNOMO . '_tombejo';

    $sql = "CREATE TABLE $tabelo_nomo (
      id bigint(11) NOT NULL AUTO_INCREMENT,
      speco varchar(20) NOT NULL,
      fonto varchar(20) NOT NULL,
      id_fonto varchar(100),
      wpid bigint(11),
      auxtoro varchar(20),
      kialo varchar(50) NOT NULL,
      tempo datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
      PRIMARY KEY  (id),
      KEY ero (speco),
      KEY id_fonto (fonto,id_fonto),
      KEY uzanto (auxtoro),
      KEY kialo (kialo)
    ) $kodoprezenta_ordigo;";

    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    $rezulto = dbDelta($sql);
}
register_activation_hook(LOKO_DE_KROMPROGRAMO, 'a_spektejo_krei_tabelon_por_forigitaj_eroj');





/* == Uzanto-roloj == */

/**
 * Difini necesajn uzanto-rolojn
 */
function a_spektejo_aldoni_uzantorolojn()
{
    // Helpulo
    $rolo_helpulo = get_role('helpulo');
    // Kapabloj
    $kapabloj_abonanto = get_role('subscriber')->capabilities;
    $kapabloj_helpulo  = array_merge(
        $kapabloj_abonanto,
        [
            'moderate_comments' => true,
            'manage_categories' => true,
            'publish_posts' => true,
            'edit_posts' => true,
            'edit_others_posts' => true,
            'edit_published_posts' => true
        ]
    );
    // Modifi la kapablojn se la rolo ekzistas, krei la rolon alikaze
    if (empty($rolo_helpulo)) {
        add_role('helpulo', 'Helpulo', $kapabloj_helpulo);
    } else { // FIXME: Ĉu necesas fari pli ekzaktan kontrolon?
        foreach ($kapabloj_helpulo as $kapablo => $cxu_permesi) {
            $rolo_helpulo->add_cap($kapablo, $cxu_permesi);
        }
    }
}
register_activation_hook(LOKO_DE_KROMPROGRAMO, 'a_spektejo_aldoni_uzantorolojn');





/* == Kromaj Datumoj  == */

/**
 * Enporti sekurdemandojn uzatajn kiel kaptĉao
 */
function a_spektejo_enporti_sekurdemandojn()
{
    if (!current_user_can('administrator') || wp_doing_ajax()) {
        return;
    }

    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton, mondo!', $protokoloID);

    $loko = ABSPATH . 'datumoj/demandoj.json';
    $dosiero = file_get_contents($loko);
    if ($dosiero === false) {
        protokolu(null, 'ERARO: la dosiero demandoj.json ne ekzistas!', $protokoloID);
        return;
    }

    $demandoj = json_decode($dosiero, true);
    if (empty($demandoj)) {
        protokolu(null, 'ERARO: ne eblas elkodigi la dosieron!', $protokoloID);
        return;
    }

    // krei liston de ĉiuj respondoj
    $cxiuj_elektoj = array_unique(
        array_reduce(
            array_column(
                $demandoj,
                'elektoj'
            ),
            'array_merge',
            []
        )
    );

    $rezulto_demandoj = update_option(PROJEKTNOMO . '_sekurdemandoj', $demandoj);
    $rezulto_elektoj = update_option(PROJEKTNOMO . '_sekurdemandoj_elektoj', $cxiuj_elektoj);

    protokolu($rezulto_demandoj, 'jen la rezulto pri konservi la datumojn:', $protokoloID);
    protokolu($rezulto_elektoj, 'jen la rezulto pri konservi la elektojn:', $protokoloID);
}


/**
 * Enporti historiajn datrevenojn
 */
function a_spektejo_enporti_datrevenojn()
{
    if (!current_user_can('administrator') || wp_doing_ajax()) {
        return;
    }

    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Saluton, mondo!', $protokoloID);

    if (!function_exists('yaml_parse')) {
        protokolu(null, 'Eraro: la necesaj funkcioj por legi YAML-dosieron ne troviĝis! Konsideru aktivigi ilin en via php-agordoj.', $protokoloID);
        return;
    }

    $loko = ABSPATH . 'datumoj/datrevenoj.yaml';
    $dosiero = file_get_contents($loko);
    if ($dosiero === false) {
        protokolu(null, 'ERARO: la dosiero datrevenoj.yaml ne ekzistas!', $protokoloID);
        return;
    }

    $datrevenoj = yaml_parse($dosiero, -1);

    protokolu($datrevenoj, 'jen la datrevenoj:', $protokoloID);

    if (empty($datrevenoj)) {
        protokolu(null, 'ERARO: ne eblas elkodigi la dosieron!', $protokoloID);
        return;
    }

    foreach ($datrevenoj as $monato_indekso => $monato_eventoj) {
        $monato = $monato_indekso + 1;
        $rezulto = update_option(PROJEKTNOMO . '_datrevenoj_' . $monato, $monato_eventoj);

        protokolu($rezulto, 'jen la rezulto pri ĝisnunigi la datumbazokampon:', $protokoloID);
    }
}



/**
 * Re-enporti datumojn el dosieroj en la datumbazon
 */
function a_spektejo_gxisnunigi_datumojn()
{
    $protokoloID = date('ymd_His_') . substr(strval(microtime()), 2, 8); // SENCIMIGO
    protokolu(null, 'Ĝisnunigas la datumojn...', $protokoloID);

    a_spektejo_enporti_lingvoterminojn();
    a_spektejo_enporti_sekurdemandojn();
    a_spektejo_enporti_datrevenojn();
}
register_activation_hook(LOKO_DE_KROMPROGRAMO, 'a_spektejo_gxisnunigi_datumojn');
add_action('a_spektejo_h_gxisnunigi', 'a_spektejo_gxisnunigi_datumojn');
